package main

import (
	"fmt"
	"os"
	"unsafe"
)

func main() {
	fmt.Println(os.Getpid())
	var a [1024 * 1024 * 1024]int8 //1Gb

	fmt.Println(unsafe.Sizeof(a))
	fmt.Scanln()
}
