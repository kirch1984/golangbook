# Contents

## Golang for Beginners

## Golang for Web
* [Templates](golangweb/01templates/README.md)
* net/http package, routing
* Serving files
* Forms
* Cookies
* Sessions
* Middleware
* CSRF
* Context  
  https://www.youtube.com/watch?v=LSzR0VEraWw

## Data Format Exchange
* [JSON](dfe/json/README.md)
* [Protocol Buffers](dfe/protobuf/README.md)

## gRPC
* [gRPC](grpc/README.md)

## DB
* [MongoDB](db/mongo/README.md)
* [Redis](db/redis/README.md)
* [RabbitMQ](db/rabbitmq/README.md)
* MySql

## DesignPatterns
* [DesignPatterns](designpatterns/README.md)
