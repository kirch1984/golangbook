package main

import (
	"fmt"
	"runtime"
	"strings"
)

const (
	iter = 5
	gor  = 7
)

func main() {
	for i := 0; i < gor; i++ {
		go doSomeWork(i)
	}
	fmt.Scanln()
}

func doSomeWork(i int) {
	for j := 0; j < iter; j++ {
		fmt.Printf(formatWork(j, i))
		runtime.Gosched()
	}
}

func formatWork(j, i int) string {
	return fmt.Sprintf("thred:%v iternum:%v %v\n", i, j, strings.Repeat("*", j+1))
}
