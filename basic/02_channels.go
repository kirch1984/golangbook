package main

import "fmt"

func main() {
	ch := make(chan int)

	go func(ch chan int) {
		v := <-ch
		fmt.Println("GO: Got data from channel:", v)
		fmt.Println("GO: After read data from channel.")
	}(ch)
	fmt.Println("MAIN: Before put data into channel.")
	ch <- 1984
	fmt.Println("MAIN: After put data into channel.")
}
