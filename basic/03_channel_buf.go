package main

import "fmt"

func main() {
	ch := make(chan int, 1)

	go func(ch chan int) {
		v := <-ch
		fmt.Println("GO: Got first data from channel:", v)
		v = <-ch
		fmt.Println("GO: Got second data from channel:", v)
		fmt.Println("GO: After read data from channel.")
	}(ch)
	fmt.Println("MAIN: Before put data into channel.")
	ch <- 1984
	ch <- 1985
	fmt.Println("MAIN: After put data into channel.")
	fmt.Scanln()
}
