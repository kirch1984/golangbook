package main

import "fmt"

func main() {
	ch := make(chan int, 1)

	go func(ch chan int) {
		for i := 1; i <= 10; i++ {
			ch <- i
		}
		close(ch)
	}(ch)

	for i := range ch {
		fmt.Println("Main: got", i)
	}
	fmt.Scanln()
}
