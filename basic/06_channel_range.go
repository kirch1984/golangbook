package main

import "fmt"

func main() {
	ch := make(chan int, 1)

	go func(ch chan int) {
		for i := 1; i <= 10; i++ {
			ch <- i
		}
		close(ch)
	}(ch)

	go func(ch chan int) {
		for i := range ch {
			fmt.Println("GO: got", i)
		}
	}(ch)

	fmt.Scanln()
}
