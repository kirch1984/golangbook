<a id="contnent" name="contnent"></a>

- [Introduction](#Introduction)
	- [Help Links](#Help-Links)
	- [Install](#Install)
	- [Hello World](#Hello-World)
	- [Packaging](#Packaging)
	- [Go CLI](#Go-CLI)
	- [Go documentation](#Go-documentation)
- [Comments](#Comments)
- [Variables & Declaration](#Variables--Declaration)
	- [Custom type declaration](#Custom-type-declaration)
	- [Constants](#Constants)
- [Basic Data types](#Basic-Data-types)
	- [Reference vs Value Types](#Reference-vs-Value-Types)
		- [Bool Type](#Bool-Type)
	- [Conversion](#Conversion)
- [fmt Package](#fmt-Package)
- [Control flow](#Control-flow)
	- [For Loop](#For-Loop)
		- [for like C style](#for-like-C-style)
		- [for with single conditoin](#for-with-single-conditoin)
		- [for infinity](#for-infinity)
		- [for with range](#for-with-range)
		- [break & continue](#break--continue)
	- [IF statment](#IF-statment)
	- [switch statement](#switch-statement)
		- [switch like if](#switch-like-if)
		- [switch with default](#switch-with-default)
		- [switch fallthrough](#switch-fallthrough)
		- [switch on value](#switch-on-value)
	- [Conditional logic operators](#Conditional-logic-operators)
- [Grouping Data](#Grouping-Data)
	- [Array](#Array)
	- [Slice](#Slice)
	- [Sort slice](#Sort-slice)
		- [Sort custom slice](#Sort-custom-slice)
	- [Byte Slice](#Byte-Slice)
	- [Map](#Map)
	- [Struct](#Struct)
		- [Embed struct](#Embed-struct)
		- [Anonymous struct](#Anonymous-struct)
		- [Struct Recivier](#Struct-Recivier)
		- [Struct and Pointers](#Struct-and-Pointers)
- [Functions](#Functions)
	- [Variadic parameter](#Variadic-parameter)
	- [UNFURLING a slice](#UNFURLING-a-slice)
	- [Defer](#Defer)
	- [Receiver Functions / Methods](#Receiver-Functions--Methods)
	- [Anonymous Functions](#Anonymous-Functions)
	- [Func expression](#Func-expression)
	- [return a func](#return-a-func)
	- [callback](#callback)
	- [Closure](#Closure)
	- [Recursion](#Recursion)
- [Interfaces & Polymorphism](#Interfaces--Polymorphism)
- [Pointers](#Pointers)
- [Application](#Application)
	- [base64](#base64)
	- [JSON](#JSON)
	- [easyjson](#easyjson)
	- [bcrypt](#bcrypt)
- [Concurrency](#Concurrency)
	- [WaitGroup](#WaitGroup)
	- [Race condition](#Race-condition)
	- [Mutex](#Mutex)
		- [sync.Mutex: паттерны использования](#syncMutex-паттерны-использования)
	- [RWMutex](#RWMutex)
	- [sync.Map](#syncMap)
	- [sync.Pool](#syncPool)
	- [sync.Once](#syncOnce)
	- [sync.Cond](#syncCond)
	- [Atomic](#Atomic)
	- [Timers and timeouts](#Timers-and-timeouts)
- [Channels](#Channels)
	- [Direction channel](#Direction-channel)
	- [Range](#Range)
	- [Select Case](#Select-Case)
	- [Context](#Context)
	- [Асинхронное получение данных](#Асинхронное-получение-данных)
	- [Пул воркеров](#Пул-воркеров)
	- [Ограничение по ресурсам](#Ограничение-по-ресурсам)
- [Error Handling](#Error-Handling)
- [Writing documentaion](#Writing-documentaion)
- [Testing & Benchmarking](#Testing--Benchmarking)
	- [Table driven tests](#Table-driven-tests)
	- [Subtests](#Subtests)
	- [Test Main](#Test-Main)
	- [Tests in Parallel](#Tests-in-Parallel)
	- [Subtests in Parallel](#Subtests-in-Parallel)
		- [Setup&Teardown with subtests in Parallel](#SetupTeardown-with-subtests-in-Parallel)
		- [Gotchas with subtests in Parallel](#Gotchas-with-subtests-in-Parallel)
	- [Run specific Test](#Run-specific-Test)
	- [Skipping Test](#Skipping-Test)
	- [Race condition](#Race-condition-1)
	- [Comparatition in Tests](#Comparatition-in-Tests)
	- [Package quick](#Package-quick)
	- [Coverage](#Coverage)
	- [Golint](#Golint)
	- [Benchmark](#Benchmark)
	- [Testing flags](#Testing-flags)
	- [Mocking](#Mocking)
	- [Testing HTTP](#Testing-HTTP)
		- [httptest.ResponseRecorder](#httptestResponseRecorder)
		- [httptest.Server](#httptestServer)
- [Examples](#Examples)
	
<a id="introduction" name="introduction"></a>

# Introduction 
[Up](#contnent)

* Кен Томпсон (UNIX,UTF-8)
* Роб Пайк (UTF-6,Plan 9,Inferno)
* Роберт Гризмер (распределенные системы в Goolge)

2009 - Первая доступная версия\
2012 - Версия 1.0

<a id="helplinks" name="helplinks"></a>
## Help Links 
[Up](#contnent)

[https://play.golang.org/](https://play.golang.org/)\
[https://tour.golang.org](https://tour.golang.org)\
[https://golang.org/](https://golang.org/)\
[https://godoc.org/](https://godoc.org/)\
[The Go Programming Language Specification](https://golang.org/ref/spec)\
[Effective Go](https://golang.org/doc/effective_go.html)

[Understanding The Memory Model Of Golang : Part 1](https://medium.com/@edwardpie/understanding-the-memory-model-of-golang-part-1-9814f95621b4)\
[Understanding The Memory Model Of Golang : Part 2](https://medium.com/@edwardpie/understanding-the-memory-model-of-golang-part-2-972fe74372ba)\
[Stack vs Heap Memory Allocation](https://www.geeksforgeeks.org/stack-vs-heap-memory-allocation/)

<a id="install" name="install"></a>

## Install 
[Up](#contnent)

[Download GoLang](https://golang.org/dl/)\
[Visual Studio Code](https://code.visualstudio.com/)\
[Go in Visual Studio Code](https://code.visualstudio.com/docs/languages/go)

<a id="helloworld" name="helloworld"></a>

## Hello World 
[Up](#contnent)
```go
package main // go программы организованы в пакеты

import "fmt" // получить доступ к внешнему пакету - fmt
//fmt - стандартная бибилиотка ввода/вывода для Go https://golang.org/pkg/fmt/

//main - главная фуния
//Любая Executable программа должна иметь "func main(){}"
func main() { 
	fmt.Println("Hello GoLang!")
}
```

<a id="packaging" name="packaging"></a>

## Packaging
[Up](#contnent)

Понятие пакета для Go - критически важно.\
Идея состоит в разделении кода на различные пакеты, что позволяет использовать пакет неоднокртано в разных местах программы и контролиорвать использование данных внтури каждого пакета.

В Go два типа пакетов:
1. Executable  - генерирует файл, который в последсвии можно запустить
2. Reusable - код используется как вспомогательный, например для написания библиотек

Соглашения об именах пакетов:
- имя пакета совападает с именем директории в которой пакет находится, это косается Reusable пакетов
- имя должно быть кратким и начинаться со строчной буквы

Go различает эти два типа по тому, что указано в строке с <code>package</code>\
<code>package main</code> - Executable\
<code>package SOME_PACKAGE_NAME</code> - Reusable

Когда собирается Executable пакет, то компилятор помимо <code>package main</code> должен еще найти фунию <code>main()</code>, иначе компилятор не сможет созадть биарник. Данная фуния явояется входной точкой для программы.

Если в программе необходимо подключить дополнчельные пакеты, то деалется это с помощью опертора <code>import</code>. Который указывает компилятору, что вы хотите ссылкаться на код содержащийся в данном пакете.\
Пример
```go
import (
	"fmt"
	"strings"
)
```
Стандартные пакеты находятся, где установлен Go (см <code>go env</code>)
Пакеты которые скачеы с git или созданы самомстоятельно - см 
<code>env | grep GOPATH</code>\
Пример
если Go установлен сюда - <code>/usr/local/go/</code>
а <code>GOPATH</code> указывает сюда - <code>/home/username/code/go</code>\
То пакет ищется в таком порядке:

	1. /usr/local/go/src/
    1. /home/username/code/go

Пакеты скачиваются с помощью <code>go get</code> и устанавливаются в <code>GOPATH</code>

Если необходимо использовать разные пакеты, но с одним и тем же именем, то можно исп  -именованныи импорт. Т.е. слева от пакета указать его псевдоним.\
Пример
```go
import (
"fmt"
myfmt "mylib/fmt"
)
```
Иногда необходимо импортировать пакет, но при этом не планируется его использование (например нужна отработка фунии <code>init</code> пакета), то можно исп пустой идентификтор <code>_</code>\
Пример
```go
import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)
```
Пустой идентификтор - очень полезная вещь. Например он часто исп, когда фуния возыращает несколко параметров, а нужен только один.\
Пример
```go
_,err := pkg.function([args])
```
<code>init</code> - фуния инициализации, выполняется до запуска <code>main</code> фунии. Исп для настройки пакетов, инициализации переменных или выполнение любой другой начальной загрузки.

Если планируется использовать gitlab,github..., то пакет следует называть по имен git репозитория. А также исходники пакета должны находится в корне репозитория.


<a id="gocli" name="gocli"></a>

## Go CLI 
[Up](#contnent)
```go
go buid - компилируем  сырцы
go clean - удалить бинарник,кэш файлы полученые при компилировании
go run - компилируем сырцы и запускаем откомпилированный код
go fmt - форматируем код во всех файлах текущей папки
go install - компилируем и устанавливаем пакет
go get - скачиваем сырцы
go test - запускаем тесты для текущего проекта
```
[An Overview of Go's Tooling](https://www.alexedwards.net/blog/an-overview-of-go-tooling)\
[50 оттенков Go: ловушки, подводные камни и распространённые ошибки новичков](https://habr.com/ru/company/mailru/blog/314804/)



<a id="godoc" name="godoc"></a>

## Go documentation
[Up](#contnent)

В Go имеется два способа предоставления информации разработчику.\
CLI: \
<code>go doc ИМЯ_ПАКЕТА</code>
```go
go doc fmt
package fmt // import "fmt"

Package fmt implements formatted I/O with functions analogous to C's printf
and scanf. The format 'verbs' are derived from C's but are simpler.


Printing

The verbs:

General:

    %v	the value in a default format
    	when printing structs, the plus flag (%+v) adds field names
    %#v	a Go-syntax representation of the value
    %T	a Go-syntax representation of the type of the value
    %%	a literal percent sign; consumes no value

Boolean:

    %t	the word true or false

```
Browser:\
<code>godoc -http=:6060</code>\
И затем в браузере открываем страницу <code>http://localhost:6060/</code>

Go doc работает и для наших пакетов, если придерживаться определенных правил при написании. Например комментировать - пакет, фунии, типы, глобальные переменные.
```go
//The package for summarization of two numbers
package main

import "fmt"

func main() {
	s := sum(1, 3)
	fmt.Println(s)
}

func sum(x, y int) int {
	return x + y
}

```


<a id="comments" name="comments"></a>

# Comments 
[Up](#contnent)
```go
//...
/*...*/
```



<a id="varsdeclare" name="varsdeclare"></a>

# Variables & Declaration 
[Up](#contnent)

When variables are being declared to their zero value, use the keyword var.\
When variables are being declared and initialized, use the short variable declaration operator.

Хорошее имя:
- последовательный (легко догадаться)
- короткое (легко набирать)
- точное (легко понять)
- 
Если переменная будед использоваться в другом файле, пакете, то имя мб длинным, по возмоности максимально описывающим за что отвечает переменная.\
При именовании нужно исп camelCase.Не используйте _, user_id,first_name - пример плохих имён.\
Сокращения должныбыть с заглавными буквами - ServerHTTP,IDProsessor.\
Параметры фуний должы следовать следующим правилам - 
если типа параметра доставточно информативен, то исп короткое имя.\
func AfterFunc(d *Duration,f func(string) string)\
если тип не дает никакий инфы, то имя должно быть более информативным\
func Name(sec,nsec int4)\
По ресиверам следующее соглашение - короткое имя переменной отражающее тип.\
func (d Duration) getTime(){}\
func (u User) showUser(){}\
Error типы должны быть в формате - FooError\
type FooError strcut{}\
А имя переменной типа Error должно быть в формате ErrFoo\
var ErrFoo = errors.New("some error happened")

```go
/*
NOTICE: declare&assign = initialization 
В Go имеется два типа объявления и присаивания значений преременной.
1. long form
объявляем и присваиваем занчения
var card = "Ace of Spades"
где
    var - означает  - создать новую переменную. Также позволяет создавать глобальные переменные. При исп := такое не возможно.
    card - имя переменной
Go - статичиски типизированный язык и поменять
тип переменной на лету как в языках js, python не получится.
Базовые типы данных в Go:
    bool
    string
    int
	float64
fmt.Printf("%T\n", x) - узнать тип переменной
`` - текст вкл в такие ковычки остается без форматирования
объявляем и НЕ присваиваем занчения
var card int - в этом случае, переменной присвоится нулевое значение данного типа (для int это 0)
2. short form
card := "Ace of Spades" - Go сам распознает тип переменной

Если объявлять переменную без присваивания ей занчения, Go такой переменной назначает нулевое значение:
bool - false,
int/float - 0, 
string - ""
pointers, functions, interfaces, slices, channels, maps - nil 
*/
package main

import "fmt"

func main() {
	card := "Ace of Spades"
	fmt.Println(card)
}
```

<a id="ctypedec" name="ctypedec"></a>

## Custom type declaration 
[Up](#contnent)
```go
//Шаблон
type nameNewType BaseTypeOrAnyCustomType
где:
nameNewType - имя нового типа
BaseTypeOrAnyCustomtype - или один из базовых типов Go или любой другой кастоммный тип созданный ранее
```
```go
//Создаём новый тип deck, который является слайсом стрингов 
type deck []string
```

<a id="const" name="const"></a>

## Constants
[Up](#contnent)
```go
//Константы существуют только во время компиляции.
//Константы могут быть не типизированными и типизированными
const ui=12345
const uf=3.141292
const ti int = 12345
const tf float32 = 3.141292
//Даже есть поддержка арифметики
const v = 2 * 2
```
<a id="datatypes" name="datatypes"></a>
# Basic Data types
[Up](#contnent)
[Built-In Types](https://golang.org/ref/spec#Boolean_types)\
https://blog.golang.org/strings



Эффективная склейка строк
```go
Т.к. строки read-only, каждая склейка через + или += приводит к выделению памяти. Что бы оптимизировать число аллокаций используйте strings.Builder
import "strings"
var b strings.Builder
for i := 33; i >= 1; i-- {
    b.WriteString("Код")
    b.WriteRune('ъ')
}
result := b.String()
```

## Reference vs Value Types 
[Up](#contnent)

```go
Reference type: 
slice
maps
channels
pointers
functions

Value type:
int
float
string
bool
structs

Когда мы объявляем Slice, то Go создаёт две записив памяти для данной переменной.
В первой хранит не изменяемый массив самих данных. А второй хранит данные сост из трёх полей - указатель на массив, текущая длина массива и вместимость массива(вмещаемое колво элементов в массив до реаллоцирования массива)

Если в фунию/метод передать данные ссылочного типа, то их значения Go может изменять напрямую без указателя.
```
<a id="booltype" name="booltype"></a>
### Bool Type 
[Up](#contnent)
```go
```
<a id="conversion" name="conversion"></a>

## Conversion 
[Up](#contnent)
```go
int(arg)
float32(arg)
bool(arg)
```

<a id="fmtpackage" name="fmtpackage"></a>

# fmt Package 
[Up](#contnent)

[package fmt](https://godoc.org/fmt)
```go
/*
%T - type
%t - bool
%b - binary (011101)
%d - int base 10
%s - string
%x - hex
%#x - 0xhex
%g - float
%v - the value in a default format
*/

//Print to standart out
func Print(a ...interface{}) (n int, err error)
func Printf(format string, a ...interface{}) (n int, err error)
func Println(a ...interface{}) (n int, err error)​

//Print to string 
func Sprint(a ...interface{}) string
func Sprintf(format string, a ...interface{}) string
func Sprintln(a ...interface{}) string

//Ptint to file or http responce

func Fprint(w io.Writer, a ...interface{}) (n int, err error)
func Fprintf(w io.Writer, format string, a ...interface{}) (n int, err error)
func Fprintln(w io.Writer, a ...interface{}) (n int, err error)
```

<a id="controlflow" name="controlflow"></a>

# Control flow 
[Up](#contnent)

<a id="loop" name="loop"></a>

## For Loop 
[Up](#contnent)
### for like C style
```go
/*
Шаблон:
C style
for init;condition;post{
	TODO
}
*/
for i := 0; i <= 100; i++ {
	fmt.Println(i)
}
//Nesting loop
for i := 0; i <= 10; i++ {
	for j := 0; j < 3; j++ {
		fmt.Printf("%v:%v\n", i, j)
	}
}
```
### for with single conditoin
```go
package main

import "fmt"

func main() {
	x := 1
	for x < 5 {
		fmt.Printf("%v\n", x)
		x++
	}
}
```

### for infinity
```go
package main

import "fmt"

func main() {
	x := 1
	for {
		if x > 9 {
			break
		}
		fmt.Printf("%v\n", x)
		x++
	}
}
```

### for with range
```go
```

### break & continue
```go
//break - прервать итерацию и немедлено завершить цикл
//continue - прервать итерацию и перейти к следующей итерации
package main

import "fmt"

func main() {
	x := 0
	for {
		if x > 100 {
			break
		}
		if x%2 != 0 {
			x++
			continue
		}
		fmt.Printf("%v\n", x)
		x++
	}
}

//print ascii example
package main

import "fmt"

func main() {
	for x := 33; x <= 122; x++ {
		fmt.Printf("%q\n", x)
	}
}
```

<a id="ifstmn" name="ifstmn"></a>

## IF statment 
[Up](#contnent)
```go
//initialization statment
package main

import "fmt"

func main() {
	if x := 42; x == 42 {
		fmt.Printf("%v\n", x)
	}
}
//x - ограничена областью видимостью if, т.е. вне данного выражения x не будет доступна

//if,else if,else
package main

import "fmt"

func main() {
	x := 41
	if x == 40 {
		fmt.Println("x is 40")
	} else if x == 41 {
		fmt.Println("x is 41")
	} else {
		fmt.Println("Default else")
	}
}
```
<a id="switchstmn" name="switchstmn"></a>

## switch statement 
[Up](#contnent)
### switch like if
```go
```
### switch  with default
```go
```
### switch fallthrough
```go
```
### switch on value
```go
```

<a id="condlogop" name="condlogop"></a>
## Conditional logic operators 
[Up](#contnent)
```go
```

<a id="grpdata" name="grpdata"></a>

# Grouping Data 
[Up](#contnent)

<a id="array" name="array"></a>

## Array 
[Up](#contnent)

Массив/Array - упорядоченый набор данных фиксированной длины сост. из элементов одного типа(bool,string,int,float32...)\

**Объявление и Инициализация**\
При объявлении массива обязательено указывается его размер. Размер не возможно изменить. Для это придётся создавать новый массив нужной длины и копировать в новый массив старый.
```go
var arr [5]int
```
Длина массива явл частью его типа.т.е. <code>[5]int</code> и  <code>[6]int</code> - это разные типы.\
После объявления массива, его элементам присваивается дефолтные значения согласно типу массива.\
Объявить и инициализировать массив можно литеральным спосбом
```go
arr:=[5]int{1,2,3,4,5}
```
Если вместо длины массива указать <code>...</code>, то размер массива Go расчитает сам
```go
arr:=[...]int{1,2,3,4,5,6,7,8,9}
```
Если необходимо проинициализиварвать только отдельные элементы массива, то используем конструкцию (остальным элементам присdоится дефолтное значение)
```go
arr:=[5]int{1:10,4:40} //[0 10 0 0 40]
```
**Работа с массивом**\
Обращение к элементу массива осуществляется оператором <code>[]</code>.
```go
arr:=[5]int{1,2,3,4,5}
arr[1] = 20 // меняем 2 на 20
```
Пример - массив указателей
```go
arr := [...]*int{new(int), new(int), new(int)}
*arr[0] = 10
*arr[1] = 20
*arr[2] = 30
fmt.Println(arr)
fmt.Println(*arr[0], *arr[1], *arr[2])
```

```go
package main

import "fmt"

func main() {
	//initialize  arrat
	var arr [5]int
	//sign value to arr index 3
	arr[3] = 42
	fmt.Printf("%v\n", arr)
}
/*
но массивы не особо используемы. чаще используют slice.
*/
```
<a id="slice" name="slice"></a>

## Slice
[https://blog.golang.org/go-slices-usage-and-internals](https://blog.golang.org/go-slices-usage-and-internals)

[Up](#contnent)
```go
/*

Slice - упорядоченный массив НЕ фиксированной длины, может расти или уменьшаться
Все элнты в слайсе должны быть одного типа.

*Create slice
-------------
	x := []int{1, 2, 3, 4, 5}
	fmt.Printf("%v\n", x)

*Full Copy
-------------
	s := []int{1,2,3}
	s2 := make([]int, len(s))
	copy(s2, s)

*Slice for range
----------------
	x := []int{1, 2, 3, 4, 5}
	for index, value := range x {
		fmt.Printf("index:%v\tvalue:%v\n", index, value)
	}

	for i:=0;i<len(x);i++{
		fmt.Println(x[i])
	}

*Slice - slising a slice
------------------------
	x := []int{1, 2, 3, 4, 5}
	fmt.Println(x[0:3]) //[1,2,3]

*Slice - append to a slice
------------------------
	x := []int{1, 2, 3, 4, 5}
	fmt.Println(x)
	x = append(x, 101, 102, 103, 104, 105)
	fmt.Println(x)
	//append slice to slice
	y := []int{10, 11, 12, 13, 14, 15}
	x = append(x, y...) //variadic
	fmt.Println(x)

*Slice - delete from a slice
----------------------------
	x := []int{1, 2, 3, 4, 5}
	fmt.Println(x) //[1 2 3 4 5]
	//delete 3
	x = append(x[:2], x[3:]...)
	fmt.Println(x) //[1 2 4 5]
	
*Slice - make
	//make([]int,length,capacity)
	//length - колво элементов в слайсе
	//cap - под какое колво элементов зарезервировано памяти
	//если привысить данное колво элементов, то capacity увелит
	//в два раза и пересоздаст слайс
	x := make([]int, 0, 100)
	fmt.Println(x)
	fmt.Println(len(x))
	fmt.Println(cap(x))

*Slice - slice of slice
	x := []int{1, 2, 3, 4, 5}
	y := []int{10, 20, 30, 40, 50}
	z := [][]int{x, y}
	fmt.Println(z)//[[1 2 3 4 5] [10 20 30 40 50]]
----------------------------

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
fruits =[]string{"apple","banana","grape","orange"}
fruits[0] //aplle
fruits[3] //orange

fruits[startIndex:endIdexNotIncluding]
fruits[0:2] //aplle, banana
fruits[2:] //grape,orange
fruits[:2] //aplle, banana
*/
package main

import "fmt"

func main() {
	cards := []string{"Ace of Diamonds", newCard()}//Объявляем слайс и 
	//присваиваем ему занчения
	cards = append(cards, "Six of Spades") //добавить новый элнт в конец слайса.
	//append фуния не модифицирует текущий слайс, а создаёт новый и присваевает
	//его обратно.

	//Итерируемся по slice
	for i, card := range cards {
		//i - индекс элта в масссиве
		//текущее значение элта из массива по которому итерируемся
		//range - ключеое слово означающее что мы будем итерироваться
		//по слайсу/массиву/структуре...
		//:= т.к. после кадой итерацци i и card уничтожаются.
		fmt.Println(i, card)
	}
}
func newCard() string {
	return "Five of Diamonds"
} 
```

<a id="sortslice" name="sortslice"></a>

## Sort slice 
[Up](#contnent)
```go
//Sort slice of Int
package main

import (
	"fmt"
	"sort"
)

func main() {
	i := []int{9, 3, 4, 5, 1, 8, 5, 3}
	sort.Ints(i)
	fmt.Println(i)//[1 3 3 4 5 5 8 9]
}
//Sort slice of String
package main

import (
	"fmt"
	"sort"
)

func main() {
	s := []string{"abc", "bac", "bca", "CAB", "acb", "cba", "cab", "ABC", "ACB", "CBA", "BCA"}
	sort.Strings(s)
	fmt.Println(s) //[ABC ACB BCA CAB CBA abc acb bac bca cab cba]
}

```

<a id="sortcustomslice" name="sortcustomslice"></a>

### Sort custom slice 
[Up](#contnent)
```go
package main

import (
	"fmt"
	"sort"
)

type person struct {
	fname string
	age   int
}

type ByAge []person

func (ba ByAge) Len() int           { return len(ba) }
func (ba ByAge) Less(i, j int) bool { return ba[i].age < ba[j].age } //если заменить на >, то отсортирует в обратном порядке
func (ba ByAge) Swap(i, j int)      { ba[i], ba[j] = ba[j], ba[i] }

type ByName []person

func (bn ByName) Len() int           { return len(bn) }
func (bn ByName) Less(i, j int) bool { return bn[i].fname < bn[j].fname } //если заменить на >, то отсортирует в обратном порядке
func (bn ByName) Swap(i, j int)      { bn[i], bn[j] = bn[j], bn[i] }

func main() {
	p1 := person{"Tony", 43}
	p2 := person{"Mark", 13}
	p3 := person{"Kirill", 35}
	p4 := person{"Anton", 23}
	p5 := person{"Zak", 110}

	people := []person{p1, p2, p3, p4, p5}
	fmt.Println("UnSorted:", people)

	//Sort By Age
	// sort.Sort(ByAge(people))
	// fmt.Println("Sorted by age:", people)
	
	//Sort By Name
	sort.Sort(ByName(people))
	fmt.Println("Sorted by name:", people)

}
```

<a id="byteslice" name="byteslice"></a>

## Byte Slice 
[Up](#contnent)
```go
byte slice(computer friendly representaion of string) - string of characters

"Hi there!" string -->ascii--> [72 105 32 116 104 101 114 101 33] byte slice 

Type Conversion:
[]byte("Hi there!)
```
```go
package main

import "fmt"

func main() {
	greeting := "Hi there!"
	fmt.Println([]byte(greeting)) //[72 105 32 116 104 101 114 101 33]
}
```


<a id="map" name="map"></a>

## Map 

[https://blog.golang.org/go-maps-in-action](https://blog.golang.org/go-maps-in-action)

[Up](#contnent)

В качестве ключа могут выступать занчения сравниваемых типов. т.е. все кроме slice,map и func. Даже struct мб ключем.

```go
/*
*map intoduction
----------------
	m := map[string]int{
		"Tony":   33,
		"Kirill": 35,
	}
	m["Mark"] = 3
	fmt.Println(m)         //map[Kirill:35 Mark:3 Tony:33]
	fmt.Println(m["Mark"]) //3
	//если обратиться к не сущесвующему элму, то вернется дефолтное значение типа
	fmt.Println(m["Jim"]) //0
	//для проверки есть элемент в мапе используют конструкцию
	v, ok := m["Jim"]
	fmt.Println(v)  //
	fmt.Println(ok) //false

	if v, ok = m["Jim"]; !ok {
		fmt.Println("Item doesn't exist.")
	}
*range
------------------
	m := map[string]int{
		"Tony":   33,
		"Kirill": 35,
	}
	m["Mark"] = 3
	for key, value := range m {
		fmt.Printf("%v:%v\n", key, value)
	}
*delete
------------------
	m := map[string]int{
		"Tony":   33,
		"Kirill": 35,
	}
	m["Mark"] = 3
	fmt.Println(m)
	delete(m, "Tony")
	fmt.Println(m)
\\\\\\\\\\\\\\\\\\\\\\\
*/
/*
map - коллекция key-value пар, где все ключи должны быть одного типа. И значения тоже одного типа. 
*/
//Существует несколько способов объявления и присваивания занчений мапу.
//1. объявляем и сразу присваиваем
colors := map[string]string{
	"red":   "#ff0000",
	"green": "#00ff00",
	"blue":  "#0000ff",
}
//2. объявляем ч.з. var
var colors map[string]string
//3. объявляем ч.з. make
colors := make(map[string]string)
//добваляем значения в мэп объявленный вторым или третьим способом
colors["red"] = "#ff0000"
colors["green"] = "#00ff00"
colors["blue"] = "#0000ff"

//delete keys
delete(colors,"blue")

//Итерация по map
package main

import "fmt"

func main() {
	colors := map[string]string{
		"red":   "#ff0000",
		"green": "#00ff00",
		"blue":  "#0000ff",
		"white": "#ffffff",
		"black": "#000000",
	}

	for k, v := range colors {
		fmt.Printf("Color: %s\tHex:%s\n", k, v)
	}

}
```

<a id="struct" name="struct"></a>

## Struct 
[Up](#contnent)

```go
//Struct - составной тип данных, позволяющий хранить данные разлиных типов.
//Рекомендовано при создании структуры поля начинать от типа занимающего больше байт к меньшему.

//define struct
type person struct {
	firstName string
	lastName  string
}
//declare struct
//long form
p1 := person{
    firstName: "Mark",
    lastName:  "Chesnov",
}
//short form
//не рекомендуется тмп дпнный вариант, ибо можно сменить поля структуры и переменная поломается
p1 := person{"Mark","Chesnov"}
//one more form
var p2 person
p2.firstName = "Tony"
p2.lastName = "Hawk"
```

### Embed struct 
[Up](#contnent)
```go
//Embed struct
//first form
package main

import (
	"fmt"
)

type person struct {
	firstName string
	lastName  string
	contact contactInfo
}
type contactInfo struct {
	email   string
	zipCode int
}

func main() {
	p1 := person{
		firstName: "Mark",
		lastName:  "Chesnov",
		contact: contactInfo{
			email:   "email@email.email",
			zipCode: 94000,
		},
	}
	fmt.Println(p1)
	fmt.Println(p1.firstName)
	fmt.Println(p1.lastName)
	fmt.Println(p1.contact.email)
	fmt.Println(p1.contact.zipCode)
}
```
```go
//Embed struct
//second form
package main

import (
	"fmt"
)

type person struct {
	firstName string
	lastName  string
	contactInfo
}
type contactInfo struct {
	email   string
	zipCode int
}

func main() {
	p1 := person{
		firstName: "Mark",
		lastName:  "Chesnov",
		contactInfo: contactInfo{
			email:   "email@email.email",
			zipCode: 94000,
		},
	}
	fmt.Println(p1)
	fmt.Println(p1.firstName)
	fmt.Println(p1.lastName)
	fmt.Println(p1.contactInfo.email)
	fmt.Println(p1.contactInfo.zipCode)
	fmt.Println(p1.email)
	fmt.Println(p1.zipCode)
}
```
### Anonymous struct
[Up](#contnent)
```go
package main

import "fmt"

func main() {
	p1 := struct {
		fname string
		lname string
		age   int
	}{
		fname: "Tony",
		lname: "Howk",
		age:   35,
	}
	//Declare p2 with zero value
	var p2 struct{
		fname string
		lname string
		age   int
	}

	fmt.Printf("%v\n", p1)
	fmt.Printf("%v\n", p2)
}
```

### Struct Recivier 
[Up](#contnent)
```go
package main

import (
	"fmt"
)

type person struct {
	firstName string
	lastName  string
	contactInfo
}
type contactInfo struct {
	email   string
	zipCode int
}

func main() {
	p1 := person{
		firstName: "Mark",
		lastName:  "Chesnov",
		contactInfo: contactInfo{
			email:   "email@email.email",
			zipCode: 94000,
		},
	}
	p1.print()
}

func (p person) print() {
	fmt.Printf("%+v\n", p)
}
```

### Struct and Pointers 
[Up](#contnent)
```go
//Go - это язык pass by value, т.е. при передаче аргумента в фунию/метод, передается копия значения аргумента, а не сам аргумент.
//Рассмотрим пример - передадим в фунию переменную и попробуем изменить. У нас ничего не полится.

package main

import (
	"fmt"
)

func main() {
	a := 10
	updateInt(a)
	fmt.Println(a) //10
}

func updateInt(a int) {
	a = 20
}


```
```go
//Но если передать аргумент в фунию через укзатель, то фуния изменит значение аргумента
package main

import (
	"fmt"
)

func main() {
	a := 10
	updateInt(&a)
	fmt.Println(a)
}

func updateInt(a *int) {
	*a = 20 //20
}
```
```go
// В Go cущетсвуют фунии конструкторы, которые возвращают обеъкет определенного типа
package main

import (
	"fmt"
)

type person struct{
	id int
	firstName string
}

func main() {
	p:= newPerson()
}

func newPerson() *person {
	p:= person{
		id:1,
		firstName:"Tony"
	}
	return &p
}
// В таких фуниях рекомендуют возвращать именно в таком виде указатель.
//т.е. вот так это уже не кошерно
func newPerson() *person {
	p:= &person{
		id:1,
		firstName:"Tony"
	}
	return p
}

```
```go
/*
&variable - Give me the memory address of the value this variable is pointing at
*pointer - Give me the value this memory address is pointing at 
Но есть отличие между *type и *value - в пером случае мы указываем Go что будем работать с указателем на переменную определенного типа. А во втором случае (*value) - мы говорим Go что хотим манипулировать данными на который указывает указатель.
*/
package main

import (
	"fmt"
)

type person struct {
	firstName string
	lastName  string
	contactInfo
}
type contactInfo struct {
	email   string
	zipCode int
}

func main() {
	p1 := person{
		firstName: "Mark",
		lastName:  "Chesnov",
		contactInfo: contactInfo{
			email:   "email@email.email",
			zipCode: 94000,
		},
	}
	p1.updateFirstName("Kirill")
	p1.print()
}

func (p *person) updateFirstName(newFirstNmae string) {
	p.firstName = newFirstNmae
}
func (p person) print() {
	fmt.Printf("%+v\n", p)
}
```


<a id="funcnandtypes" name="funcnandtypes"></a>

# Functions
[Up](#contnent)

Функция как объект первого класса означает,
что вы можете присваивать функцию в какую-то переменную, принимать функцию,
как аргумент в другую функцию и возвращать функцию,
как результат работы какой-то функции.
Ну а также иметь функцию, как поле какой-то структуры.

```go
Фунии позволяют разбить код на более мелкие составляюще.

//Шаблон
func funcName([arg1 type_arg1],[arg2 type_arg2],...) ([type_ret_arg1,type_ret_arg2]){
    //TODO
    [return arg1,arg2,...]
}
[] - всё что в данных скобках - не является обязательным параметром
func - ключевое слово при объявлении фунии
funcName - имя фунии
arg1 type_arg1 - при передаче аргументов в фунию, указываем имя аргумента и его тип
type_ret_arg - если фуния что нибудь возвращает, то указываем тип возвращаемых данных
[return arg1,arg2,...] - можно возвращать один и более аргументов
//Example: no args, return nothing
package main

import "fmt"

func main() {
	foo()
}
func foo() {
	fmt.Println("foo func called")
}
//Example: arg passed
package main

import "fmt"

func main() {
	bar("Tony")
}
//PASS BY VALUE
func bar(s string) {
	fmt.Printf("Hello, %s!\n", s)
}
//Example: func return int
package main

import "fmt"

func main() {
	fmt.Printf("%v\n", sum(8, 4))
}

func sum(x, y int) int {
	return x + y
}
//Example: func multiple return
package main

import "fmt"

func main() {
	x, y := woo("Tony", "Hawk")
	fmt.Println(x)
	fmt.Println(y)
}

func woo(fname, lname string) (string, bool) {
	a := fmt.Sprintf("Hello, %v %v", fname, lname)
	b := true
	return a, b
}
```
<a id="variadic" name="variadic"></a>

## Variadic parameter
[Up](#contnent)
```go
Передаём, как бы по одному аргументу, но в фуние эти аргументы складываются в слайс. 
Когда исп вариативный аргумент, то он должен приниматься последним в очереди переданных аргументов.
Данный аргумент межет принять и 0 занчений, т.е.
x := foo() - тоже будет работать, но результат будет соответствующий - всё по нулям.


package main

import "fmt"

func main() {
	x := foo(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)
	fmt.Println("Total:", x)
}

func foo(x ...int) int {
	fmt.Println(x)
	fmt.Printf("%T\n", x)
	sum := 0
	for i, v := range x {
		fmt.Println("index is", i, ":", v)
		sum += v
	}
	return sum
}
```

<a id="unfurlingslice" name="unfurlingslice"></a>

## UNFURLING a slice
[Up](#contnent)
```go
Используется, когда необхоимо в фунию передать слайс,как вариационный аргумент
package main

import "fmt"

func main() {
	x := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	s := foo(x...) //
	fmt.Println("Total:", s)
}

func foo(x ...int) int {
	fmt.Println(x)
	fmt.Printf("%T\n", x)
	sum := 0
	for i, v := range x {
		fmt.Println("index is", i, ":", v)
		sum += v
	}
	return sum
}

```
<a id="defer" name="defer"></a>

## Defer
[Up](#contnent)
```go
defer  - оператор отложенных фуний. Т.е. defer вызфывает фунии либо за вершению основой main фунии(т.е. по завершению программы), либо при панике случившейся в программе.
Исп при panic recovery и закрытии файлов,соединений.
//Example: call defer in the end main function
package main

import "fmt"

func main() {
	defer foo() //вызовется самой последней
	bar()
	woo()
}

func foo() {
	fmt.Println("foo")
}

func bar() {
	fmt.Println("bar")
}

func woo() {
	fmt.Println("woo")
}
```

<a id="receiver" name="receiver"></a>

## Receiver Functions / Methods 
[Up](#contnent)
```go
//Шаблон
func (arg arg_type) funcName(){
    //TODO
}
где
(arg arg_type) - обязательное поле при моздании методов
arg_type - только указанный тип данных может использовать данный метод
```

```go
//deck.go
package main
import "fmt"
//Создаём новый тип
type deck []string
//Создаём метод, который будет доступен переменным толко типа deck
func (d deck) print() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}
```

```go
//main.go 
package main
func main() {
    //объявляем переменноую типа deck и присваиваем ей значения
	cards := deck{"Ace of Diamonds", newCard()}
    cards = append(cards, "Six of Spades")
    //вызываем метод
	cards.print()
}
func newCard() string {
	return "Five of Diamonds"
}
```
<a id="anonymous" name="anonymous"></a>

## Anonymous Functions
[Up](#contnent)
```go
package main

import "fmt"

func main() {
	//run anonymous func
	func() {
		fmt.Println("Run Anonymous function.")
	}()
	//pass arguments in anonymous func
	func(x int) {
		fmt.Println("x is", x)
	}(42)
}
```
<a id="funcexp" name="funcexp"></a>

## Func expression
[Up](#contnent)
```go
package main

import "fmt"

func main() {
	s := func() {
		fmt.Println("Hello")
	}
	s()
	f := func(x int) {
		fmt.Println("x is", x)
	}
	f(42)
}
```

<a id="returnfunc" name="returnfunc"></a>

## return a func
[Up](#contnent)

```go
package main

import "fmt"

func main() {
	x := foo()
	//тоже самое
	// x := func() int {
	// 	return 1984
	// }
	fmt.Printf("%T\n", x) //func()int
	fmt.Println(x())      //1984
	fmt.Println(foo()())  //фокус-покус. 1984
}

func foo() func() int {
	return func() int {
		return 1984
	}
}
```
<a id="callback" name="callback"></a>

## callback
[Up](#contnent)

```go
передача фунии, как аргумент в другую фунию
package main

import "fmt"

func main() {
	ii := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	s := sum(ii...)
	fmt.Println("All num sum:", s)

	evensum := evensum(sum, ii...)
	fmt.Println("Even Sum: ", evensum)
}

func sum(x ...int) int {
	total := 0
	for _, v := range x {
		total += v
	}
	return total
}

func evensum(f func(x ...int) int, j ...int) int {
	var y []int
	for _, v := range j {
		if v%2 == 0 {
			y = append(y, v)
		}
	}
	s := f(y...)
	return s
}
```


<a id="closure" name="closure"></a>

## Closure
[Up](#contnent)

```go
- одна  область видимости охватывает другую оюласть видимости
- переменные инициализированнве в родиетльской области видимости доступны дочерней области видимости
- замыкания помогают ограничить область видмость переменных
//Example:package scope
package main

import "fmt"

//package scope
var x int

func main() {
	fmt.Println("main:", x)
	x++
	fmt.Println("main:", x)
	foo()
	fmt.Println("main:", x)
}

func foo() {
	x++
	fmt.Println("foo:", x)
}

//Example: main scope
package main

import "fmt"

func main() {
	//main scope
	var x int
	fmt.Println("main:", x)
	x++
	fmt.Println("main:", x)
	foo()
	fmt.Println("main:", x)
}

func foo() {
	fmt.Println("foo:")
}
//Example: block scope
package main

import "fmt"

func main() {
	var x int
	fmt.Println(x)
	//block scope {},if,for...
	//y - не будет дсотупна вне {}
	//но x - будет доступен в блоке {}
	{
		y := 1984
		fmt.Println(y)
		fmt.Println(x)
	}
	x++
	fmt.Println(x)
}
//Example: closer
package main

import "fmt"

func main() {
	i := incrementor()
	fmt.Println(i()) //1
	fmt.Println(i()) //2
	fmt.Println(i()) //3
	fmt.Println(i()) //4
	j := incrementor()
	fmt.Println(j()) //1
	fmt.Println(j()) //2
	fmt.Println(j()) //3
	fmt.Println(j()) //4
}

func incrementor() func() int {
	var x int
	return func() int {
		x++
		return x
	}
}

```
<a id="recursion" name="recursion"></a>

## Recursion
[Up](#contnent)

```go
- фуния вызывающая сама себя
- всё что делает рекусрия - можно сделать с loop
package main

import "fmt"

func main() {
	n := factorial(4)
	fmt.Println(n)
}

func factorial(n int) int {
	if n == 0 {
		return 1
	}
	return n * factorial(n-1)
}
//Тоже самое только через loop
package main

import "fmt"

func main() {

	n := factorial(4)
	fmt.Println(n)
}

func factorial(n int) int {
	total := 1
	for ; n > 0; n-- {
		total *= n
	}
	return total
}
```
<a id="interfaces--polymorphism" name="interfaces--polymorphism"></a>

# Interfaces & Polymorphism
[Up](#contnent)
```go
/*
Интерфейс позволяют описать поведение.
Главная его задача - реализация полиморфизма в Go. 
Получается что значение(переменная) может иметь больше чем один тип.
*/
package main

import "fmt"

//создаём тип men
type men struct {
	id    int
	fname string
	lname string
	sex   string
	age   int
}

//создаём метод для типа men
func (p men) speak() {
	fmt.Printf("Hello, I am %v %v!\n", p.fname, p.lname)
}

//создаём тип women
type women struct {
	id    int
	fname string
	lname string
	sex   string
	age   int
}

//создаём метод для типа women
func (p women) speak() {
	fmt.Printf("Hello, I am %v %v!\n", p.fname, p.lname)
}

//создаём тип human
type human interface {
	speak()
}

//cоздаём фунию для типа human, полиморфизм в действии
func tell(h human) {
	fmt.Printf("Human told - ")
	h.speak()
}

func main() {
	tony := men{
		id:    1,
		fname: "Tony",
		lname: "Hawk",
		sex:   "Male",
		age:   43,
	}
	fmt.Printf("Men speak - ")
	tony.speak()

	phaina := women{
		id:    2,
		fname: "Phaina",
		lname: "Ranevskya",
		sex:   "Female",
		age:   35,
	}
	fmt.Printf("Women speak - ")
	phaina.speak()
	//полиморфизм в действии
	tell(tony)
	tell(phaina)
}
/////////////////////////////////
package main

import "fmt"

type englishBot struct{}
type spanishBot struct{}
type boter interface {//объявляем новый тип boter
	getGreeting() string//любой объект имеющий метод/фунию 'getGreeting() string' будет являться типом boter
}

func main() {
	eb := englishBot{}
	sb := spanishBot{}

	printGreeting(eb)
	printGreeting(sb)
}

func (englishBot) getGreeting() string {
	return "Hello There!"
}

func (spanishBot) getGreeting() string {
	return "Hola!"
}

func printGreeting(b boter) {
	fmt.Println(b.getGreeting())
}
```
<a id="pointers" name="pointers"></a>

# Pointers 
[Up](#contnent)
```go
Переменные хранятся в памяти. Каждое место в памяти имеет адрес. Указатель - адрес памяти.
& -> give me the address
* -> give me the value stored in this address

package main

import (
	"fmt"
)

func main() {
	a := 1984
	fmt.Println(a)  //1984
	fmt.Println(&a) //адрес памяти 0xc00001a0b8, где хранится переменная 'a'

	fmt.Printf("%T\n", a)  //int
	fmt.Printf("%T\n", &a) //*int - pointer

	var b *int = &a
	fmt.Println(b)        //0xc00001a0b8
	fmt.Printf("%T\n", b) //*int
	fmt.Println(*b)       //1984
	fmt.Println(*&a)      //1984

	*b = 2033
	fmt.Println(a) //2033
}

When to use pointers
- когда необходимо оперировать(изменять) большими данными и не хочется пресоздавать их постоянно при переадчи их  фунии.
- Everything in Go is PASS BY VALUE.

//Example - no pointers
package main

import (
	"fmt"
)

func main() {
	x := 0
	fmt.Println("before foo x:", x)//0
	foo(x)
	fmt.Println("after foo x:", x)//0
}

func foo(y int) {
	fmt.Println("foo start y:", y)//0
	y = 1984
	fmt.Println("foo end y:", y)//1984
}

//Example - pointers
package main

import (
	"fmt"
)

func main() {
	x := 0
	fmt.Println("before foo x:", x) //0
	foo(&x)
	fmt.Println("after foo x:", x) //1984
}

func foo(y *int) {
	fmt.Println("foo start y:", y) //0xc00001a0b8
	*y = 1984
	fmt.Println("foo end y:", y) //0xc00001a0b8
}


Receivers	Values
-------------------------
(t T)		T and *T
(t *T)		*T
```

<a id="application" name="application"></a>

# Application

<a id="base64" name="jsobase64n"></a>

## base64 
[Up](#contnent)

[http://base64.ru/](http://base64.ru/)\
[encoding/base64](https://godoc.org/encoding/base64)

Base64 - стандарт кодирования(обратимый) двоичных данных при помощи только 64 символов ASCII.\
В основе алгоритма лежит сведение трех восьмерок битов (24) к четырем шестеркам (тоже 24) и представление этих шестерок в виде символов ASCII.\
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=" - как можно заметить в строке не 64 символа, а 65. Последний символ нужен для обработки особых случаев,когда нужно "добить" "четвёрку" до конца.
Преобразование байтов в символы ASCII происходит итеративно, группами по 3 байта или 24 бита.\
Если в выбранной группе, байтов меньше чем 3, тогда недостающие байты конвертируются в 65 символ, т.е. в знак =\
Из трех восьмибитных групп выделяются четыре шестибитные группы. Далее битовые последовательности переводятся в десятичную систему счисления и получаются индексы. По полученным индексам выбираются символы из кодирующей строки и записывается результат.\
На практике это выгдядит так
```go
Допустим нужно символ H представить в base64.
H в ASCII имеет в base10 номер 72 (см https://www.asciitable.com/index/asciifull.gif)
в base2 это 01001000 - делим на группу состоящую из 4 байт по 6 бит
010010
000000
т.к. до полной 4 не хватает 2 байт, то заполнены они будут символом =
далее эти байты переводим в base10
010010 -> 18
000000 -> 0
Полученные числа используем в качестве индексов(см https://qph.fs.quoracdn.net/main-qimg-c49c369409da7d0b660f84e8df16fac9.webp)
 и получаем символы
18 -> S
0 -> A
не забываем про последнии два пустых байта 
и того H в base64 будет как SA==

еще пример
abc -->(ASCII tbale)->a:97,b:98,c:99
97,98,99 --> base2 --> 97:01100001,98:01100010,99:01100011
01100001,01100010,01100011 --> делим на группу по 4 байта, в каждом байте по 6 бит -->
011000
010110
001001
100011
далее переводим эти байты в base10
011000-->24
010110-->22
001001-->9
100011-->35
Полученые числа используем как индексы для символов в base64
24:Y
22:W
9:J
35j
и того abc->YWJj
```

```go
//Пример на Go
package main

import (
	"encoding/base64"
	"fmt"
)

func main() {
	data := "abc"
	dataEnc := base64.StdEncoding.EncodeToString([]byte(data))
	fmt.Printf("%v\n", dataEnc) //YWJj

	data = "SGVsbG8sIEkgYW0gc3RyaW5nLg=="
	dataDec, _ := base64.StdEncoding.DecodeString(data)
	fmt.Printf("%v\n", string(dataDec))//Hello, I am string.
}
```
```go
//если хотим организовать поточное кодирование/декодирование base64
package main

import (
	"encoding/base64"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	dataByte := []byte("Hello,I am string")
	encoder := base64.NewEncoder(base64.StdEncoding, os.Stdout)
	encoder.Write(dataByte) //SGVsbG8sSSBhbSBzdHJpbmc=
	encoder.Close()         //нужно обязательно закрывать, иначе не будет дописан =
	fmt.Println()
	dataString := "SGVsbG8sSSBhbSBzdHJpbmc="
	r := base64.NewDecoder(base64.StdEncoding, strings.NewReader(dataString))
	io.Copy(os.Stdout, r) //Hello,I am string
	fmt.Println()
}
```
<a id="json" name="json"></a>

## JSON 
[Up](#contnent)\
[JSON-to-Go](https://mholt.github.io/json-to-go/)
```go
JSON(JavaScript Object Notation)
marshal - это процесс преобразованя объекта хранимого в памяти в формат данных одубного для хранения и передачи.(unmarshal - обратный процесс)

-marshal: struct-->json (convert struct into json)
если мы хотим конвертировать объекты структуры в json, то поля структуры должны быть с заглавной буквы (иначе они будут недоступны внешним пакетам).
если поле структуры содержит тэг json: tag, то данные будут сконвертированы с именем поля указанного в тэге.
func Marshal(v interface{}) ([]byte, error)
v interface{} - which can be 'any' go type
[]byte - slice of byte []byte, containing the literal string that is the JSON object

-unmarshal: json-->struct (convert json into struct)
func Unmarshal(data []byte, v interface{}) error
data -  slice of bytes (This a raw string, this is the JSON object that you want to parse)
v interface{} - указатель на структуру

//Example - marshal
package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type person struct {
	First string
	Last  string
	Age   int
}

func main() {
	p1 := person{
		First: "Tony",
		Last:  "Hawk",
		Age:   43,
	}

	p2 := person{
		First: "Mark",
		Last:  "Chesnov",
		Age:   3,
	}

	people := []person{p1, p2}

	bs, err := json.Marshal(people)
	if err != nil {
		log.Println(err)
	}
	fmt.Printf("JSON: %v\n", string(bs))
}

//Example - unmarshal
package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type person struct {
	First string `json:"First"`
	Last  string `json:"Last"`
	Age   int    `json:"Age"`
}

func main() {
	s := `[{"First":"Tony","Last":"Hawk","Age":43},{"First":"Mark","Last":"Chesnov","Age":3}]`
	bs := []byte(s)

	var people []person
	err := json.Unmarshal(bs, &people)
	if err != nil {
		log.Println(err)
	}

	fmt.Println("All data:\n", people)

	for _, v := range people {
		fmt.Println(v.First, v.Last, v.Age)
	}
}

```
<a id="easyjson" name="easyjson"></a>

## easyjson 
[Up](#contnent)

[easyjson](https://github.com/mailru/easyjson)

```go
```

<a id="bcrypt" name="bcrypt"></a>

## bcrypt 
[Up](#contnent)
```go
package main

import (
	"fmt"
	"log"

	"golang.org/x/crypto/bcrypt"
)

func main() {
	s := "password12345"
	salt := "salt"
	//Generate Hash
	bs, err := bcrypt.GenerateFromPassword([]byte(s+salt), bcrypt.MinCost)
	if err != nil {
		log.Panicln(err)
	}
	fmt.Println(string(bs)) //$2a$04$ouUbPUhtN8nTetJ8xvsq..YoEusADnVDqPyrtJj9DiO8H/.vMFOuu
	//Compare Hashes
	loginPassword := "Password12345"
	err = bcrypt.CompareHashAndPassword(bs, []byte(loginPassword+salt))
	fmt.Println(err)
}

```
<a id="concurrency" name="concurrency"></a>

# Concurrency 
[Up](#contnent)
```go
Go Routines -
При запуске нашей программы мы автоматически запускаеи главную go рутину - main go routine.
Чтоб запустить новую go рутину необходимо воспользоваться оператором go внутри кода.
runtime.Gosched() - передать управление другой горутине.
```
<a id="waitgroup" name="waitgroup"></a>

## WaitGroup 
[Up](#contnent)
```go
package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	wg := &sync.WaitGroup{} //создаем указатель чтоб проще было передавать
	fmt.Println("OS:\t\t", runtime.GOOS)
	fmt.Println("ARCH:\t\t", runtime.GOARCH)
	fmt.Println("CPUs:\t\t", runtime.NumCPU())
	fmt.Println("Goroutines:\t", runtime.NumGoroutine())
	wg.Add(1) //добавляем счетчик до запуска горутины
	go foo(wg)
	bar()
	fmt.Println("CPUs:\t\t", runtime.NumCPU())
	fmt.Println("Goroutines:\t", runtime.NumGoroutine())
	wg.Wait()
}

func foo(wg *sync.WaitGroup) {
	for i := 0; i < 10; i++ {
		fmt.Println("foo:", i)
	}
	wg.Done()
}

func bar() {
	for i := 0; i < 10; i++ {
		fmt.Println("bar:", i)
	}
}


```

<a id="racecondition" name="racecondition"></a>

## Race condition 
[Up](#contnent)
```go
package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("Goroutins:", runtime.NumGoroutine())
	counter := 0
	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)
	for i := 0; i < gs; i++ {
		go func() {
			v := counter
			// time.Sleep(time.Second)
			runtime.Gosched()
			v++
			counter = v
			wg.Done()
		}()

	}
	wg.Wait()
	fmt.Println("Goroutins:", runtime.NumGoroutine())
	fmt.Println("counter:", counter)
}
```
Теперь что бы проверть есть ли race condition, нужно запустить\
go run --race man.go


<a id="mutex" name="mutex"></a>

## Mutex 
[Up](#contnent)
```go
package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("Start Goroutins:", runtime.NumGoroutine())
	counter := 0
	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)

	var mu sync.Mutex

	for i := 0; i < gs; i++ {
		go func() {
			mu.Lock()
			v := counter
			// time.Sleep(time.Second)
			runtime.Gosched()
			v++
			counter = v
			mu.Unlock()
			wg.Done()
		}()
		fmt.Println("Middle Goroutins:", runtime.NumGoroutine())
	}
	wg.Wait()
	fmt.Println("End Goroutins:", runtime.NumGoroutine())
	fmt.Println("counter:", counter)
}

```
### sync.Mutex: паттерны использования
помещайте мьютекс выше тех полей доступ к которым он будет защищать
```go
var sum struct {
		sync.Mutex	// <-- этотмьютексзащищает    
		i int		// <-- полеподним
}
```
держите блокировку не дольше, чем требуется
```go
func doSomething(){
	mu.Lock()
	item:=cache["myKey"]
	http.Get() // какой нибудь дорогой io вызов
	mu.Unlock
}
```
используйте defer чтобы разблокировать мьютекс там где у функции есть несколько точек выхода
```go
func doSomething(){
	mu.Lock()
	defer mu.Unlock
	err:=...
	if err !=nil{
		//
		return err
	}
	...
	err=...
	if err !=nil{
		//
		return err
	}
	
}
```
Когда использовать каналы:
* передача данныx 
* распределение вычислений 
* передача асинхронных результатов
 
Когда использовать мьютексы:
* кэши
* состояния

<a id="rwmutex" name="rwmutex"></a>

## RWMutex 
[Up](#contnent)

```go
RLock/RUnlock - блокировка на чтение(другие горутины читают но не могут писать)
```

<a id="syncmap" name="syncmap"></a>

## sync.Map 
[Up](#contnent)

Если у вас высоконагруженная (и 100нсрешают) система с большим количеством ядер процессора (32+), вы можете захотеть использовать sync.Map вместо стандартного map+sync.RWMutex. В остальных случаях, sync.Map особо не нужен.\
https://www.youtube.com/watch?v=C1EtfDnsdDs


<a id="syncpool" name="syncpool"></a>

## sync.Pool 
[Up](#contnent)

Pool - хранилище временных объектов, безопасное для использования несколькими горутинами. Выделение
памяти - это достаточно дорогая операция по сравнению со всем остальным. Поэтому
иногда для ускорения хочется не выделять новую память каждый раз, а переиспользовать какой-то пул
памяти.

```go
type Pool struct {
	//New - фения, которая возвращает значение, если Get() возвращает nil
	New func() intergfce{}
}
func (p *Pool) Get( )interface{}
func (p *Pool) Put(x interface{})
```

```go
type Dog struct{}

var dogPool = sync.Pool{
	New: func() interface{} {
		return &Dog{}
	},
}
...
dog := dogPool.Get().(*Dog) //достаем из пула
//somethong do...
dog.Reset() // обнуляем
dogPool.Put(dog) //возвращаем в пул
```

<a id="synconce" name="synconce"></a>

## sync.Once 
[Up](#contnent)

Do вызывает функцию f только в том случае, если это первый вызов Do для этого экземпляра Once. Другими словами, если у нас есть <code> var once Once </code> и <code>once.Do(f)</code> будет вызываться несколько раз, f выполнится только в момент первого вызова, даже если f будет иметь каждый раз другое значение. Для вызова нескольких функций таким способом нужно несколько экземпляров Once. 

Do предназначен для инициализации, которая должна выполняться единожды Так как f ничего не возвращает, может быть необходимым использовать замыкание для передачи параметров в функцию, выполняемую Do:
<code>config.once.Do(func() {conog.init(olename) }) </code>

Поскольку ниодин вызовк Do не завершится пока не произойдет первый вызов f, то f может заблокировать последующие вызовы Do и получится дедлок.Если f паникует, то Do считает это обычнымвы зовоми, при последующих вызовах, Do не будет вызывать f
```go
type oldDog struct{
	name string
	die sync.Once
}
func (d *oldDog) Die(){
	d.die.Do(func(){
		fmt.Println("bye!")
	})
}

func main(){
	d := oldDog{name:"Bob"}
	d.Die() // bye
	d.Die() // not called
	d.Die() // not called
}
```

<a id="synccond" name="synccond"></a>

## sync.Cond 
[Up](#contnent)

```go
```

<a id="atomic" name="atomic"></a>

## Atomic 
[Up](#contnent)
```go
package main

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
)

func main() {
	fmt.Println("Start Goroutins:", runtime.NumGoroutine())
	var counter int64
	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)

	for i := 0; i < gs; i++ {
		go func() {
			atomic.AddInt64(&counter, 1)
			fmt.Println(atomic.LoadInt64(&counter))
			runtime.Gosched()
			wg.Done()
		}()
		fmt.Println("Middle Goroutins:", runtime.NumGoroutine())
	}
	wg.Wait()
	fmt.Println("End Goroutins:", runtime.NumGoroutine())
	fmt.Println("counter:", counter)
}
```

<a id="gocurtt" name="gocurtt"></a>

## Timers and timeouts
[Up](#contnent)

Исп. когда необходимо выполнять, какие-то операции через небольшой промежуток времени или каждый промежуток времени
```go
//Example Timeout

//time.After(Duration) - возвращает канал, в который запишится текущее время через промежуток времени Duration
//timer=time.NewTimer(Duration) - возвтращает структуру с полем C, которое является каналом.
//т.е. в канал timer.C будет записано текущее время черезе промежуток времени Duration

package main

import (
	"fmt"
	"time"
)

func longSQLQuery() chan bool {
	ch := make(chan bool, 1)
	go func() {
		time.Sleep(2 * time.Second)
		ch <- true
	}()
	return ch
}

func main() {
	// при 1 выполнится таймаут, при 3 - выполнится операция
	timer := time.NewTimer(2 * time.Second)
	select {
	case v := <-timer.C:
		fmt.Println("timer.C timeout happend", v)
	case <-time.After(time.Minute):
		// пока не выстрелит - не соберётся сборщиком мусора
		fmt.Println("time.After timeout happend")
	case result := <-longSQLQuery():
		// освобождет ресурс
		if !timer.Stop() {
			<-timer.C
		}
		fmt.Println("operation result:", result)
	}
}

//Example Timers
//ticker = time.NewTicker(d Duration)-  возвтращает структуру с полем C, которое является каналом.
//т.е. в канал ticker.C будет записано текущее время каждый промежуток времени Duration
//ticker.Stop() - останавливаем тикер
//c = time.Tick(time.Second) - это алиас для time.NewTicker котороый сразу возвращает канал, а не структуру.
//Но данный тикер уже не возможно остановить. Используется в месте где тикер должен работать вечно
package main

import (
	"fmt"
	"time"
)

func main() {
	ticker := time.NewTicker(time.Second)
	i := 0
	for tickTime := range ticker.C {
		i++
		fmt.Println("step", i, "time", tickTime)
		if i >= 5 {
			// надо останавливать, иначе потечет
			ticker.Stop()
			break
		}
	}
	fmt.Println("total", i)

	// не может быть остановлен и собран сборщиком мусора
	// используйте если должен работать вечено
	c := time.Tick(time.Second)
	i = 0
	for tickTime := range c {
		i++
		fmt.Println("step", i, "time", tickTime)
		if i >= 5 {
			break
		}
	}

}

//еще один пример использования timeout
//time. AfterFunc(d Duration, f func()) *Timer - выполнит фунию f через Duration. Возвращает объект типа Timer, чтоб можно было  остановить таймер

package main

import (
	"fmt"
	"time"
)

func sayHello() {
	fmt.Println("Hello World")
}

func main() {
	_ = time.AfterFunc(1*time.Second, sayHello)

	// fmt.Scanln()
	// timer.Stop()

	fmt.Scanln()
}

```

<a id="channels" name="channels"></a>

# Channels
[Up](#contnent)
```go
/*
Channels - позволяют коммуницировать между go рутинами. Каналы тоже типизированы, это означает,
что только определенный тип данных может передваться по каналам.
Шаблон: 
nameChannel:= make(chan typeChannel)
ch := make(chan string)
пишем в канал: ch <- "какой-то текст" 
читаем из канала(blocking call): 
 - в переменную: var <- ch
 - на экран: fmt.Println(<-ch) 
*/
//Best practise: Закрыватьканалдолжнапишущаягорутина, либосоздатель!
//unbuffer
package main

import "fmt"

func main() {
	ch := make(chan int)
	go func() {
		ch <- 1984
	}()

	fmt.Println(<-ch)
}

//ok idiom
package main

import "fmt"

func main() {
	ch := make(chan int)

	go func() {
		ch <- 1984
		close(ch)
	}()
	v, ok := <-ch
	fmt.Println(v, ok)
	fmt.Println(<-ch)
}


//buffer
package main

import (
	"fmt"
)

func main() {
	ch := make(chan int, 1)
	ch <- 1984
	fmt.Println(<-ch)
}

//buffer
package main

import (
	"fmt"
)

func main() {
	ch := make(chan int, 2)
	ch <- 1984
	ch <- 2033
	fmt.Println(<-ch)
	fmt.Println(<-ch)
}

```

<a id="dirchannel" name="dirchannel"></a>

## Direction channel 
[Up](#contnent)
```go
//в фунии можно передавать каналы аргументом только для чтения или записи
package main

import "fmt"

func main() {
	ch := make(chan int)
	//send
	go foo(ch)
	//receive
	bar(ch)
	fmt.Println("Done")
}

func foo(ch chan<- int) {
	ch <- 1984
}

func bar(ch <-chan int) {
	fmt.Println(<-ch)
}

```
<a id="range" name="range"></a>

## Range 
[Up](#contnent)
```go
/*
если необходимо считать из канала опред колво данных, то можно исп range и обязательно закрыть канал - close(ch)
*/
package main

import "fmt"

func main() {
	ch := make(chan int)
	//send
	go foo(ch)
	//recieve
	for v := range ch {
		fmt.Println(v)
	}
	fmt.Println("Done")
}

func foo(ch chan<- int) {
	for i := 0; i <= 10; i++ {
		ch <- i
	}
	close(ch)
}
```
<a id="selectcase" name="selectcase"></a>

## Select Case 
[Up](#contnent)
```go
package main

import "fmt"

func main() {
	eve := make(chan int)
	odd := make(chan int)
	quite := make(chan bool)

	//send
	go send(eve, odd, quite)
	//receive
	receive(eve, odd, quite)
	fmt.Println("Done")
}

func receive(e, o <-chan int, q <-chan bool) {
	for {
		select {
		case v := <-e:
			fmt.Println("eve channel:", v)
		case v := <-o:
			fmt.Println("odd channel:", v)
		case i, ok := <-q:
			if !ok {
				fmt.Println("quite channel:", i, ok)
				return
			} else {
				fmt.Println("quite channel:", i)
			}

		}
	}
}

func send(e, o chan<- int, q chan<- bool) {
	for i := 0; i <= 100; i++ {
		if i%2 == 0 {
			e <- i
		} else {
			o <- i
		}
	}
	close(q)
}

```

<a id="context" name="context"></a>

## Context
[Up](#contnent)

[Go Concurrency Patterns: Pipelines and cancellation](https://blog.golang.org/pipelines)\
[Изучаем net/context в Go](https://habr.com/ru/post/269299/)\
Одно из основных предназначаний пакета context - это отмена асинхроных операций.\
Пакет context определяет тип Context, при помощи которого можно контролировать процесс выполнения горутин (задавать таймауты, дедлайны, послать сигналы отмены или передать параметры запроса). Разработчики из Google рекомендуют передавать его первым параметром в вызовы, которые могут занять время или требуют возможности прерывания. \
DeadLine отличается от WithTimeout - тем что в WithTimeout указывается интервал вермени, а в DeadLine точное время.
```go
//Example Cancel
package main

import (
	"context"
	"fmt"
	"math/rand"
	"time"
)

func worker(ctx context.Context, workerNum int, out chan<- int) {
	waitTime := time.Duration(rand.Intn(100)+10) * time.Millisecond
	fmt.Println(workerNum, "sleep", waitTime)
	select {
	case <-ctx.Done():
		return
	case <-time.After(waitTime):
		fmt.Println("worker", workerNum, "done")
		out <- workerNum
	}
}

func main() {
	ctx, finish := context.WithCancel(context.Background())
	result := make(chan int, 1)

	for i := 0; i <= 10; i++ {
		go worker(ctx, i, result)
	}

	foundBy := <-result
	fmt.Println("result found by", foundBy)
	finish()

	time.Sleep(time.Second)
}
```
```go
//Example Timeout
package main

import (
	"context"
	"fmt"
	"math/rand"
	"time"
)

func worker(ctx context.Context, workerNum int, out chan<- int) {
	waitTime := time.Duration(rand.Intn(100)+10) * time.Millisecond
	fmt.Println(workerNum, "sleep", waitTime)
	select {
	case <-ctx.Done():
		return
	case <-time.After(waitTime):
		fmt.Println("worker", workerNum, "done")
		out <- workerNum
	}
}

func main() {
	workTime := 50 * time.Millisecond
	ctx, _ := context.WithTimeout(context.Background(), workTime)
	result := make(chan int, 1)

	for i := 0; i <= 10; i++ {
		go worker(ctx, i, result)
	}

	totalFound := 0
LOOP:
	for {
		select {
		case <-ctx.Done():
			break LOOP
		case foundBy := <-result:
			totalFound++
			fmt.Println("result found by", foundBy)
		}
	}
	fmt.Println("totalFound", totalFound)
	time.Sleep(time.Second)
}


```

<a id="asyncgetdata" name="asyncgetdata"></a>

## Асинхронное получение данных 
[Up](#contnent)
```go
package main

import (
	"fmt"
	"time"
)

func getComments() chan string {
	// надо использовать буферизированный канал
	result := make(chan string, 1)
	go func(out chan<- string) {
		time.Sleep(2 * time.Second)
		fmt.Println("async operation ready, return comments")
		out <- "32 комментария"
	}(result)
	return result
}

func getPage() {
	resultCh := getComments()

	time.Sleep(1 * time.Second)
	fmt.Println("get related articles")

	// return

	commentsData := <-resultCh
	fmt.Println("main goroutine:", commentsData)
}

func main() {
	for i := 0; i < 3; i++ {
		getPage()
	}
}
```

<a id="workerpool" name="workerpool"></a>

## Пул воркеров 
[Up](#contnent)

Данный пример показывает как можно дилегировать работу пулу воркеров
```go
package main

import (
	"fmt"
	"runtime"
	"strings"
	"time"
)

const goroutinesNum = 3

func startWorker(workerNum int, in <-chan string) {
	for input := range in {
		fmt.Printf(formatWork(workerNum, input))
		runtime.Gosched() // попробуйте закомментировать
	}
	printFinishWork(workerNum)
}

func main() {
	runtime.GOMAXPROCS(0)               // попробуйте с 0 (все доступные) и 1
	worketInput := make(chan string, 2) // попробуйте увеличить размер канала
	for i := 0; i < goroutinesNum; i++ {
		go startWorker(i, worketInput)
	}

	months := []string{"Январь", "Февраль", "Март",
		"Апрель", "Май", "Июнь",
		"Июль", "Август", "Сентябрь",
		"Октябрь", "Ноябрь", "Декабрь",
	}

	for _, monthName := range months {
		worketInput <- monthName
	}
	close(worketInput) // попробуйте закомментировать

	time.Sleep(time.Millisecond)
}

func formatWork(in int, input string) string {
	return fmt.Sprintln(strings.Repeat("  ", in), "█",
		strings.Repeat("  ", goroutinesNum-in),
		"th", in,
		"recieved", input)
}

func printFinishWork(in int) {
	fmt.Println(strings.Repeat("  ", in), "█",
		strings.Repeat("  ", goroutinesNum-in),
		"===", in,
		"finished")
}

```
<a id="ratelimit" name="ratelimit"></a>

## Ограничение по ресурсам
[Up](#contnent)

```go
```

<a id="error-handling" name="error-handling"></a>

# Error Handling 
[Up](#contnent)

Тип error это интерфейс. Т.е. если создать свой тип (например struct) и добавть данному типу метод Error возвращающий строку. То наш типа также будет error типом.
```go
type error interface {
    Error() string
}
```
В Go всегда нужно обрабатывать ошибку
```go
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	f, err := os.Open("filename")
	if err != nil {
		fmt.Println(err) //open filename: no such file or directory
		return
	}

	bs, err := ioutil.ReadAll(f)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(bs))
}
```
Ошибки можно отображать и логирвать разными способами
```go
fmt.Println(err) // print err
log.Println(err) // print err with time
log.Fatalln(err) // print err with time and complete the programm(os.Exit())
log.Panicln(err) // defer func run, can use "recover"
panic()
```
Пример использования <code>errors.New()</code>\
<code>errors.New()</code> принимет аргументом строку и возвращает аргумент типа errorString. Данный тип реализует тип error interface{}
```go
package main

import (
	"errors"
	"log"
)

func main() {
	_, err := foo()
	if err != nil {
		log.Println(err)
	}
}

func foo() (string, error) {
	return "", errors.New("my error happaned")
}
```

<a id="writing-documentaion" name="writing-documentaion"></a>

# Writing documentaion 
[Up](#contnent)

https://golang.org/ - содержит инфу только по стандартным пакетам\
https://godoc.org/ - помимо страндартныйх пакетов содержит еще инфу о пакетах третьих лиц
```go
go help doc
go doc - display documentaion of current package you are in folder
go doc <pkg> - display doc of pkg
go doc fmt
go doc <pkg>.<method>
go doc fmt.Println
```

```go
godoc -http=:8080 //run local server golang.org включая документацию пакетов расположенных в GOPATH
godoc <pkg>
godoc fmt
godoc <pkg> <method>
godoc fmt Println
```
Чтобы писать документацию к пакетам достаточно обавлять коментарии в нужных местах.\
Но необхоодимо придерживтаься следующих соглашений:
1. После // должен быть пробел
2. Коментарий к публичномк методу начинается с имени этого метода
3. Коментарий к пакету начинатся с  Package имя_пакета
```go
cat testpkg.go

// Package testpkg
// some documentaion
// for my test package
package testpkg

// TestFunc documentation should be here
func TestFunc(){}

```
проверяем
```go
//go doc testpkg
package testpkg // import "testpkg"

Package testpkg some documentaion for my test package

func TestFunc()



// go doc testpkg.Testfunc
package testpkg // import "testpkg"

func TestFunc()
    TestFunc documentation should be here


```
Если необходимо добавить большое описание к пакету, то можно исп файл doc.go, который располагается в корне пакета
```go
cat doc.go

/*
Bla1
Bla2
Bla3
*/
package main
```
проверяем
```go
//go doc
//Bla1 Bla2 Bla3
```



<a id="testing--benchmarking" name="testing--benchmarking"></a>

# Testing & Benchmarking
[Up](#contnent)

https://godoc.org/testing\

Unit Tests - тестирование очень маленьких частей программы, как например одну фунию/метод/тип/аргумент в изоляции от работы с остальными частями программы.

Integration Tests - тестируют как две и более системы/пакеты работают друг с другом.

End-to-end - тестируется работа приложения целиком или большая ее часть. Граница между E2E и Integration Tests достаточно размытая. Грубо можно сказать что E2E - это тесты того как с приложением будт работать пользлватель (хотя это не обязятельное условие).

Также тесты бывают internal и external.\
internal - когда мы тестируем фунии/методы/типы/переменные в рамках тогоже пакета.\
external - кода мы тестируем фунии/методы/типы/переменные пакет в рамках другого. т.е. как бы имортируем пакет и тестируем его публично доступные элементы. Если необходимо протестировать приватные елементы в  external тестах, то можно исп следующий финт ушами.\
Допуттип уна симеется пакет со следующим содержимым
```go
package draw

var a = 123

type dog struct{
	name string
	id int
}

func sayHello() string{
	return "Hello"
}

func doDog(d dog) string{
	return "name="+d.name+" id="+d.id
}
```
чтоб протестировать такой пакет из вне необходимо создать фалй <code>external_test.go</code>. Данный файл позволяет сделать приватные элементы пакета публичными, но только для тестов.
```go
package draw
var A=a
var SayHello=sayHello
type Dog = dog
var DoDog = doDog
```
Теперь файл с external тестами будет выглядить так
```go
package draw_test
import draw

func TestA(t *testing.T){
	got := draw.A
	...
}

func TestSayHello(t *testing.T){
	got := draw.SayHello()
	...
}

func TestDog(t *testing.T){
	got := draw.Dog{} //правда поля структуры мы задать не сможем, они остаются приватные
	...
}

func TestDoDog(t *testing.T){
	d := draw.Dog{}
	got := d.DoDog()
}
```

```
XXXX_test.go => external tests (package name - package_test)
XXXX_internal_test.go => internal tests
export_test.go => is used export unexported stuff
```

Тесты должны:
1. быть в файле именуемом как "_test.go"
2. находится в том же пакете, что тестируемый пакет
3. быть в фунии с сигнатутрой ​ "func TestXxx(t *testing.T)"
4. если тестируется метод то фун-ия тестер имеет должна името след имя - TestTypaName_MethodName
   ```go
   func (d *Dog) Talk(){}

   func TestDog_Talk(t *testing.t){}
   ```
5. переменные отвечающе за ответ и правельный(ожидаемый) результат именовать как: got и want
6. если тестируется работа фунии с определенным аргументом то тестирующая фуния именуется, как TestFuncName_arg


Запуск теста : <code>go test</code>\
main.go
```go
package main

import "fmt"

func main() {
	fmt.Println("2 + 3 =", sum(2, 3))
}

func sum(xi ...int) int {
	total := 0
	for _, v := range xi {
		total += v
	}
	return total
} 
```
main_test.go
```go
package main

import "testing"

func TestSum(t *testing.T) {
	s := sum(2, 3)
	if s != 5 {
		t.Error("Expected:", 5,"Got:", s)
	}
}
```
<code>go test</code>
```go
PASS
ok  	golangBook/basic	0.001s
```

<code>go clean -testcache</code> удалить закешированные результаты тестов


testing.T имеет методы Log и Logf - аналог fmt.Println и fmt.Printf\
Существует два пути сигнализирующих, что тест провален:
1. Fail - тест провален, но продолжает работать
2. FailNow - тест провален и остановлен


Но для описания вышеупомянутых поведений чаще используются методы:
1. Error = Log + Fail
2. Errorf = Logf + Fail
3. Fatal = Log + FailNow
4. Fatalf = Logf + FailNow

Если после провала теста необходимо продолжить работу то исп Error/Errorf. Если необходимо остановить тест после провала,то исп Fatal/Fatalf

Чтоб увидеть сообщения t.Log и t.Logf исп команду <code>go test -v</code>

<a id="tabledriventests" name="tabledriventests"></a>

## Table driven tests
[Up](#contnent)

Это дизайн патер тестирования, позволяющий сделать тест не с одним значением/аргументом/параметром, а использовать несколько возможных вариантов.\

Table tests (multiple tests) - несколько проверок
```go
package main

import "testing"

func TestSum(t *testing.T) {
	type test struct {
		data  []int
		total int
	}

	tests := []test{
		test{[]int{1, 1}, 2},
		test{[]int{-1, 0, 1}, 0},
		test{[]int{1, 2, 3, 4, 5}, 15},
		test{[]int{99, 1}, 100},
		test{[]int{1983, 1}, 1984},
	}
	for _, v := range tests {
		s := sum(v.data...)
		if s != v.total {
			t.Error("Expected:", v.total, "Got:", s)
		}
	}
}
```

Напишем тест для такой фунии Camel
```go
package underscore

import (
	"strings"
)

func Camel(str string) string {
	var sb strings.Builder
	for _, ch := range str {
		if ch >= 'A' && ch <= 'Z' {
			sb.WriteRune('_')
		}
		sb.WriteRune(ch)
	}
	return strings.ToLower(sb.String())
}
```

```go
package underscore

import "testing"

func TestCamel(t *testing.T) {
	testCase := []struct {
		arg  string
		want string
	}{
		{"thisIsACamelCaseString", "this_is_a_camel_case_string"},
		{"with a space", "with a space"},
		{"endsWithA", "ends_with_a"},
	}

	for _, tc := range testCase {
		got := Camel(tc.arg)
		if got != tc.want {
			t.Errorf("Camel(%q) = %q; want=%q", tc.arg, got, tc.want)
		}
	}

}
```
В vs code есть читинг по созданию тестов.
Выделяем нужную фунию, нажимаем правую кнопку мыши и выбираем Go: Generate Unit Tests for Function.\
[gotests](https://github.com/cweill/gotests)\
После такой генерации мв получаем файл c фунией, где нужно просто заполнить структуру
```go
func TestCamel(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		//заполняем структуру вариантомаи тестов
		{"simpel", args{"thisIsACamelCaseString"}, "this_is_a_camel_case_string"},
		{"spaces", args{"with a space"}, "with a space"},
		{"ends with capital", args{"endsWithA"}, "ends_with_a"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Camel(tt.args.str); got != tt.want {
				t.Errorf("Camel() = %v, want %v", got, tt.want)
			}
		})
	}
}
```

<a id="subtests" name="subtests"></a>

## Subtests
[Up](#contnent)

метод [t.Run](https://godoc.org/testing#T.Run) позволяте запускать под тесты, см пример выше. Одним из приемуществ использования под тестов является возможнсть использования t.Fatalf - которая по своему обыкновению не остановит весь тест, а только один под тест. Что дает приемущества в использовании в паралельном тестировании.

<a id="testmain" name="testmain"></a>

## Test Main
[Up](#contnent)

В некоторых случаях необходимо перед запуском тестов выполнить дополнительную настройку или выполнить какую либо переконфигурацию до или после тестов.
Для таких случаев используется следующаф конструкция. 
```go
func TestMain(m *testing.M){
	//0. flag.Parse() - если необходимо исп флаги для заупска тестов
	//1. произвети необходимые настройки
	//2. запусе тестов
	exitCode:=m.Run()
	os.Exit(exitCode) //Есть ограничене использование defer с os.Exit - defer никогда не выполнится. Пример https://play.golang.org/p/I2EMkoidLT8 
	//т.е. если в пункте 1 етсьт defer, то он не запустится. Для этого можно всё то в пункте 1 вынести в отдельную фунию как:
	//func run( m*testing.M){
	//	...
	//	...
	// return m.Run()
	//}
	// и тогда пункт 2 выгдлдит так
	// exitCode:=run(m)
	// os.Exit(exitCode)
}
```

<a id="testinparallel" name="testinparallel"></a>

## Tests in Parallel
[Up](#contnent)

В Go есть возможность запускать тесты gаралельно. Для это нужно воспользоваться методом <code>t.Parallel()</code>, который необходимо разместить в самом верху фунии.\
Пусть у нас есть следующая последовательность тестов. Время выполнения данных тестов займе 3 сек.
```go
package parallel

import (
	"testing"
	"time"
)

func TestSomthing(t *testing.T) {
	// t.Parallel()
	time.Sleep(time.Second)
}

func TestA(t *testing.T) {
	// t.Parallel()
	time.Sleep(time.Second)
}

func TestB(t *testing.T) {
	// t.Parallel()
	time.Sleep(time.Second)
}
```
```go
=== RUN   TestSomthing
--- PASS: TestSomthing (1.00s)
=== RUN   TestA
--- PASS: TestA (1.00s)
=== RUN   TestB
--- PASS: TestB (1.00s)
PASS
ok  	testWithGo/parallel	3.004s
```

Но если сейчас добавить <code>t.Parallel</code>, то выполнение всех тестов зайимт 1 сек
```go
package parallel

import (
	"testing"
	"time"
)

func TestSomthing(t *testing.T) {
	t.Parallel()
	time.Sleep(time.Second)
}

func TestA(t *testing.T) {
	t.Parallel()
	time.Sleep(time.Second)
}

func TestB(t *testing.T) {
	t.Parallel()
	time.Sleep(time.Second)
}
```
```go
=== RUN   TestSomthing
=== PAUSE TestSomthing
=== RUN   TestA
=== PAUSE TestA
=== RUN   TestB
=== PAUSE TestB
=== CONT  TestSomthing
=== CONT  TestA
=== CONT  TestB
--- PASS: TestB (1.00s)
--- PASS: TestA (1.00s)
--- PASS: TestSomthing (1.00s)
PASS
ok  	testWithGo/parallel	1.002s
```
т.е. <code>t.Parallel()</code> позволяет запустить тесты асинхронно. Тест без использования <code>t.Parallel()</code>  будет запущен последовательно.

<a id="subtestinparallel" name="subtestinparallel"></a>

## Subtests in Parallel
[Up](#contnent)

```go
package parallel

import (
	"testing"
	"time"
)

func TestA(t *testing.T) {
	t.Parallel()
	time.Sleep(time.Second)
}

func TestB(t *testing.T) {
	t.Parallel()
	time.Sleep(time.Second)
}

func TestC(t *testing.T) {
	t.Parallel()
	t.Run("sunC1", func(t *testing.T) {
		t.Parallel()
	})
	t.Run("sunC2", func(t *testing.T) {
		t.Parallel()
	})
}
```
```go
=== RUN   TestA
=== PAUSE TestA
=== RUN   TestB
=== PAUSE TestB
=== RUN   TestC
=== PAUSE TestC
=== CONT  TestA
=== CONT  TestC
=== CONT  TestB
=== RUN   TestC/sunC1
=== PAUSE TestC/sunC1
=== RUN   TestC/sunC2
=== PAUSE TestC/sunC2
=== CONT  TestC/sunC1
=== CONT  TestC/sunC2
--- PASS: TestC (0.00s)
    --- PASS: TestC/sunC1 (0.00s)
    --- PASS: TestC/sunC2 (0.00s)
--- PASS: TestA (1.00s)
--- PASS: TestB (1.00s)
PASS
ok  	testWithGo/parallel	1.002s
```

<a id="stsubtestinparallel" name="stsubtestinparallel"></a>

### Setup&Teardown with subtests in Parallel
[Up](#contnent)
пусть имеется следющий тест

```go
func TestC(t *testing.T) {
	t.Logf("setup")
	defer t.Logf("defer teardown")
	t.Run("subC1", func(t *testing.T) {
		t.Parallel()
		time.Sleep(time.Second)
		t.Logf("subC1 done")
	})
	t.Run("subC2", func(t *testing.T) {
		t.Parallel()
		time.Sleep(time.Second)
		t.Logf("subC2 done")
	})
	t.Logf("teardown")
}
```
Но он работает в не той последовательноти как ожидается
```go
=== RUN   TestC
=== RUN   TestC/subC1
=== PAUSE TestC/subC1
=== RUN   TestC/subC2
=== PAUSE TestC/subC2
=== CONT  TestC/subC1
=== CONT  TestC/subC2
--- PASS: TestC (0.00s)
    parallel_test.go:19: setup
    parallel_test.go:31: teardown
    parallel_test.go:32: defer teardown
    --- PASS: TestC/subC2 (1.00s)
        parallel_test.go:29: subC2 done
    --- PASS: TestC/subC1 (1.00s)
        parallel_test.go:24: subC1 done
PASS
ok  	testWithGo/parallel	1.003s

```
т.е. нужно чтоб subC2 done и subC1 done вывелись до teardown и defer teardown\
для это необходимо все подтесты поместить в еще один общий подтест, но запустить его не рпралельно
```go
func TestC(t *testing.T) {
	t.Logf("setup")
	defer t.Logf("defer teardown")
	t.Run("main Sub", func(t *testing.T) {
		t.Run("subC1", func(t *testing.T) {
			t.Parallel()
			time.Sleep(time.Second)
			t.Logf("subC1 done")
		})
		t.Run("subC2", func(t *testing.T) {
			t.Parallel()
			time.Sleep(time.Second)
			t.Logf("subC2 done")
		})
	})
	t.Logf("teardown")
}
```
теперь результат будет как и ожидалось
```go
=== RUN   TestC
=== RUN   TestC/main_Sub
=== RUN   TestC/main_Sub/subC1
=== PAUSE TestC/main_Sub/subC1
=== RUN   TestC/main_Sub/subC2
=== PAUSE TestC/main_Sub/subC2
=== CONT  TestC/main_Sub/subC1
=== CONT  TestC/main_Sub/subC2
--- PASS: TestC (1.00s)
    parallel_test.go:19: setup
    --- PASS: TestC/main_Sub (0.00s)
        --- PASS: TestC/main_Sub/subC2 (1.00s)
            parallel_test.go:30: subC2 done
        --- PASS: TestC/main_Sub/subC1 (1.00s)
            parallel_test.go:25: subC1 done
    parallel_test.go:33: teardown
    parallel_test.go:34: defer teardown
PASS
ok  	testWithGo/parallel	1.002s
```
<a id="gotchassubtestinparallel" name="gotchassubtestinparallel"></a>

### Gotchas with subtests in Parallel
[Up](#contnent)

Если запускать подтесты паралельно в цикле, то имеется одна проблема - не происходит копирования значений в подтест из цикла.
Пример
```go
func TestC(t *testing.T) {

	for i := 0; i < 10; i++ {
		t.Run(fmt.Sprintf("test%v", i), func(t *testing.T) {
			t.Parallel()
			t.Logf("i:%v", i)
		})
	}
}
```
результат такой
```go
    --- PASS: TestC/test0 (0.00s)
        parallel_test.go:23: i:10
    --- PASS: TestC/test9 (0.00s)
        parallel_test.go:23: i:10
    --- PASS: TestC/test8 (0.00s)
        parallel_test.go:23: i:10
    --- PASS: TestC/test4 (0.00s)
        parallel_test.go:23: i:10
    --- PASS: TestC/test3 (0.00s)
        parallel_test.go:23: i:10
    --- PASS: TestC/test2 (0.00s)
        parallel_test.go:23: i:10
    --- PASS: TestC/test1 (0.00s)
        parallel_test.go:23: i:10
    --- PASS: TestC/test6 (0.00s)
        parallel_test.go:23: i:10
    --- PASS: TestC/test7 (0.00s)
        parallel_test.go:23: i:10
    --- PASS: TestC/test5 (0.00s)
        parallel_test.go:23: i:10
```
как видим везде <code>i:10</code>\
Одним из решений является определение новой переменной где нибудь до <code>t.Parallel()</code>
```go
func TestC(t *testing.T) {
	for i := 0; i < 10; i++ {
		//j := i //<-- либо здесь
		t.Run(fmt.Sprintf("test%v", i), func(t *testing.T) {
			j := i //<-- либо здесь 
			t.Parallel()
			t.Logf("i:%v", j)
		})
	}
}
```
Результат
```go
    --- PASS: TestC/test0 (0.00s)
        parallel_test.go:25: i:0
    --- PASS: TestC/test9 (0.00s)
        parallel_test.go:25: i:9
    --- PASS: TestC/test5 (0.00s)
        parallel_test.go:25: i:5
    --- PASS: TestC/test6 (0.00s)
        parallel_test.go:25: i:6
    --- PASS: TestC/test4 (0.00s)
        parallel_test.go:25: i:4
    --- PASS: TestC/test3 (0.00s)
        parallel_test.go:25: i:3
    --- PASS: TestC/test2 (0.00s)
        parallel_test.go:25: i:2
    --- PASS: TestC/test7 (0.00s)
        parallel_test.go:25: i:7
    --- PASS: TestC/test1 (0.00s)
        parallel_test.go:25: i:1
    --- PASS: TestC/test8 (0.00s)
        parallel_test.go:25: i:8
```


<a id="runspecifictest" name="runspecifictest"></a>

## Run specific Test
[Up](#contnent)

Определенный тест можно запустить с помощью ключа <code>-run</code>\
Допустим у нас имеются следующие тесты

```go
func TestA(t *testing.T) {
	time.Sleep(time.Second)
}

func TestB(t *testing.T) {
	time.Sleep(time.Second)
}

func TestC(t *testing.T) {
	time.Sleep(time.Second)
}
```
Тогда
```go
go test -v -run TestA
=== RUN   TestA
--- PASS: TestA (1.00s)
PASS
ok  	testWithGo/parallel	1.003s

go test -v -run TestB
=== RUN   TestB
--- PASS: TestB (1.00s)
PASS
ok  	testWithGo/parallel	1.003s

go test -v -run TestC
=== RUN   TestC
--- PASS: TestC (1.00s)
PASS
ok  	testWithGo/parallel	1.002s

```

Запустить тесты сразу для всех пакетов приложения можно командой <code>go test ./...</code>

<a id="skippingtest" name="skippingtest"></a>

## Skipping Test
[Up](#contnent)

если по каким либо причинам нужен функционал с пропусканием определенных тестов, то исп метод <code>t.Skip()</code>\
Пример
```go
func TestA(t *testing.T) {

	if testing.Short() {
		t.Skip()
	}

	t.Log("run test")
}
```
Здесь <code>testing.Short()</code> зависит от того передали мы ключ <code>-short</code> или нет. если внешнего управления для пропуска не требуется, то вместо <code>testing.Short()</code> можно исп любую булевую переменную.\
Запускаем тест без пропуска

```go
go test -v
=== RUN   TestA
--- PASS: TestA (0.00s)
    parallel_test.go:13: run test
PASS
ok  	testWithGo/parallel	0.001s
```
Теперь пропускаем тест
```go
go test -v -short
=== RUN   TestA
--- SKIP: TestA (0.00s)
    parallel_test.go:10: 
PASS
ok  	testWithGo/parallel	0.001s
```

Помимо такого способа включения/выключения тестов, еще можно использовать тэги.\
Пусть у нас имеется три файла с тестами\
main_test.go
```go
package skip

import (
	"testing"
)

func TestM(t *testing.T) {
	t.Log("run main test")
}
```
a_test.go
```go
package skip

import (
	"testing"
)

func TestA(t *testing.T) {
	t.Log("run test A")
}
```
b_test.go
```go
package skip

import (
	"testing"
)

func TestB(t *testing.T) {
	t.Log("run test B")
}
```
При запуске тестов они буду выполнятся все
```go
go test -v
=== RUN   TestA
--- PASS: TestA (0.00s)
    a_test.go:8: run test A
=== RUN   TestB
--- PASS: TestB (0.00s)
    b_test.go:8: run test B
=== RUN   TestM
--- PASS: TestM (0.00s)
    main_test.go:8: run main test
PASS
ok  	testWithGo/skip	0.002s
```
Если допусти необходимо отключить тест A и тест B, то  достаточно в каждый из файтов за указанными тестами добавить перед <code>package</code> строку типа <code>// +build tagName</code>
a_test.go
```go
// +build testA

package skip

import (
	"testing"
)

func TestA(t *testing.T) {
	t.Log("run test A")
}
```
b_test.go
```go
// +build testB

package skip

import (
	"testing"
)

func TestB(t *testing.T) {
	t.Log("run test B")
}
```
После добавления данных строк по дефолту будет выполнятся только TestM
```go
go test -v
=== RUN   TestM
--- PASS: TestM (0.00s)
    main_test.go:8: run main test
PASS
ok  	testWithGo/skip	0.001s
``` 
Если же теперь неоходимо вызвать один из тестов то нужно исп ключ <code>-tags</code>
```go
go test -v -tags=testA
=== RUN   TestA
--- PASS: TestA (0.00s)
    a_test.go:10: run test A
=== RUN   TestM
--- PASS: TestM (0.00s)
    main_test.go:8: run main test
PASS
ok  	testWithGo/skip	0.001s


go test -v -tags=testB
=== RUN   TestB
--- PASS: TestB (0.00s)
    b_test.go:10: run test B
=== RUN   TestM
--- PASS: TestM (0.00s)
    main_test.go:8: run main test
PASS
ok  	testWithGo/skip	0.001s

```

Если необходимо вызвать сразу несколко таких тестов то используем следюущую конструкцию
```go
go test -v -tags="testA testB"
=== RUN   TestA
--- PASS: TestA (0.00s)
    a_test.go:10: run test A
=== RUN   TestB
--- PASS: TestB (0.00s)
    b_test.go:10: run test B
=== RUN   TestM
--- PASS: TestM (0.00s)
    main_test.go:8: run main test
PASS
ok  	testWithGo/skip	0.001s
```




<a id="racecondition" name="racecondition"></a>

## Race condition
[Up](#contnent)

[https://blog.golang.org/race-detector](https://blog.golang.org/race-detector)

Чтоб обнаружить race condition достатоточно исп флаг <code>-race</code>
```go
$ go test -race mypkg    // test the package
$ go run -race mysrc.go  // compile and run the program
$ go build -race mycmd   // build the command
$ go install -race mypkg // install the package
```
Данный способ может проверит Race condition тольео если он случается в самом Go коде. Если Race condition случится с данными в БД,то данный флаг не отследит этого.



<a id="comparatitionintests" name="comparatitionintests"></a>

## Comparatition in Tests
[Up](#contnent)

[https://golang.org/ref/spec#Comparison_operators](https://golang.org/ref/spec#Comparison_operators)

```go
reflect.DeepEqual(struct1,struct2)
```
<a id="packagequick" name="packagequick"></a>

## Package quick
[Up](#contnent)

Пакет для тестирования рандомных св-в методов/фуний 
[https://golang.org/pkg/testing/quick/](https://golang.org/pkg/testing/quick/)
```go
```


<a id="coverage" name="coverage"></a>

## Coverage
[Up](#contnent)

[https://blog.golang.org/cover](https://blog.golang.org/cover)\
Показывает какое кол-во кода покрыто тестами.\
<code>go test -cover</code>\
<code>go test -coverprofile c.out</code>\
<code>go tool cover -html=c.out</code>\
<code>go tool cover -func=c.out</code>

<a id="golint" name="golint"></a>

## Golint
[Up](#contnent)


Golint показывает ошибки стиля в коде.\
https://github.com/golang/lint


<a id="benchmark" name="benchmark"></a>

## Benchmark
[Up](#contnent)


[https://golang.org/pkg/testing/#hdr-Benchmarks](https://golang.org/pkg/testing/#hdr-Benchmarks)\
Benchmark - часть пакета тестирования позволяющая измерять скорость выполнения кода.\
Запуск Benchmark: <code>go test -bench .</code>
```go
package main

import "testing"
func BenchmarkSum(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sum(1, 2, 3, 4, 5)
	}
}
```

<a id="testflag" name="testflag"></a>

## Testing flags
[Up](#contnent)



```go
-timeout d
	If a test binary runs longer than duration d, panic.
	If d is 0, the timeout is disabled.
	The default is 10 minutes (10m).

Using:
go test -v -timeout 5s

???
-parallel
-cpu
-count
-p
```

<a id="mocking" name="mocking"></a>

## Mocking
[Up](#contnent)

Types:
* Dummy
* Stub
* Fake(Double)
* Spy
* Mock

```go
```

<a id="testinghttp" name="testinghttp"></a>

## Testing HTTP
[Up](#contnent)

Существует две техники тестирования web приложений:
 * httptest.ResponseRecorder - с помощью httptest пакета создается запрос. И затем напрямую вызывается фуния хэндлер.
 * httptest.Server - с помощью httptest пакета создается экземпляр веб сервера, который эмулирует поведение нашего веб приложения. И затем  тестируются запросы/ответы отпраляемые к нему.
   

Разница между этими способами, чтоs Server позволяет сэмулировать реальное поведение запроса, т.е. он пройдет через все middleware
<a id="httptestrr" name="httptestrr"></a>

### httptest.ResponseRecorder

[Up](#contnent)

Тестируем просто одну фунию хэндлер.
```go
package app

import (
	"fmt"
	"net/http"
)

func Home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Welcome</h1>")
}
```

```go
package app_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testWithGo/app"
	"testing"
)

func TestHome(t *testing.T) {
	w := httptest.NewRecorder()
	r, err := http.NewRequest(http.MethodGet, "/", nil)
	if err != nil {
		t.Errorf("http.NewRequest() = err:%v\n", err)
	}
	app.Home(w, r)
	resp := w.Result()
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("ioutil.ReadAll() = err:%v\n", err)
	}
	want := "<h1>Welcome</h1>"
	got := string(body)
	if got != want {
		t.Errorf("GET / = %s ; want %s\n", got, want)
	}

}
```
<a id="httptests" name="httptests"></a>

### httptest.Server

[Up](#contnent)

```go
package app

import (
	"fmt"
	"net/http"
	"sync"
)

type Server struct {
	mux  *http.ServeMux
	once sync.Once
}

func (a *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	a.once.Do(func() {
		a.mux = http.NewServeMux()
		a.mux.HandleFunc("/", a.Home)
	})
	a.mux.ServeHTTP(w, r)
}

func (a *Server) Home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Welcome</h1>")
}
```

```go

func TestHomeV2(t *testing.T) {
	server := httptest.NewServer(&app.Server{})
	defer server.Close()

	resp, err := http.Get(server.URL)
	if err != nil {
		t.Fatalf("GET / err = %s; want nil", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("ioutil.ReadAll() err = %s; want nil", err)
	}
	got := string(body)
	want := "<h1>Welcome</h1>"
	if got != want {
		t.Errorf("GET / = %s; want %s", got, want)
	}
}
```
<a id="examples" name="examples"></a>

# Examples
[Up](#contnent)

Помимо тестов можно писать фунии Example, кторые так же могут быть использованы для тестов, а так же попадаю в документацию про наш пакет.\
Соглашение по использваонию имен фунии теже что и для тестов, только вместо Test пишем Example.\
- ExampleFuncName
- ExampleTypeName_MethodName
- ExampleFuncName_arg

Пусть в качестве примера есть функция
```go
package examples

import "fmt"

func Hello(name string) (string, error) {
	return fmt.Sprintf("Hello, %s!\n", name), nil
}
```
Example для неё может быть такой
```go
package examples

import "fmt"

func Example() {
	name := "Kirill"
	r, err := Hello(name)
	if err != nil {
		panic(err)
	}
	fmt.Println(r)
	//Output:
	//Hello, Kirill!
}

func ExampleHello() {
	name := "Tony"
	r, err := Hello(name)
	if err != nil {
		panic(err)
	}
	fmt.Println(r)
	//Output:
	//Hello, Tony!
}
// ExampleHello_mark пример использования фунии с аргументом Mark
func ExampleHello_mark() {
	name := "Mark"
	r, err := Hello(name)
	if err != nil {
		panic(err)
	}
	fmt.Println(r)
	//Output:
	//Hello, Mark!
}
```
Запускаем тесты <code>go test</code>. Т.е. в комментариях указывается ожидаемый положительный реузльтат.\
Запускаем локальный сервер с документацией  <code>godoc -http=:3030</code> и ищем в вебе наш пакет.\
Если результат фунии получаем в виде map или по каналу, т.е. не сортированный то вместо Output используется Unordered output и делее ожидаемый результат мапы.
```go
Вместо
//Output:
Указываем
//Unordered output
```
