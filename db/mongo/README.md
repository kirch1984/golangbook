- [Introduction](#introduction)
- [Run MongoDB server in docker](#run-mongodb-server-in-docker)
- [Connect to MongoDB with GO](#connect-to-mongodb-with-go)
- [CRUD with GO and MongoDB](#crud-with-go-and-mongodb)
	- [Insert](#insert)
		- [Insert Struct](#insert-struct)
		- [Insert Map](#insert-map)
	- [Delete](#delete)
	- [Read](#read)
		- [Find(nil)](#findnil)
		- [Find(nil).Sort()](#findnilsort)
		- [Find(nil).Sort() reverse](#findnilsort-reverse)
		- [Find(nil).One()](#findnilone)
		- [Find({selector})](#findselector)
		- [Find({selector1,selector2,…})](#findselector1selector2)
		- [Find({selector},{fields})](#findselectorfields)
		- [Find with Comparison Operators — \$lt,\$gt,\$lte,\$gte](#find-with-comparison-operators--ltgtltegte)
		- [Find with Logical Operators — \$or,\$and,\$nor,\$not](#find-with-logical-operators--orandnornot)
		- [Find with operator $exists](#find-with-operator-exists)
		- [Find with operator $type](#find-with-operator-type)
	- [Update](#update)
		- [\$set,\$unset](#setunset)
		- [\$rename](#rename)
  
<a name="introduction"></a>
# Introduction
MongoDB — нереляционная база данных, хранящая данные в бинарном виде называемым <code>Binary JSON (BSON)</code>. Структуры GO легко сериализуются в BSON.
Данные в MongoDB хранятся в коллекциях состоящих из документов, которые в свою очередь состоят из полей вида <code>key:value</code>. Если проводить аналогию с MySQL,то коллекции MongoDB это аналог таблиц в MySQL, документы аналог строк, а поля документов это имена колонок таблиц MySQL.

<a name="run_mongo_docker"></a>
# Run MongoDB server in docker
[docker mongo](https://docs.docker.com/samples/library/mongo/)

[Install MongoDB Enterprise with Docker](https://docs.mongodb.com/manual/tutorial/install-mongodb-enterprise-with-docker/)

Создаём том для баз mongo
```
docker volume  create mongodb_vol
```
Запускаем контейнер
```
docker container run \
-d \
--restart=always \
--name mongodb \
-p 27017:27017 \
-e MONGO_INITDB_ROOT_USERNAME="admin" \
-e MONGO_INITDB_ROOT_PASSWORD="adminpass" \
-v mongodb_vol:/data/db \
mongo
```
Проверяем доступ к mongodb серверу с помощью mongo shell
```
mongo -u 'admin' -p 'adminpass' --authenticationDatabase admin
```

<a name="connect_to_mongo_with_go"></a>
# Connect to MongoDB with GO
[package mgo](https://godoc.org/gopkg.in/mgo.v2)

[package bson](https://godoc.org/gopkg.in/mgo.v2/bson)

Для работы с MongoDB в GO используется пакет <code>mgo</code>.

Устанавливаем <code>mgo</code>:
```
go get gopkg.in/mgo.v2
```
Для использования mgo необходимо импортировать два пакеты:
```
import (
    "gopkg.in/mgo.v2"
    "gopkg.in/mgo.v2/bson"
)

```
Для подключения к базе необходимо создать сессию с помощью фун-ии <code>Dial</code> или <code>DialWithInfo</code> (предварительно указав с помощью <code>DialInfo</code> необходимые параметры подключения).

Пример с <code>Dial</code>
```
session, err := mgo.Dial("127.0.0.1")
```
Если необходимо подключится к кластеру то
```
session, err := mgo.Dial("server1.mongodb.srv,server2.mongodb.srv,server3.mongodb.srv")
```
Пример с <code>DialWithInfo</code>
```
//предварительно указываем необходимую информацию для подулюченя, такую как логин, пароль, базу авторизации
mongoDialInfo := &mgo.DialInfo{
    Addrs: []string{"localhost"},
    Timeout: 5 * time.Second,
    Database: "admin",
    Username: "admin",
    Password: "adminpass",
    Mechanism: "SCRAM-SHA-1",
}
//создаём сессию
session, err := mgo.DialWithInfo(mongoDialInfo)
if err != nil {
    log.Fatalln("Create sessio error: ",err)
}
defer session.Close()
```
Для того чтоб приступить к операциям CRUD предварительно необходимо создать объект коллекции с помощью метода С (возвращает объект типа <code>mgo.Collection</code>). Данный метод в свою очередь может быть вызван объектом базы, который создается методом DB(возвращает объект типа <code>mgo.Database</code>). В свою очередь данный метод доступен объекту <code>session</code> созданного с помощь метода <code>Dial</code> или <code>DialWithInfo</code>.
```
//подключаемся к базе  usersdb, коллекции  users
c := session.DB("usersdb").C("users")
```
<a name="crud-with-go-and-mongodb"></a>
# CRUD with GO and MongoDB
В GO можно сохранять стурктуры, слайсы, мапы как BSON документы, т. е. mgo автоматически сериализует данные типы данных в BSON.
<a name="insert"></a>
## Insert
Для добавления документа используется метод <code>Insert(docs ...interface{})</code>, данный метод доступен объекту коллекции.
<a name="insert-struct"></a>
### Insert Struct
Рассмотрим пример добавления документов в базу. В примере ниже документы будет получаться путем сериализации GO структур.
```golang
package main

import (
	"log"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//данный тип используется для анонимнго поля структуры user
type info struct {
	Age    int
	Gender string
}
//объявляем тип user являющийся структурой
//в дальнейшем объекты данного типа будут использоваться как доументы базы
type user struct {
	ID    bson.ObjectId `bson:"_id,omitempty"`
	UName string        `bson:"login"`
	FName string        `bson:"firstName"`
	LName string        `bson:"lastName"`
	info  `bson:",inline"` //анонимное поле,inline -важный ключ для работы 
                               //с аномнимными полями https://godoc.org/gopkg.in/mgo.v2/bson#Marshal
}

//создаём данный тип,в качестве примера будем итерироваться по объекту данного типа 
//и добалять их как документы в базу
type users []user

func main() {
        //создам сессию
	mongoDialInfo := &mgo.DialInfo{
		Addrs:     []string{"localhost"},
		Timeout:   5 * time.Second,
		Database:  "admin",
		Username:  "admin",
		Password:  "adminpass",
		Mechanism: "SCRAM-SHA-1",
	}

	session, err := mgo.DialWithInfo(mongoDialInfo)
	if err != nil {
		log.Fatalln("Create sessio error: ", err)
	}
	defer session.Close()

        //создаём объект коллекции
	c := session.DB("usersdb").C("users")

        //далее в качестве примера создадим несколько объектов типа user различными способами
	//короткий вариант
	u1 := user{
		ID:    bson.NewObjectId(),
		UName: "admin",
		FName: "Tony",
		LName: "Hawk",
		info: info{
			23,
			"male",
		},
	}
	//с помощью оператора "new"
	u2 := new(user)
	u2.ID = bson.NewObjectId()
	u2.UName = "billy"
	u2.FName = "Billy"
	u2.LName = "Bones"
	u2.Age = 27
	u2.Gender = "male"

	//с помощью указателя
	u3 := &user{
		ID:    bson.NewObjectId(),
		UName: "artur",
		FName: "Artur",
		LName: "Pirozhkov",
		info: info{
			Age:    25,
			Gender: "male",
		},
	}
	//анонимная стуктура
	u4 := struct {
		ID    bson.ObjectId `bson:"_id,omitempty"`
		UName string        `bson:"login"`
		FName string        `bson:"firstName"`
		LName string        `bson:"lastName"`
		info  `bson:",inline"`
	}{
		ID:    bson.NewObjectId(),
		UName: "dima",
		FName: "Dima",
		LName: "Shvetcov",
		info: info{
			Age:    21,
			Gender: "male",
		},
	}
        
        //традиционный вариант объявления переменной без указания полей структуры
	var u5 = user{
		bson.NewObjectId(),
		"alex",
		"Alexander",
		"Sirop",
		info{
			23,
			"male",
		},
	}

        //традиционный вариант объявления переменной с указаниеи полей структуры
	var u6 = user{
		ID:    bson.NewObjectId(),
		UName: "lena",
		FName: "Elena",
		LName: "Grachevskaya",
		info: info{
			19,
			"female",
		},
	}

        //далее будем итерироваться по обеъктам данного слайса
	var us users
	us = []user{u5, u6}

	//Ниже приведены несколько примеров добавления документов в базу
        //добаляем один документ
	err = c.Insert(u1)
	if err != nil {
		log.Fatalln("Insert u1 error:", err)
	}
        //добаляем один документ
	err = c.Insert(u2)
	if err != nil {
		log.Fatalln("Insert u2 error:", err)
	}
        //добаляем сразу два документа
	err = c.Insert(u3, u4)
	if err != nil {
		log.Fatalln("Insert u3,u4 error:", err)
	}
        //итерируемся и добаляем документ в базу
	for i := range us {
		err := c.Insert(us[i])
		if err != nil {
			log.Fatalf("Insert %s error:%s\n", us[i].UName, err)
		}
	}
}
```
Проверить результат можно через mongo shell,выполнив каманды
```
use usersdb
db.users.find().pretty()
```
или дописав код, в конце фун-ии main() добавить
```golang
count, err := c.Count()
if err != nil {
    log.Fatalln("Count error:", err)
}
fmt.Println(count)
```
<code>c.Count()</code> — данный метод вернёт кол-во документов содержащихся в коллекции.
Надо отметить, что каждый документ базы содержит поле <code>_id</code>. Это так называемый [objectid](https://docs.mongodb.com/manual/reference/method/ObjectId/), это не случайная уникальная последовательность.\
Рассмотрим пример такой последовательности <code>5b4edf36bd575d1b29145aaf</code>, она состоит из:\
<code>4 bytes 5b4edf36</code> — timestamp, кол-во секунд с полуночи (00:00:00 UTC) 1 января 1970 года (см. [Unix Epoch](https://en.wikipedia.org/wiki/Unix_time))\
<code>3 bytes bd575d</code> — mid идентификатор машины\
<code>2 bytes 1b29</code> — pid идентификаатор процесса\
<code>3 bytes 145aaf</code> — просто счетчик, перое число выбирается случайно\
Если данное поле не выставлять самим, то mongodb сгенерирует его автоматически.\
В нашем примере в структуре user для поля ID указали тип <code>bson.ObjectId</code> и для него указали bson тэг <code>_id</code>. Для генерации значения данного типа используется метод <code>bson.NewObjectId()</code>
<a name="insert-map"></a>
### Insert Map
Рассмотрим пример добавления GO map, как документ mongoDB.
```golang
package main

import (
	"log"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func main() {
	mongoDialInfo := &mgo.DialInfo{
		Addrs:     []string{"localhost"},
		Timeout:   5 * time.Second,
		Database:  "admin",
		Username:  "admin",
		Password:  "adminpass",
		Mechanism: "SCRAM-SHA-1",
	}

	session, err := mgo.DialWithInfo(mongoDialInfo)
	if err != nil {
		log.Fatalln("Create sessio error: ", err)
	}
	defer session.Close()

	c := session.DB("usersdb").C("users")
        //классическая инициализация map в GO
	//map[string]interface{}
	m := make(map[string]interface{})
	m["_id"] = bson.NewObjectId()
	m["fname"] = "Viktor"
	m["lname"] = "Cherkov"
	m["age"] = 29
	m["phone"] = "0000000000"
	
        //bson.M аналог map[string]interface{}
	p1 := bson.M{
		"_id":   bson.NewObjectId(),
		"fname": "Tony",
		"lname": "Hawk",
		"age":   21,
		"phone": "1111111111",
	}

	p2 := bson.M{
		"_id":   bson.NewObjectId(),
		"fname": "Tony",
		"lname": "Alva",
		"age":   20,
		"phone": "2222222222",
	}

	p3 := bson.M{
		"_id":   bson.NewObjectId(),
		"fname": "Kris",
		"lname": "Karter",
		"age":   24,
		"phone": "3333333333",
	}
	//bson.D см. https://godoc.org/gopkg.in/mgo.v2/bson#D
	p4 := bson.D{
		{"_id", bson.NewObjectId()},
		{"fname", "Boris"},
		{"lname", "Britva"},
		{"age", 27},
		{"phone", "4444444444"},
	}
	p5 := bson.D{
		{"_id", bson.NewObjectId()},
		{"fname", "Tyler"},
		{"lname", "Durden"},
		{"age", 23},
		{"phone", "5555555555"},
	}
	p6 := bson.D{
		{"_id", bson.NewObjectId()},
		{"fname", "Evgen"},
		{"lname", "Pergen"},
		{"age", 23},
		{"phone", "6666666666"},
	}

	c.Insert(m, p1, p2, p3, p4, p5, p6)
	if err != nil {
		log.Fatalln("bsonM insert error: ", err)
	}

}
```
<a name="delete"></a>
## Delete
Удаление одного документа осуществляется методом <code>Remove(selector interface{})</code>, данный метод доступен объекту коллекции . Где селектор передается, как тип <code>bson.M{}</code>.
```golang
err = c.Remove(bson.M{"fname": "Evgen"})
if err != nil {
    log.Fatalln("bsonM insert error: ", err)
}
```
Удаление нескольких документов осуществляется методом <code>RemoveAll</code>. Данный метод помимо ошибки возвращает данные типа <code>ChangeInfo</code>, включающие в себя такую полезную информацию как — сколько найдено совпадений, сколько документов удалено.
```golang
info,err := c.RemoveAll(bson.M{"fname": "Tony"})
if err != nil {
    log.Fatalln("bsonM insert error: ", err)
}
fmt.Println(info)
```
Если необходимо очистить всю коллекцию, то в <code>RemoveAll</code> передаём <code>nil</code>:
```golang
info,err := c.RemoveAll(nil)
if err != nil {
    log.Fatalln("bsonM insert error: ", err)
}
fmt.Println(info)
```
<a name="read"></a>
## Read
Поиск/чтение документов осуществляется методом <code>Find(query interface{})</code>. Данный метод возвращает объект типа <code>Query</code>. Этот тип обладает такими методами как <code>One,Iter,Sort</code>, которые позволяют полученный результат сериализовать в данные GO map или struct типа.
<a name="findnil"></a>
### Find(nil)
Пример получения всех документов в коллекции
```golang
iter := c.Find(nil).Iter()
res := make(bson.M)
for iter.Next(&res) {
    fmt.Printf("fname:%s,lname:%s,age:%d,phone:%s,\n", res["fname"], res["lname"], res["age"], res["phone"])
}
if err = iter.Close(); err != nil {
    log.Fatal(err)
}

```
<a name="findnilsort"></a>
### Find(nil).Sort()
Если необходимо вернуть отсортированный результат по определенному полю, то используется метод Sort
```golang
iter := c.Find(nil).Sort("fname").Iter()
res := make(bson.M)
for iter.Next(&res) {
    fmt.Printf("fname:%s,lname:%s,age:%d,phone:%s,\n", res["fname"], res["lname"], res["age"], res["phone"])
}
if err = iter.Close(); err != nil {
    log.Fatal(err)
}
```
<a name="findnilsort-reverse"></a>
### Find(nil).Sort() reverse
Сортируем в обратном порядке
```golang
iter := c.Find(nil).Sort("-fname").Iter()
res := make(bson.M)
for iter.Next(&res) {
    fmt.Printf("fname:%s,lname:%s,age:%d,phone:%s,\n", res["fname"], res["lname"], res["age"], res["phone"])
}
if err = iter.Close(); err != nil {
    log.Fatal(err)
}
```
<a name="findnilone"></a>
### Find(nil).One()
Возвращаем только один результат
```golang
res := make(bson.M)
err = c.Find(nil).One(res)
if err != nil {
    log.Fatalln(err)
}
fmt.Printf("fname:%s,lname:%s,age:%d,phone:%s,\n", res["fname"], res["lname"], res["age"], res["phone"])
```
<a name="findselector"></a>
### Find({selector})
В качестве селектора метод find принимает значение типа <code>bson.M{}</code>
```golang
iter := c.Find(bson.M{"fname": "Tony"}).Iter()
res := make(bson.M)
for iter.Next(&res) {
    fmt.Printf("fname:%s,lname:%s,age:%d,phone:%s,\n", res["fname"], res["lname"], res["age"], res["phone"])
}
if err = iter.Close(); err != nil {
    log.Fatal(err)
}
```
<a name="findselector1selector2"></a>
### Find({selector1,selector2,…})
Селектор по двум полям
```golang
iter := c.Find(bson.M{"fname": "Tony"},{"lname":"Hawk"}).Iter()
res := make(bson.M)
for iter.Next(&res) {
    fmt.Printf("fname:%s,lname:%s,age:%d,phone:%s,\n", res["fname"], res["lname"], res["age"], res["phone"])
}
if err = iter.Close(); err != nil {
    log.Fatal(err)
}
```
<a name="findselectorfields"></a>
### Find({selector},{fields})
Поиск документов и вывод результата с определенными полями, аналог <code>find({selector1,selector2,…},{fields})</code> в mongodb
```golang
iter := c.Find(bson.M{"fname": "Tony"}).Select(bson.M{"_id": 0, "fname": 1, "lname": 1, "age": 1}).Iter()
res := make(bson.M)
for iter.Next(&res) {
    fmt.Println(&res)
}
if err = iter.Close(); err != nil {
    log.Fatal(err)
}
```
<a name="find-with-comparison-operators--ltgtltegte"></a>
### Find with Comparison Operators — \$lt,\$gt,\$lte,\$gte
При задании селектора могут быть использованы различные [логические операторы, операторы сравнения и т.д.](https://docs.mongodb.com/manual/reference/operator/query/)
```
Неполный список операторов сравнения:
$ne не равно
$lt < 
$gt >
$lte <= 
$gte >=
```
Пример селектора с использование оператора сравнения
```golang
iter := c.Find(bson.M{"age": bson.M{"$gte": 23}}).Iter()
res := make(bson.M)
for iter.Next(&res) {
    fmt.Println(&res)
}
if err = iter.Close(); err != nil {
    log.Fatal(err)
}
```
<a name="find-with-logical-operators--orandnornot"></a>
### Find with Logical Operators — \$or,\$and,\$nor,\$not
Пример селектора с использование лигических операторов
```golang
iter := c.Find(bson.M{"$or": []bson.M{bson.M{"phone": "1111111111"}, bson.M{"phone": "2222222222"}}}).Select(bson.M{"_id": 0}).Iter()
res := make(bson.M)
for iter.Next(&res) {
    fmt.Println(&res)
}
if err = iter.Close(); err != nil {
    log.Fatal(err)
}
```
<a name="find-with-operator-exists"></a>
### Find with operator $exists
Если необходимо найти документы содержащии или не содержащии определенное поле,
то используется оператор [$exists](https://docs.mongodb.com/manual/reference/operator/query/exists/#op._S_exists)
```golang
iter := c.Find(bson.M{"phone": bson.M{"$exists": true}}).Select(bson.M{"_id": 0}).Iter()
res := make(bson.M)
for iter.Next(&res) {
    fmt.Println(&res)
}
if err = iter.Close(); err != nil {
    log.Fatal(err)
}
```
<a name="find-with-operator-type"></a>
### Find with operator $type
Если необходимо найти документы содержащии поле с определенным типом данных,
то используется оператор [$type](https://docs.mongodb.com/manual/reference/operator/query/type/#op._S_type)
```golang
iter := c.Find(bson.M{"age": bson.M{"$type": 16}}).Select(bson.M{"_id": 0}).Iter()
res := make(bson.M)
for iter.Next(&res) {
    fmt.Println(&res)
}
if err = iter.Close(); err != nil {
    log.Fatal(err)
}
```
<a name="update"></a>
## Update
Обновление документов осуществляется методом <code>Update(selector interface{}, update interface{})</code> и <code>UpdateAll(selector interface{}, update interface{})</code>, где первый метод обновит первый документ попавший под селектор, а второй все документы подходящие под селектор.
Если обновлять документ без каких-либо дополнительных операторов таких как <code>\$set,\$unset</code> и т.д., то перезаписывается весь документ.
```golang
Было
{ "_id" : ObjectId("5beb717cd582872c5a4312f2"), "fname" : "Tony", "lname" : "Hawk", "age" : 21, "phone" : "1111111111" }

После выполнения данного кода
err = c.Update(bson.M{"lname": "Hawk"}, bson.M{"fname": "TONY"})
if err != nil {
    log.Fatal(err)
}

Результат будет такой
{ "_id" : ObjectId("5beb717cd582872c5a4312f2"), "fname" : "TONY" }
т.е. все не указанные поля удалились и перезаписалось только одно поле.
```
Метод update() сожержит различные операторы, которые помогают модифицировать [отдельные поля, массивы данных…](https://docs.mongodb.com/manual/reference/operator/update/)
<a name="setunset"></a>
### \$set,\$unset
<code>\$set</code> — изменить/добавить поле(если поля в документе не существует, то оно добавляется)
<code>\$unset</code> — удалить поле.
Обновим документ используя <code>\$set</code>, в качестве примера добавим новое поле <code>"testfield": "testfield"</code>
```golang
err = c.Update(bson.M{"fname": "TONY"}, bson.M{"$set": bson.M{"fname": "Tony", "lname": "Hawk", "age": 22, "phone": "1111111111", "testfield": "testfield"}})
if err != nil {
    log.Fatal(err)
}
```
Теперь удалим поле <code>"testfield": "testfield"</code> с помощью оператора <code>\$unset</code>
```golang
err = c.Update(bson.M{"lname": "Hawk"}, bson.M{"$unset": bson.M{"testfield": "testfield"}})
if err != nil {
    log.Fatal(err)
}
```
<a name="rename"></a>
### \$rename
Данный оператор позволяет переименовывать поля документов.
Переименуем поле <code>lname</code> в <code>lastname</code> всех документов.
```golang
info, err = c.UpdateAll(bson.M{}, bson.M{"$rename": bson.M{"lname": "lastname"}})
if err != nil {
    log.Fatal(err)
}
fmt.Println(info)
```