<a id="contnent" name="contnent"></a>

- [Introduction](#introduction)
  - [Основные понятия](#Основные-понятия)
  - [Exchanges and Exchange Types](#exchanges-and-exchange-types)
    - [Default Exchange](#default-exchange)
    - [Direct Exchange](#direct-exchange)
    - [Fanout Exchange](#fanout-exchange)
    - [Topic Exchange](#topic-exchange)
    - [Headers Exchange](#headers-exchange)
  - [Queues](#queues)
  - [Bindings](#bindings)
  - [Publisher/Producer](#publisherproducer)
  - [Consumers](#consumers)
  - [Connections](#connections)
  - [Channels](#channels)
  - [Virtual Hosts](#virtual-hosts)
- [Настройка WorkFlow](#Настройка-workflow)
- [UI и RabbitMQ](#ui-и-rabbitmq)
- [Go и RabbitMQ](#go-и-rabbitmq)
  - [Default Exchange](#default-exchange-1)
    - [Round-robin](#round-robin)
    - [Message acknowledgment](#message-acknowledgment)

<a name="introduction" id="introduction"></a>

# Introduction

[Up](#contnent)

[RabbiMQ Documentation](https://www.rabbitmq.com/documentation.html)\
[RabbitMQ Simulator](http://tryrabbitmq.com/)

[RabbitMQ essentials with Go examples](https://rafalgolarz.com/blog/2018/02/20/rabbitmq_essentials_with_go_examples/)\
[How RabbitMQ Works and RabbitMQ Core Concepts](https://www.javaguides.net/2018/12/how-rabbitmq-works-and-rabbitmq-core-concepts.html)

RabbitMQ - это основанный на Erlang брокер сообщений, предоставляющий поддержку AMQP,STOMP, MQTT протоколов.

Общие случаи использования:
* Распределение информации
* Форматирование данных
* Агрегация информации
* Вызов событий, например в приложении хранящем события
* Складировать сообщения в очередь для дальнейше обработки



## Основные понятия

[Up](#contnent)

Можно думать про RabbitMQ в концепции почтового отделения. Когда вы опускаете письмо в почтовый ящик, то вы можете быть уверены, что письмо дойдет до адресата. По этой аналогии RabbitMQ выступает,как почтовый ящик, почтовое отделение и почтальон.

<img src="./imgs/rabbitmq_diagram1.png">

Producer/Publisher отправляет сообщение на "биржу" (exchanges), затем биржа распределяет копии сообщений по очередям(queues) согласно правилам (bindings). Затем брокер доставляет сообщения потребителям (consumers) подписанным на очередь (push API) или потребитель сам достает сообщения из очереди по требованию (pull API).\
После получения сообщения потребителем, он либо автоматически, либо как укажет разработчик уведомляет брокера о получении. После чего брокер может удалить сообщение из очереди.

Основные определения:
* Broker - это промежуточное приложение получающее сообщение от отправителя и доставляющий его получателю. Включает в себя компоненты: Exchange,Queue,Binding,Connection,Channel,Virtual Host,Users
* Exchange - объект получающий сообщения и распределяющий по очередям
* Binding - "связь" между биржей и очередью
* Queue - буфер для хранения сообщений,куда сообщения от Producer складываются и откуда затем Consumer читает их 
* Message - информация передаваемая от Producer к Consumer
* Connection - сетевое соединение между Producer/Consumer и брокером
* Channel - виртуальное/логическое соединение между Producer/Consumer и брокером. Таких каналов может быть несколко в рамках одной сетевого соединения
* Virtual Host - позволяет брокеру создавать изолированные среды сгруппированные по пользователям, очередям, биржам... 
* Routing key - ключ маршрутизации подобен адресу на конверте. Используется для персылки сообщения в нужную очередь
* Producer/Publisher - приложение/сервис которое создает сообщения и отправляет на биржу
* Consumer - приложение/сервис получает сообщения




## Exchanges and Exchange Types

[Up](#contnent)

Биржы являются входной точкой для сообщений откуда они перенапраляются по очередям. В зависимости от типа биржы используются различные методы мршрутизации сообщений.\
Биржы бываю следющих типов:
* Direct
* Fanout
* Topic
* Headers

Биржы имеют атрибуты:
* Name
* Durability - при рестарте брокера биржа сохраняется
* Auto-delete - биржа удаляется, как только отписывается последняя очередь
* Arguments - опциональный атрибут, используется плгинами и брокером

Также биржи бывают временными и долговременными. Долговременные сохраняются при презапуске брокера, временные нет.

### Default Exchange

[Up](#contnent)

По сути это `Direct` биржа без указанного имени т.е. в качестве имени пустая строка, данная биржа является предустановленной брокером.\
У данного типа биржы есть особенность - каждая созданная очередь подписывается на данную биржу, где в качестве ключа маршрутизации используется имя очереди.

### Direct Exchange

[Up](#contnent)

`Direct` биржа доставляет собщения в очередь на основе ключа маршрутизации указанного в сообщении. Данный тип идеален для unicast сообщений.\
Как это работатет:
* очередь подписывается/связываается/соединяется с биржей с ключем маршрутизации K
* когда на биржу поступает сообщение с ключем R, то биржа направит его в эту очередь если K=R
  
Данный тип используется для распределения задач между "воркерами"(экземплярами одного и того же приложения) по циклу (round robin). Но нужно понимать, AMQP балансирует сообщения между `Consumer`, а не очередями.

<img src="./imgs/exchange-direct.png">

### Fanout Exchange

[Up](#contnent)

Данный тип биржы перенаправляет сообщения во все подписанные на него очереди, ключ маршрутизации игнорируется. Т.е. если на биржу подписаны N очередей, то сообщение будет направлено во все N очередей. `Fanout` ипользуется для широковещателного(broadcast) перенаправления сообщений.

<img src="./imgs/exchange-fanout.png">

### Topic Exchange

[Up](#contnent)

Topic биржа направляет сообщения в одну очередь или более на основе соответстия между ключем и шаблоном вида `*.*` (пример таких ключей: error.\*,*.log,debug.log,notice.\*) использованным при привзяке очереди к бирже. В общем данный тип используется для многоадресной (multicast) маршрутизации.\
\# - совпадение по 0 или более слов\
\* - совпадение по одному слову

### Headers Exchange

[Up](#contnent)

Headers биржа предназначена для маршрутизации по нескольким атрибутам, которые легче выразить в заголовке чем через ключ. Ключи в данном типе игнорируюся. Для этого используется x-match аргумент, данный аргумент может иметь параметр any или all. Т.е. any - любое совпадение, all - все совпадения.

## Queues

[Up](#contnent)

[Queues](https://www.rabbitmq.com/queues.html)

Очередь - упорядоченная коллекция сообщений. Сообщения добавляются и удаляются по принципу FIFO.\
Перед началом ипользования очередь должна быть объявлена. При объявлении она создается если еще не существует. Если уже есть такая очередь с темиже атрибутами, то никакого эффекта на уже существующую очередь не оказывает. Если атрибуты отличаются, то будет сгенерировано исключение на уровне канала с кодом 406.

Имеет следующие св-ва:
  * Name
  * Durable - очередь сохраняется при перезапуске брокер
  * Exclusive - используется только одним подулючением и удаляется после закрытия подключения
  * Auto-delete - очередь удаляется, как только последний Consumer отпишится от него
  * Arguments - опциональное св-во(используется плгинами и брокером для указания TTL, длиины очереди и т.д.)

**Name**  - имя очереди разработчик может задать сам или брокер сгенерирует его, если будет указана пустая строка. Имя может быть не больше 255 байт в UTF-8.\
Имена начинающиеся с `amq.` зарезервированы.\
**Durable** - очереди, как и биржы могут быть временными или долговременными. Данная опция отвечает за хранение очереди на диске, чтоб в случае перезапуска брокера, очередь сохранилась. 
При этом долговременность очереди не влияет на сохранность сообщений. За сохранность сообщений отвечают атрибут самого сообщения.\
**Exclusive** - очереди с данной опцией используются только в рамках того соединения, которого были созданы.\
Такие очереди удаляются при закрытие или потере соединения.\
**Auto-delete** - очередь удаляется когда последний Consumer отписывается от неё или закрывает канал, соединение или рвется соединение.\
Но если очередь никогда не имела получателей, т.е. доставка сообщений осуществлялась pull API методом, то очередь не удалится. Для этого нужно использовать TTL в `Optional Arguments`.\
**Optional Arguments** - необязательные аргументы, также известны как "x-arguments". Они используются плагинами или брокером для указания таких св-в как:
* TTL
* Queuq Length Limit/bytes
* Mirroring settings
* Max number of priorities
* Consumer priorities

## Bindings

[Up](#contnent)

Привязки - это правила по которым биржа маршрутизирует мообщение в нужную очередь. Чтоб биржа E направила сообщение в очередь Q, Q должнабыть привязана/подписана на E.\
Привязки могут иметь атрибут - ключ маршрутизации используемы в некоторых типах бирж. Назначение ключа маршрутизации состоит в том, чтобы выбрать определенные сообщения, из биржы,и направить их в связанную очередь. Другими словами, ключ маршрутизации действует как фильтр.


## Publisher/Producer

[Up](#contnent)

[Publishers](https://www.rabbitmq.com/publishers.html)


## Consumers

[Up](#contnent)

[Consumers](https://www.rabbitmq.com/consumers.html)

Сообщения сохраняются в очереди до тех пока приложение не заберёт их. Доставка сообщений осуществляется двумя способами:
* Брокер сам доставляет их получателю (push API)
* Получатель сам извлекает их из очериди по мере необходимости (pull API) 

Для получания сообщений методом push API получатель должен подписаться/зарегистироваться в очереди. 

## Connections

[Up](#contnent)

AMQP 0-9-1 соединения обычно долгоживущий объект. AMQP 0-9-1 это протокол прикладного уровня использующий TCP для надежной доставки. Соединения используют аутентификацию и могут быть защищены TLS.

## Channels

[Up](#contnent)

[Channels](https://www.rabbitmq.com/channels.html)

Некоторым приложениям необходимо более, чем одно соединение с сервером. AMQP 0-9-1 соединения мультиплексируются с помощью каналов, которые по сути являются облегченными соединениями совместно использующие одно TCP соединение.\
Каждая операция клиента осуществялется по каналу. Связь по одному каналу полностью отделена от другого.\
Каналы всегда существуют в контексте одного соединения и не могут соществовать сами по себе. Если соединение закрыть, то и все каналы на нем закроются.\

В приложениях использующих несколько потоков/процессов для обработки, часто открывают новый канал для каждого потока и не делят его.


## Virtual Hosts

[Up](#contnent)

[Virtual Hosts](https://www.rabbitmq.com/vhosts.html)

Логическая группа сущностей таких как: бирж,соединений,очередей, пользовательских прав, подписей к биржам ит.д. По сути, как виртуальный брокер. По умолчанию создается виртуальный хост с именем "/". В общем Virtual Hosts RabbitMQ имеет туже концепцию, что и Virtual Hosts в Apache или Nginx. Доступ между виртуальными хостами нет, как и какой-либо коммуникации.


# Настройка WorkFlow

[Up](#contnent)

 Run rabbitMQ docker container
```
//create separate network
docker network create  --subnet 172.20.0.0/24 rabbitmq
//run rabbitMQ container
docker container run \
-d \
--network rabbitmq \
--ip 172.20.0.2 \
--restart=always \
--name rmq \
rabbitmq:3.8.0-rc.1-management
```
Install golang package
```
go get github.com/streadway/amqp
```

Visit the page by your web browser
```
http://172.20.0.2:15672/
guest/guest
```

# UI и RabbitMQ

[Up](#contnent)

# Go и RabbitMQ

[Up](#contnent)

## Default Exchange

[Up](#contnent)

В данном примере попробуем реализовать самую базовую концепцию используя биржу созданную по умолчанию.\

<img src="./imgs/python-one.png">


т.е. у нас имеется `Publisger`, дефолтная биржа, очередь и `Consumer`.

Создаём рабочую папку. Все дальнейшие примеры будут располагаться по данному пути.
```
mkdir -p $GOPATH/src/gitlab.com/kirch1984/rabbitmq/{consumer,publisher}
cd $GOPATH/src/gitlab.com/kirch1984/rabbitmq/
```
Соответственно в `consumer` будет находиться код для получателя, а в `publisher` отправителя.

`$GOPATH/src/gitlab.com/kirch1984/rabbitmq/publisher/sender.go`
```go
package main

import (
	"fmt"
	"log"

	"github.com/streadway/amqp"
)

func main() {
	//создаем cоединение
	login := "guest"
	password := "guest"
	host := "172.20.0.2"
	port := "5672"
	dsn := fmt.Sprintf("amqp://%s:%s@%s:%s/", login, password, host, port)
	conn, err := amqp.Dial(dsn)
	checkError("Error to create connection with RabbitMQ server", err)
	defer conn.Close()
	//создаем канал
	ch, err := conn.Channel()
	checkError("Error to create a channel", err)
	defer ch.Close()
	//создаем очередь
	q, err := ch.QueueDeclare(
		"hello", // Name
		false,   // Durable
		false,   // Auto-delete
		false,   // Exclusive
		false,   // no-wait
		nil,     // arguments
	)
	checkError("Error to create a queue", err)

	body := "Hello World!"
	err = ch.Publish(
		"",     // exchange,т.к. пустая строка, то будет использован default exchange
		q.Name, // routing key,т.е. default exchange, то в качестве routing key указываем имя очереди
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
	log.Printf("Sent %s", body)
	checkError("Error to publish a message", err)
}

func checkError(msg string, err error) {
	if err != nil {
		log.Fatalf("%v: %v\n", msg, err)
	}
}
```

`$GOPATH/src/gitlab.com/kirch1984/rabbitmq/consumer/reciever.go`
```go
package main

import (
	"fmt"
	"log"

	"github.com/streadway/amqp"
)

func main() {
	//создаем соединение
	login := "guest"
	password := "guest"
	host := "172.20.0.2"
	port := "5672"
	dsn := fmt.Sprintf("amqp://%s:%s@%s:%s/", login, password, host, port)
	conn, err := amqp.Dial(dsn)
	checkError("Error to create connection with RabbitMQ server", err)
	defer conn.Close()
	//создаем канал
	ch, err := conn.Channel()
	checkError("Error to create a channel", err)
	defer ch.Close()
	//создаём такую же очередь
	//т.е. полчателя чисто теоретически мы можем запустить раньше, чем отправителя то создаем точно такуюже очередь
	q, err := ch.QueueDeclare(
		"hello", // Name
		false,   // Durable
		false,   // Auto-delete
		false,   // Exclusive
		false,   // no-wait
		nil,     // arguments
	)
	checkError("Error to create a queue", err)
	//далее сообщаем серверу чтоб сообщения нам доставлялись по push API
	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	checkError("Failed to register a consumer", err)

	forever := make(chan bool)

	go func() {
		for d := range msgs {
      log.Printf("Received a message: %s", d.Body)
      log.Printf("Work in progress...")
      time.Sleep(time.Second*2)
      log.Printf("Work Done.")
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

func checkError(msg string, err error) {
	if err != nil {
		log.Fatalf("%v: %v\n", msg, err)
	}
}
```

Запускаем
```
go run publisher/sender.go
2019/09/26 12:22:42 Sent Hello World!

go run consumer/reciever.go 
2019/09/26 12:21:39  [*] Waiting for messages. To exit press CTRL+C
2019/09/26 12:22:42 Received a message: Hello World!
```

### Round-robin

[Up](#contnent)

Теперь немного дополним пример, добавив `time.Sleep()` для имитации работы в файл `reciever.go`
```go
go func() {
  for d := range msgs {
    log.Printf("Received a message: %s", d.Body)
    log.Printf("Work in progress...")
    time.Sleep(time.Second * 2)
    log.Printf("Work Done.")
  }
}()
```
и заупсти еще одного `Consumer`, т.е. запустим еще в одном терминале получателя.

<img src="./imgs/python-two.png">

Запускаем
```
//shell1
go run publisher/sender.go 
2019/09/26 15:16:05 Sent Hello World!
go run publisher/sender.go 
2019/09/26 15:16:11 Sent Hello World!

//shell2
go run consumer/reciever.go 
2019/09/26 15:16:01  [*] Waiting for messages. To exit press CTRL+C
2019/09/26 15:16:05 Received a message: Hello World!
2019/09/26 15:16:05 Work in progress...
2019/09/26 15:16:07 Work Done.

//shell3
go run consumer/reciever.go
2019/09/26 15:16:03  [*] Waiting for messages. To exit press CTRL+C
2019/09/26 15:16:11 Received a message: Hello World!
2019/09/26 15:16:11 Work in progress...
2019/09/26 15:16:13 Work Done.
```

Если на очередь подписаны несколько получателей, то RabbitMQ использует Round-robin при доставке сообщений.

### Message acknowledgment

[Up](#contnent)
