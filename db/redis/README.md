>* [Introduction](#introduction)
>* [Run Redis server in docker](#redisdocker)
>* [Connect to Redis with GO](#connectwithgo)
>* [CRUD with GO and Redis](#crudwithgo)
>   * [String](#string)
>     * [set](#set)
>     * [setex](#setex)
>     * [get](#get)
>     * [del](#del)
>     * [append](#append)
>     * [mset](#mset)
>     * [mget](#mget)
>     * [strlen](#strlen)
>   * [List](#list)
>     * [rpush/lpush](#rpushlpush)
>     * [lrange](#lrange)
>     * [rpop/lpop](#rpoplpop)
>     * [lindex](#lindex)
>     * [lset](#lset)
>     * [llen](#llen)
>   * [Hashes](#hashes)
>     * [hset](#hset)
>     * [hget](#hget)
>     * [hmset](#hmset)
>     * [hmget](#hmget)
>     * [hgetall](#hgetall)
>     * [hkeys](#hkeys)
>     * [hdel](#hdel)
>     * [hexists](#hexists)
>   * [Sets](#sets)
>     * [sadd](#sadd)
>     * [smembers](#smembers)
>     * [scard](#scard)
>     * [sismember](#sismember)
>     * [srem](#srem)
>   * [Sorted sets](#sortedsets)
>     * [zadd](#zadd)
>     * [zrange](#zrange)
>     * [zrevrange](#zrevrange)
>     * [zcount](#zcount)
>     * [zrank](#zrank)
>     * [zincrby](#zincrby)
>     * [zscore](#zscore)
> 
<a name="introduction" id="introduction"></a>
# Introduction
<a name="redisdocker" id="redisdocker"></a>
# Run Redis server in docker
Создаём два тома, для редис конфига и бэкапов
```
docker volume create redisVol
docker volume create redisCFG
```
Оригинал redis.conf можно скачать [отсюда](https://raw.githubusercontent.com/antirez/redis/4.0.11/redis.conf) (в ссылке необходимо поменять версию redis, если он у вас отличается от моего 4.0.11)\
Включим аутентификацию в redis, в конфиг добавим строчку <code>requirepass SOMESTRONGPASSWORD</code> (вместо SOMESTRONGPASSWORD у вас должен быть указан свой пароль, у себя в качестве примера(**!!!ТОЛЬКО В КАЧЕСВТЕ ПРИМЕРА!!!**) буду использовать не секьюрный пароль 12345).
Соответственно далее смотрим, где у нас расположен том <code>redisCFG</code> командой
<code>docker volume inspect redisCFG</code> и туда копируем конфиг редиса.\
Выбор ip адреса для redis контейнера оставляю на ваш выбор, либо создайте отдельную сеть например так
```
docker network create \
--subnet=172.18.0.0/24 \
lazercat
```
либо гляньте текущую конфигурацию сети bridge и исходя из занятых ip на данный момент выберите свободный
```
docker network inspect bridge
```
Запускамем redis в контейнере
```
docker container run \
-h redisSrv \
--restart=always \
--name redisSrv \
--ip 172.17.0.10 \ 
-d \
-v redisVol:/data \
-v redisCFG:/usr/local/etc/redis/ \
redis redis-server /usr/local/etc/redis/redis.conf
```
Проверяем, что redis успешно стартанул и у нас есть к нему доступ
```
docker container exec -it redisSrv redis-cli -a 12345
127.0.0.1:6379>
```
либо ставим redis-cli для вашей ОС и проверяем
```
redis-cli -h 172.17.0.10 -a 12345
172.17.0.10:6379>
```
<a name="connectwithgo" id="connectwithgo"></a>
# Connect to Redis with GO
[Redis Go clients](https://redis.io/clients#go)\
[heroku.com recommend](https://devcenter.heroku.com/articles/redistogo#golang)\
[package redis](https://godoc.org/github.com/gomodule/redigo/redis)\
Для работы с Redis в GO будем использовать пакет redigo.\
Устанавливаем
```
go get github.com/gomodule/redigo/redis
```
Импортируем
```
import (
	"github.com/gomodule/redigo/redis"
)
```
Соединение можно создать с помощью фун-ий <code>Dial</code>,<code>DialWithTimeout</code>,<code>NewConn</code> или <code>DialURL</code>. В примере ниже используем <code>Dial</code> и <code>DialURL</code> (закоменчен)
```golang
package main

import (
	"log"

	"github.com/gomodule/redigo/redis"
)

//const redisurl = "redis://:12345@172.17.0.3:6379/1"

func main() {
    args := []redis.DialOption{
        redis.DialDatabase(1), //select db 1
        redis.DialPassword("12345"), //set the password
   }
    c, err := redis.Dial("tcp", "172.17.0.10:6379", args...)
    
    //c, err := redis.DialURL(redisurl)
    if err != nil {
        log.Fatalln(err)
    }
    defer c.Close()
}
```

<a name="crudwithgo" id="crudwithgo"></a>
# CRUD with GO and Redis
Для выполнения [redis команд](https://redis.io/commands) используется Do метод.

<a name="string" id="string"></a>
## String
[Redis string commands](https://redis.io/commands#string)\
Общий и самый используемый тип данных в программировании и приложениях. Так же является фундаментальным типом в redis в котором все ключи являются строками.

<a name="set" id="set"></a>
### set
<code>set KEY VALUE</code> - выставить значение ключа (если ключ уже существует, то перезаписывает его)
```golang
res, err := redisConn.Do("set", "mykey", "value")
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```
```golang
res, err := redisConn.Do("set", "mykey", "value", "ex", 100)
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```

<a name="setex" id="setex"></a>
### setex
<code>setex KEY SECONDS VALUE</code> - добавить ключ и его время жизни - через которые ключ удалится автоматически
```golang
res, err := redisConn.Do("setex", "mykey", 100, "value")
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```
<a name="get" id="get"></a>
### get
Т.к. redigo не сериализует данные автоматически, то необходимо дополнительно использовать фун-ии для приведение типов.
```golang
res, err := redis.String(redisConn.Do("get", "mykey"))
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```
<a name="del" id="del"></a>
### del
Команда <code>del</code> удаляет ключ и его значение любого типа, не только string.
```golang
res, err := redisConn.Do("del", "mykey")
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```

<a name="append" id="append"></a>
### append
<code>append KEY VALUE</code> - добавляет значение в конец строки. Если ключа не существует, то создается пустой ключ и затем добавляется значение.
```golang
res, err := redisConn.Do("append", "mykey", 9999)
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```

<a name="mset" id="mset"></a>
### mset
<code>mset KEY VALUE KEY VALUE KEY VALUE KEY VALUE etc ...</code> - добавить сразу несколько ключей
```golang
res, err := redisConn.Do("mset", "mykey1", 10, "mykey2", 20, "mykey3", "test")
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```

<a name="mget" id="mget"></a>
### mget
<code>mget KEY KEY KEY etc...</code> - получить сразу несколько значений ключей
```golang
var mykey1 int
var mykey2 int
var mykey3 string

res, err := redis.Values(redisConn.Do("mget", "mykey1", "mykey2", "mykey3"))
if err != nil {
    log.Fatalln(err)
}

_, err = redis.Scan(res, &mykey1, &mykey2, &mykey3)
if err != nil {
    log.Fatalln(err)
}

fmt.Println(mykey1, mykey2, mykey3)
```

<a name="strlen" id="strlen"></a>
### strlen
<code>strlen KEY</code> - вернуть длину значения ключа
```golang
res, err := redisConn.Do("strlen", "mykey1")
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```

<a name="List" id="List"></a>
## List
[Redis list commands](#https://redis.io/commands#list)\
Упорядоченный список. Список хранит последовательность объектов и может быть использован, как очередь или стэк.

<a name="rpushlpush" id="rpushlpush"></a>
### rpush/lpush
<code>rpush KEY ITEM1 ITEM2 etc...</code> - добавить эл-нты «с правого конца» списка, если список не существует, то он создается.\
<code>lpush KEY ITEM1 ITEM2 etc...</code> - добавить эл-нты «с левого конца» списка, если список не существует, то он создается.
```golang
res, err := redisConn.Do("rpush", "array1", 1, 2, 3, 4, 5)
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```
```golang
res, err := redisConn.Do("lpush", "array1", 9, 8, 7, 6)
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```
<a name="lrange" id="lrange"></a>
### lrange
<code>lrange KEY 0 -1</code> - вернуть значения всего списка
```golang
res, err := redis.Ints(redisConn.Do("lrange", "array1", 0, -1))
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```
<a name="rpoplpop" id="rpoplpop"></a>
### rpop/lpop
<code>lpop KEY</code> - удалить эл-нт «с левого конца» списка\
<code>rpop KEY</code> - удалить эл-нт «с правого конца» списка
```golang
res, err := redisConn.Do("rpop", "array1")
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```
```golang
res, err := redisConn.Do("lpop", "array1")
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```
<a name="lindex" id="lindex"></a>
### lindex
<code>lindex KEY INDEX</code> - вернуть значение эл-та по указанному индексу
```golang
res, err := redis.Int(redisConn.Do("lindex", "array1", 0))
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```
<a name="lset" id="lset"></a>
### lset
<code>lset KEY INDEX ITEM</code> - установиьт значение эл-та в списке по указанному индексу
```golang
res, err := redisConn.Do("lset", "array1", 3, 77)
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```

<a name="llen" id="llen"></a>
### llen
<code>llen KEY</code> — вернуть длину ключа
```golang
res, err := redisConn.Do("llen", "array1")
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```
<a name="hashes" id="hashes"></a>
## Hashes
[Redis hash commands](#https://redis.io/commands#hash)\
Представляет собой тип данных воссоздающих отношения поле-значение.

<a name="hset" id="hset"></a>
### hset
<code>hset KEY FILED VALUE</code> - установить значение поля
```golang
res, err := redisConn.Do("hset", "p1", "firstname", "Tony")
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```

<a name="hget" id="hget"></a>
### hget
<code>hget KEY FIELD</code> - получить значение поля
```golang
res, err := redis.String(redisConn.Do("hget", "p1", "firstname"))
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```

<a name="hmset" id="hmset"></a>
### hmset
<code>hmset KEY FILED1 VALUE1 FILED2 VALUE2 etc...</code> - установить несколько значений для нескольких полей сразу
```golang
res, err := redisConn.Do("hmset", "p1", "firstname", "Tony", "lastname", "Hawk", "age", 25)
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```

<a name="hmget" id="hmget"></a>
### hmget
<code>hmget KEY FILED1 FILED2 etc...</code> - получить несколько значений полей сразу
```golang
type person struct {
    Firstname string `redis:"firstname"`
    Lastname  string `redis:"lastname"`
    Age       int    `redis:"age"`
}
res, err := redis.Values(redisConn.Do("hmget", "p1", "firstname", "lastname", "age"))
if err != nil {
    log.Fatalln(err)
}

p1 := person{}
_, err = redis.Scan(res, &p1.Firstname, &p1.Lastname, &p1.Age)
if err != nil {
    log.Fatalln(err)
}

fmt.Println(p1)
```

<a name="hgetall" id="hgetall"></a>
### hgetall
<code>hgetall KEY</code> - получить все поля и значения
```golang
type person struct {
    Firstname string `redis:"firstname"`
    Lastname  string `redis:"lastname"`
    Age       int    `redis:"age"`
}
res, err := redis.Values(redisConn.Do("hgetall", "p1"))
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
p1 := person{}
err = redis.ScanStruct(res, &p1)
if err != nil {
    log.Fatalln(err)
}

fmt.Println(p1)
```

<a name="hkeys" id="hkeys"></a>
### hkeys
<code>hkeys KEY</code> - получить имена всех полей
```golang
res, err := redis.Strings(redisConn.Do("hkeys", "p1"))
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```

<a name="hdel" id="hdel"></a>
### hdel
<code>hdel KEY FILED</code> - удалить одно поле
```golang
res, err := redisConn.Do("hdel", "p1", "age")
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```

<a name="hexists" id="hexists"></a>
### hexists
<code>hexists KEY FILED</code> - проверка существования поля, возвращает 0 или 1 соответственно
```golang
res, err := redisConn.Do("hexists", "p1", "age")
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```
```golang
res, err := redisConn.Do("hexists", "p1", "firstname")
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```
<a name="sets" id="sets"></a>
## Sets
[Redis set commands](#https://redis.io/commands#set)\
Мн-во уникальных и не упорядоченных объектов

<a name="sadd" id="sadd"></a>
### sadd
<code>sadd KEY ITEM1 ITEM2 ITEM3 etc...</code> - создать мн-во
```golang
res, err := redisConn.Do("sadd", "cars", "toyota", "nissan", "suzuki", "toyota", "lexus", "infiniti")
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```

<a name="smembers" id="smembers"></a>
### smembers
<code>smembers KEY</code> - показать всё мн-во
```golang
res, err := redis.Strings(redisConn.Do("smembers", "cars"))
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```
<a name="scard" id="scard"></a>
### scard
<code>scard KEY</code> - показать кол-во эл-ов в мн-ве
```golang
res, err := redisConn.Do("scard", "cars")
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```

<a name="sismember" id="sismember"></a>
### sismember
<code>sismember KEY ITEM</code> - проверка принадлежности указанного эл-та указанному мн-ву, возвращает 0 или 1 соответственно
```golang
res, err := redisConn.Do("sismember", "cars", "toyota")
if err != nil {
    log.Fatalln(err)
}
fmt.Println(res)
```
```golang
res, err := redisConn.Do("sismember", "cars", "honda")
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```

<a name="srem" id="srem"></a>
### srem
<code>srem KEY ITEM1 ITEM2 ITEM3 etc…</code> - удалить эл-ты из мн-ва
```golang
res, err := redisConn.Do("srem", "cars", "nissan")
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```
<a name="sortedsets" id="sortedsets"></a>
## Sorted sets
[Redis sorted set commands](https://redis.io/commands#sorted_set)\
Мн-во уникальных и упорядоченных объектов,сортиурет от меньшего к большему

<a name="zadd" id="zadd"></a>
### zadd
<code>zadd KEY SCORE1 ITEM1 SCORE2 ITEM2 SCORE3 ITEM3 etc...</code> - создать мн-во,добавить один эл-нт мн-ва или обновить вес эл-та, если уже существует

```golang
res, err := redisConn.Do("zadd", "cars", 100, "toyota", 90, "nissan", 80, "suzuki")
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```
<a name="zrange" id="zrange"></a>
### zrange
<code>zrange KEY 0 -1</code> - показать всё мно-во
```golang
res, err := redis.Strings(redisConn.Do("zrange", "cars", 0, -1))
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```

<code>zrange KEY 0 -1 withscores</code> - показать всё мн-во с весами значений
```golang
res, err := redis.StringMap(redisConn.Do("zrange", "cars", 0, -1, "withscores"))
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```

<a name="zrevrange" id="zrevrange"></a>
###  zrevrange
<code>zrevrange KEY 0 -1 withscores</code> - показать всё мн-во сортированного в обратном порядке с весами значений
```golang
res, err := redis.StringMap(redisConn.Do("zrevrange", "cars", 0, -1, "withscores"))
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```

<a name="zcount" id="zcount"></a>
### zcount
<code>zcount KEY SCORE1 SCORE2 etc...</code> - показать кол-во объектов с рейтингами между score1 и score2
```golang
res, err := redis.Int(redisConn.Do("zcount", "cars", 81, 99))
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```

<a name="zrank" id="zrank"></a>
### zrank
<code>zrank KEY ITEM</code> - показать индекс эл-та в мн-ве
```golang
res, err := redis.Int(redisConn.Do("zrank", "cars", "toyota"))
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```

<a name="zincrby" id="zincrby"></a>
### zincrby
<code>zincrby KEY INCREMENT ITEM</code> - увеличить вес эл-та на указанное число
```golang
res, err := redis.Int(redisConn.Do("zincrby", "cars", 100, "toyota"))
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```

<a name="zscore" id="zscore"></a>
### zscore
<code>zscore KEY ITEM</code> - показать вес эл-та
```golang
res, err := redis.Int(redisConn.Do("zscore", "cars", "toyota"))
if err != nil {
    log.Fatalln(err)
}

fmt.Println(res)
```