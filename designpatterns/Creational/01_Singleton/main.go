package main

import (
	"fmt"
	"go-designpatterns/Creational/01_Singleton/singleton"
)

func callC() {
	c, err := singleton.GetConnection()
	if err != nil {
		panic(err)
	}
	fmt.Println("callC func:", c)
}

func main() {
	fmt.Println("-----------")
	a, err := singleton.GetConnection()
	if err != nil {
		panic(err)
	}
	fmt.Println("a:", a)
	fmt.Println("-----------")
	b, err := singleton.GetConnection()
	if err != nil {
		panic(err)
	}
	fmt.Println("b:", b)
	fmt.Println("-----------")
	callC()
	fmt.Println("-----------")
	fmt.Println("Closing DB Connection...")
	err = singleton.CloseConnection()
	if err != nil {
		panic(err)
	}
	fmt.Println("-----------")
	d, err := singleton.GetConnection()
	if err != nil {
		panic(err)
	}
	fmt.Println("d:", d)
	fmt.Println("-----------")
	e, err := singleton.GetConnection()
	if err != nil {
		panic(err)
	}
	fmt.Println("e:", e)
	fmt.Println("-----------")
}
