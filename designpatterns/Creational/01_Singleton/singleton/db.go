//singleton/db.go
package singleton

import (
	"database/sql"
	"fmt"
	//postgres driver
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type conn struct {
	db *sql.DB
}

var connInstance *conn

const (
	host     = "172.17.0.2"
	port     = 5432
	user     = "singleton"
	password = "Qwerty12345"
	dbname   = "singleton"
)

//GetConnection - returns conn struct instance
func GetConnection() (*conn, error) {

	connectionInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	if connInstance == nil {

		db, err := sql.Open("postgres", connectionInfo)
		if err != nil {
			return nil, err
		}
		err = db.Ping()
		if err != nil {
			return nil, err
		}
		connInstance = &conn{
			db: db,
		}
		fmt.Println("New Instance created.")
		fmt.Println("Stats:", connInstance.db.Stats())
		return connInstance, nil
	}
	fmt.Println("Old Instance returned.")
	fmt.Println("Stats:", connInstance.db.Stats())
	return connInstance, nil
}

//CloseConnection - closes db connection and clears conn struct instance
func CloseConnection() error {
	if connInstance == nil {
		return nil
	}
	err := connInstance.db.Close()
	if err != nil {
		return err
	}
	connInstance = nil
	return nil

}
