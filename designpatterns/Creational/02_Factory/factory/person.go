package factory

import (
	"errors"
	"fmt"
)

type Employeer interface {
	ViewPage()
}

type User struct {
}

type Admin struct {
}

func (u *User) ViewPage() {
	fmt.Println("View User")
}

func (a *Admin) ViewPage() {
	fmt.Println("View Admin")
}

func CreatePerson(p string) (Employeer, error) {
	switch p {
	case "user":
		return new(User), nil
	case "admin":
		return new(Admin), nil
	default:
		err := errors.New("not specified person")
		return nil, err
	}
}

func WhoViewPage(e Employeer) {
	e.ViewPage()
}
