package main

import (
	"fmt"
	"go-designpatterns/Creational/02_Factory/factory"
	"log"
)

func main() {
	u, err := factory.CreatePerson("user")
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("%T\n", u)
	factory.WhoViewPage(u)

	a, err := factory.CreatePerson("admin")
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("%T\n", a)
	factory.WhoViewPage(a)

	e, err := factory.CreatePerson("some person")
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("%T\n", e)
	factory.WhoViewPage(e)
}
