# Design Patterns for Golang
Данный документ является резюмирующей компиляцией прочитанных книг 
* [Go Design Patterns by Mario Castro Contreras](https://www.packtpub.com/application-development/go-design-patterns)
* [Погружение в ПАТТЕРНЫ ПРОЕКТИРОВАНИЯ. Александр Швец](https://refactoring.guru/ru/design-patterns/book)

и не в коем случае не претендует на уникальность или авторство ниже изложенного материала.


> Дополнительный матерьял на который следует обратить внимание
> * [Wiki Design Patterns](https://ru.wikipedia.org/wiki/Design_Patterns)
> * [GitHub: Go-Patterns by tmrts](https://github.com/tmrts/go-patterns)
> * [GitHub: Design Patterns For Humans by kamranahmedse](https://github.com/kamranahmedse/design-patterns-for-humans)

# Table of contents
1. [Creational Patterns](#creational)
   1. [Singleton](#singleton)
      -  [Golang Example](#singletonexmpl)
   2. [Factory](#factory)
      -  [Golang Example](#factoryexmpl)
   3. ~~[Abstract Factory]()~~
   4. ~~[Builder]()~~
   5. ~~[Prototype]()~~

2. ~~[Structural](#structural)~~

3. ~~[Behavioral](#behavioral)~~
   
<a name="creational"></a>
## Creational Patterns
Данные паттерны предоставляют механизмы  для безопасного создания объекта или группы объектов, повышая гибкость кода.
<a name="singleton"></a>
## Singleton pattern / Паттерн одиночка 
Самый популярный паттерн, он создает единственный экземпляр объекта, гарантирует его уникальность и обеспечивает доступ только к этому  экземпляру.

Суть реализации паттерна сводится к тому, чтобы скрыть конструктор и создать публичную функцию, которая будет контролировать жизненный цикл экземпляра. При первом вызове функции - создается экземпляр и затем повторно используется всеми частями приложения которым необходимо поведение данного экземпляра. Из какой точки бы не вызвали функцию всегда будет отдаваться один и тот же экземпляр.

Например данный паттерн можно использовать при:
* Выполнении различных запросов к БД используя одно и тоже подключение
* Выполнении различных задач на сервере по SSH используя одно и тоже подключение
* Если необходимо ограничить доступ к переменной, то Singleton используется, как единая точка доступа к данной переменной
* Если необходимо ограничить количество запросов в каких-либо местах, вы создаете Singleton экземпляр для выполнения этих запросов в допустимом объеме

<a name="singletonexmpl"></a>
**Golang Example**

Рассмотрим пример использования Singleton на практике. Создадим объект подключения к postgres базе и получим доступ к этому объекту из различных мест приложения. 

Для нашей песочницы создадим postgres контейнер
~~~~
docker container run \
-d \
--rm \
-e POSTGRES_DB=singleton \
-e POSTGRES_USER=singleton \
-e POSTGRES_PASSWORD=Qwerty12345 \
--name singleton \
postgres
~~~~
Узнаем <mark>ip postgres</mark> контейнера
~~~~
docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' singleton
>172.17.0.2
~~~~
Эти данные нам понадобятся при подключении к БД в нашем приложении
~~~~
host=172.17.0.2
user=singleton
password=Qwerty12345
dbname=singleton
~~~~
Структура проекта примера будет следующая
~~~~
mkdir -p $GOPATH/src/go-designpatterns/Creational/01_Singleton/singleton
cd $GOPATH/src/go-designpatterns/Creational/01_Singleton/
ls -alFR
.
+--main.go
+--singleton/
|  +--db.go
~~~~
Где в <mark>main.go</mark> будет располагаться код вызова экземпляра, а <mark>singleton/db.go</mark> - это пакет отвечающий за создание экземпляра подключения и возвращение экземпляра при его вызове
```go
//singleton/db.go

package singleton

import (
	"database/sql"
	"fmt"
	//postgres driver
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

//Создаём новый тип conn,который имеет поле db типа *sql.DB
type conn struct {
	db *sql.DB
}
//Объявляем приватную переменную connInstance типа *conn
var connInstance *conn
//Константы для подключения к postgres контейнеру
const (
	host     = "172.17.0.2"
	port     = 5432
	user     = "singleton"
	password = "Qwerty12345"
	dbname   = "singleton"
)

//GetConnection - публичная функция создает подключение и возвращает экземпляр connInstance
func GetConnection() (*conn, error) {

   connectionInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
      host, port, user, password, dbname)
   //проверка на существование экземпляра
   if connInstance == nil {
      db, err := sql.Open("postgres", connectionInfo)
      if err != nil {
         return nil, err
      }
      err = db.Ping()
      if err != nil {
         return nil, err
      }
      connInstance = &conn{
         db: db,
      }
      fmt.Println("New Instance created.")
      fmt.Println("Stats:", connInstance.db.Stats())
      return connInstance, nil
   }
   fmt.Println("Old Instance returned.")
   fmt.Println("Stats:", connInstance.db.Stats())
   return connInstance, nil
}

//CloseConnection - данная публичная функция закрывает подключение и "обнуляет" connInstance
func CloseConnection() error {
	if connInstance == nil {
		return nil
	}
	err := connInstance.db.Close()
	if err != nil {
		return err
	}
	connInstance = nil
	return nil

}
```
```go
//main.go
package main

import (
	"fmt"
	"go-designpatterns/Creational/01_Singleton/singleton"
)
//В качестве примера сделаем вызов connInstance не из функции main
func callC() {
	c, err := singleton.GetConnection()
	if err != nil {
		panic(err)
	}
	fmt.Println("callC func:", c)
}

func main() {
   //первый вызов connInstance
   fmt.Println("-----------")
   a, err := singleton.GetConnection()
   if err != nil {
      panic(err)
   }
   fmt.Println("a:", a)
   //повторный вызов connInstance
   fmt.Println("-----------")
   b, err := singleton.GetConnection()
   if err != nil {
      panic(err)
   }
   fmt.Println("b:", b)
   //вызов connInstance в другой функции
   fmt.Println("-----------")
   callC()
   //закрываем соединение с БД
   fmt.Println("-----------")
   fmt.Println("Closing DB Connection...")
   err = singleton.CloseConnection()
   if err != nil {
      panic(err)
   }
   //делаем вызов connInstance после закрытия
   fmt.Println("-----------")
   d, err := singleton.GetConnection()
   if err != nil {
      panic(err)
   }
   fmt.Println("d:", d)
   //вызаваем connInstance еще раз
   fmt.Println("-----------")
   e, err := singleton.GetConnection()
   if err != nil {
   panic(err)
   }
   fmt.Println("e:", e)
   fmt.Println("-----------")
}
```

Запускаем и изучаем результат
~~~~
go run main.go
-----------
New Instance created.
Stats: {0 1 0 1 0 0s 0 0}
a: &{0xc0000ea0c0} //при первом вызове создается новый объет 
-----------
Old Instance returned.
Stats: {0 1 0 1 0 0s 0 0}
b: &{0xc0000ea0c0} //как видим при повторном обращении нам возвращается тот же самый объект
-----------
Old Instance returned.
Stats: {0 1 0 1 0 0s 0 0}
callC func: &{0xc0000ea0c0} //вызвали объект в другой части приложения и нам вернулся всё тот же объет
-----------
Closing DB Connection... //закрываем соединение и обнуляем объект
-----------
New Instance created.
Stats: {0 1 0 1 0 0s 0 0}
d: &{0xc0000ea1} //создали новый объект
-----------
Old Instance returned.
Stats: {0 1 0 1 0 0s 0 0}
e: &{0xc0000ea1} //при повторном вызове нам вернулся объект созданный на шаге d
-----------
~~~~
<a name="factory"></a>
## Factory pattern / Фабричный паттерн
Второй по популярности паттерн. Он определяет интерфейс для создания семейства объектов. В момент создания пользователь сам орпеделяет кокого типа ему необходим объект. Т.е. данный паттерн реализует полиморфизм, создавая метод/функцию, общую для данных различного типа. Позволяее системе оставаться гибкой/расширяемой при необходимости добавления новых типов данных.

<a name="factoryexmpl"></a>
**Golang Example**

Рассмотрим пример, создадим сотрудников разного типа - User и Admin используя данный паттерн.
~~~~
mkdir -p $GOPATH/src/go-designpatterns/Creational/02_Factory/factory
cd $GOPATH/src/go-designpatterns/Creational/02_Factory/
ls -alFR
.
+--main.go
+--factory/
|  +--person.go
~~~~

```go
//factory/person.go
package factory

import (
	"errors"
	"fmt"
)
//создаёмм тип Employeer являющийся интерфейсом,данным типом будут являтся все объекты обладающие методом ViewPage
type Employeer interface {
	ViewPage()
}
//создаём тип User
type User struct {
}
//создаём  тип Admin
type Admin struct {
}
//определяем метод ViewPage для типа User
func (u *User) ViewPage() {
	fmt.Println("View User")
}
//определяем метод ViewPage для типа Admin
func (a *Admin) ViewPage() {
	fmt.Println("View Admin")
}
//создаём функцию возвращающую в зависимости от аргумента функции типа Employeer
func CreatePerson(p string) (Employeer, error) {
	switch p {
	case "user":
		return new(User), nil
	case "admin":
		return new(Admin), nil
	default:
		err := errors.New("not specified person")
		return nil, err
	}
}
//функция вызывает ViewPage() в зависимости от типа объета
func WhoViewPage(e Employeer) {
	e.ViewPage()
}
```
```go
//main.go
package main

import (
	"fmt"
	"go-designpatterns/Creational/02_Factory/factory"
	"log"
)

func main() {
   //создаём объект типа User
   u, err := factory.CreatePerson("user")
   if err != nil {
      log.Fatalln(err)
   }
   fmt.Printf("%T\n", u)
   factory.WhoViewPage(u)
   //создаём объект типа Admin
   a, err := factory.CreatePerson("admin")
   if err != nil {
      log.Fatalln(err)
   }
   fmt.Printf("%T\n", a)
   factory.WhoViewPage(a)
   //проверяем, как поведет себя функция если передать не user или не admin
   e, err := factory.CreatePerson("some person")
   if err != nil {
      log.Fatalln(err)
   }
   fmt.Printf("%T\n", e)
   factory.WhoViewPage(e)
   }
```
Запускаем и изучаем результат
~~~~
go run main.go
*factory.User //вызвали factory.CreatePerson для создания User
View User
*factory.Admin //вызвали factory.CreatePerson для создания Admin
View Admin
2018/12/12 15:55:40 not specified person
exit status 1
~~~~
Из примера видно, как путём вызова одной и той же функции можно создавать объекты раличного типа. Данный паттерн позволяет приложению оставаться легко модифицируемой в случае необходимости добавления объектов нового типа. 

<a name="structural"></a>
# Structural patterns
Данные паттерны объясняют, как собирать объекты и классы в структуры, сохраняя гибкость и эффективность данной структуры.
<a name="behavioral"></a>
# Behavioral patterns
Данные паттерны отвечают за коммуникации между объектами.




