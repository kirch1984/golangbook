> - [Intruduction](#intruduction)
>	- [JSON (JavaScript Object Notation)](#json-javascript-object-notation)
>	- [Primitive JSON](#primitive-json)
>	- [Object JSON](#object-json)
>	- [Array JSON](#array-json)
>- [JSON with GO](#json-with-go)
 


<a name="intruduction"></a>
## Intruduction
[Introducing JSON](https://www.json.org/)

[JSON and Go](https://blog.golang.org/json-and-go)
<a name="json"></a>
### JSON (JavaScript Object Notation)

Это формат обмена данных. Чаще всего используется в web для передачи информации между back-end и front-end (написанным на JavaScript) сторонами.
JSON поддерживает примитивные типы такие как:
<code>strings, numbers, booleans, и null</code>
и структурированные типы такие как:
<code>array и object</code>
<a name="primitive_json"></a>
### Primitive JSON
```
string - "Sanity is not a statistical."
int- 1984
bool - true/false
null  - null
```
<a name="object_json"></a>
### Object JSON
Объекты в JSON — это структура данных неупорядоченных коллекций заключенных в <code>{}</code>, содержащая 0 или более эл-ов состоящих из пар
<code>"name": value</code>.
Где <code>value</code> это любое значение типа: 
<code>strings,numbers,booleans,null,array,object</code>
```
{
    "id:",
    "UserInfo": {
        "First Name":"Tony",
	"Lasr Name":"Hawk",
	"Age": 23,
	"Height": 180,
	"Weight":75
    },
   "Position": "Develop",
   "Role": "Golang developer",

"Skills":["Goalng","JS","ReactJS","Bash","Python","Mysql","MongoDB","Redis","RabbitMQ","HTML","CSS","Bootstrap"]
}
```
<a name="array_json"></a>
### Array JSON
Это структура данных заключенная в <code>[]</code> и содержащая 0 или более эл-ов типа: <code>int,string,bool,null,array,object</code>
```
["Goalng","JS","ReactJS","Bash","Python","Mysql","MongoDB","Redis","RabbitMQ","HTML","CSS","Bootstrap"]
```
<a name="json_with_go"></a>
### JSON with GO
[package json](https://godoc.org/encoding/json)

[Convert JSON to Go struct](https://mholt.github.io/json-to-go/)

Основными фун-ми в пакете <code>json</code>, который позволяет работать с <code>json</code> форматом в golang, являются <code>Marshal</code> (GO->JSON) и <code>Unmarshal</code> (JSON->GO) — используются для конвертации данных из golang в <code>json</code> и обратно соответственно.
```golang
package main
import (
	"fmt"
	"encoding/json"
	"log"
)
type person struct {
	FName  string `json:"FirstName"`
	LName  string `json:"LastName"`
	Age    int
	Skills []string `json:"Stack"`
}
func main() {
        //здесь p - будет являтся примером golang данных 
	p := &person{"Tony", "Hawk", 23, []string{"Go", "JS", "Bash", "Python"}}

        //конвертируем из golang в json
	//Marshal example
	j, err := json.Marshal(p)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("%s\n", j)
        
        //тоже самое что и Marshal, но в более "удобоваримом" виде
	//MarshalIndent example
	d, err := json.MarshalIndent(p, "", "    ")
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("%s\n", d)

        //конвертируем из json в golang 
	//UnMarshal example
	var name struct {
		FirstName string
	}
	err = json.Unmarshal(j, &name)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(name)
}
```
[https://play.golang.org/p/RivlqIpJmJp](https://play.golang.org/p/RivlqIpJmJp)

Помимо <code>Marshal/Unmarshal</code> имеются фун-ии <code>Encoder/Decoder</code>.
Разница использования зависит — откуда данные получены и куда в последствии будут отправлены. Если данные поступают из <code>io.Reader</code> потока, например <code>http.Request</code>, то надо использовать <code>Decoder/Encoder</code>. Если данные — это просто некоторая переменная в памяти, то <code>Unmarshal/Marshal</code>.
Напишем небольшой пример программы с возможностью просмотра списка пользователей и добавления нового пользователя в данный список.
При обращении к нашему серверу в корень <code>"/"</code> будет отдаваться список всех текущих пользователей.
Чтоб добавить пользователя нужно будет обратиться к <code>"/add"</code> методом <code>POST</code> и передать необходимые значения.
Для этого будем использовать команду
```
curl -X POST -H "Content-Type: application/json" -d '{"id":2,"login":"james","fname":"James","role":"user"}' http://localhost:8080/add
```
Программа будет выглядить так:
```golang
package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

type user struct {
	ID        int    `json:"id"`
	UserName  string `json:"login"`
	FirstName string `json:"fname"`
	Role      string `json:"role"`
}

type users []*user

var us users

func init() {
        //в качестве примера создадим пользователя по умолчанию
	u1 := &user{1, "admin", "Tony", "admin"} 
	us = append(us, u1)
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", index).Methods("GET")
	r.HandleFunc("/add", add).Methods("POST")
	fmt.Println(":8080 Listen...")
	http.ListenAndServe(":8080", r)

}
func index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(&us)
}

func add(w http.ResponseWriter, r *http.Request) {
	var u = new(user)
	json.NewDecoder(r.Body).Decode(&u)
	us = append(us, u)
	fmt.Fprintf(w, "User added.\n")
}
```
После запуска программы при обращении к <code>"/"</code> увидим полный список пользователей, при запуске будет только один пользователь, который был создан в фун-ии <code>init()</code>
Далее в консоли выполним пару команд
```
curl -X POST -H "Content-Type: application/json" -d '{"id":2,"login":"james","fname":"James","role":"user"}' http://localhost:8080/add
```
```
curl -X POST -H "Content-Type: application/json" -d '{"id":3,"login":"mark","fname":"Mark","role":"user"}' http://localhost:8080/add
```
Теперь опять обратимся к <code>"/"</code>,сейчас видим что помимо пользователя Tony выдались данные еще двух пользователей.