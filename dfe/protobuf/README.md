<a id="contnent" name="contnent"></a>

- [Introduction](#introduction)
- [Настройка WorkFlow](#Настройка-workflow)
  - [Расширения для VSC](#Расширения-для-vsc)
  - [Setup Protoc Compiler](#setup-protoc-compiler)
  - [Golang Packages](#golang-packages)
- [Basic](#basic)
  - [Naming Conventions](#naming-conventions)
  - [Scalar Value Types](#scalar-value-types)
  - [Tag/Field Numbers](#tagfield-numbers)
  - [Repeated](#repeated)
  - [Comments](#comments)
  - [Default Values](#default-values)
  - [Enum](#enum)
  - [oneof](#oneof)
  - [map](#map)
  - [Embedded messages](#embedded-messages)
  - [Nested messages](#nested-messages)
  - [Imports messages](#imports-messages)
  - [Packages](#packages)
  - [Updating Protocol Rules](#updating-protocol-rules)
    - [Adding Fields](#adding-fields)
    - [Renaming Fields](#renaming-fields)
    - [Removing Fields](#removing-fields)
- [Go и Protocol Buffers](#go-и-protocol-buffers)
  - [option go_package](#option-go_package)
  - [read/write file](#readwrite-file)
  - [Enum,repeated](#enumrepeated)
  - [oneof](#oneof-1)
  - [map](#map-1)

<a name="introduction" id="introduction"></a>

# Introduction

[Up](#contnent)

[Developer Guide](https://developers.google.com/protocol-buffers/docs/overview)\
[Language Guide (proto3)](https://developers.google.com/protocol-buffers/docs/proto3)\
Protocol buffers - это гибкий, эффективный, автоматизированный механизм для сериализации структурированных данных - как XML, только меньше, быстрее и проще.\
Вы определяете один раз , как хотите чтоб ваши данный были структурированы, а затем можете использовать специально сгенерированный код для чтения/записи данных из/в различных потоков данных используя различные языки программирования. Вы даже можете обновить структуру данных и при этом не нарушить работу программы, которая была скомпилирована для данных старого формата.\
Вы указываете, как хотите структурировать данные определяя <code>protocol buffer message</code> в <code>.proto</code> файле. Каждый <code>protocol buffer message</code> - это небольшая логическая запись информации, содержащая набор пар ключ-значение. Простой пример <code>.proto</code> файла в котором определен <code>message</code> содержащий информацию о пользователи:
```protobuf
message User {
  int32 id = 1;
  string name = 2; 
  string email = 3;
}
```
Как вы видите, формат <code>message</code> прост — каждый <code>message</code> содержит уникальные пронумерованные поля(field tag/number), состоящие из имени поля(field name) и типа данных значения(field type) (int32/64,string,float,double,bool…)\
После того, как определили все <code>messages</code> в <code>.proto</code> вы запускаете компилятор <code>protoc</code> для генерации методов доступа к данным. Они предоставляют упрощенный доступ к полям, а также предоставляют методы для сериализации/парсинга всей структуры.\
Вы можете добавлять новыйе поля в <code>message</code> не переживая за поломку программы, новые поля будут просто игнорироваться при этом не вызывая ошибок в коде.

<a name="workflow" id="workflow"></a>
# Настройка WorkFlow

[Up](#contnent)

<a name="vsc" id="vsc"></a>
## Расширения для VSC
Для работы с protocol buffer в [Visual Studio Code](https://code.visualstudio.com/) необходимо установить два расширения:
> [vscode-proto3](https://marketplace.visualstudio.com/items?itemName=zxh404.vscode-proto3)\
> [Clang-Format](https://marketplace.visualstudio.com/items?itemName=xaver.clang-format)\
Затем установить пакет <code>clang-format</code> для вашей ОС.\
Установка для Ubuntu:
```
aptitude install clang-format-8.0
ln -s /usr/bin/clang-format-8 /usr/bin/clang-format
```
Либо вместо создания ссылка зайти в настройки vsc <code>File->Preferences->Settings</code>, найти ключ <code>clang-format.executable</code> и указать в качестве значения <code>/usr/bin/clang-format-8</code>.

<a name="protoccompiler" id="protoccompiler"></a>
## Setup Protoc Compiler
<code>protoc</code> из созданного файла <code>.proto</code>, в котором содержится нужная нам структура <code>message</code>, генерирует файл под определенный язык программирования.
В этом файле содержится темплэйт,созданный из <code>message</code> в <code>.proto</code>, который затем будет использоваться для создания объектов, а также различные методы для работы.\
Установка для Linux:
```
curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.9.0/protoc-3.9.0-linux-x86_64.zip
unzip protoc-3.9.0-linux-x86_64.zip -d protoc-3.9.0
sudo mv protoc-3.9.0/bin/* /usr/local/bin/
sudo mv protoc-3.9.0/include/* /usr/local/include/
```

<a name="gopackages" id="gopackages"></a>
## Golang Packages
Для работы в Go с protocol buffer нам понадобятся два пакета:
> [protoc-gen-go package](#https://godoc.org/github.com/golang/protobuf/protoc-gen-go/descriptor)\
> [proto package](#https://godoc.org/github.com/golang/protobuf/proto)
```
go get -u github.com/golang/protobuf/protoc-gen-go
go get -u github.com/golang/protobuf/proto
```
<a name="basic" id="basic"></a>
# Basic

[Up](#contnent)

Рассмотрим простой пример, предположим что мы хотим определить формат структуры User содержащую id,name,email,photo.\
Наш <code>user.proto</code> будет выглядть как:
```
syntax="proto3";

message User {
    int32 id = 1;
    string name = 2;
    string email = 3;
    bytes photo = 4;
}
```
Первой строкой идёт <code>syntax</code>, если мы хотим использовать <code>proto3</code>, а не <code>proto2</code>, то обязатеьно должны его указывать.\
Далее у нас идёт сама стурктура состоящая из полей. В каждом поле указывается тип,имя и тэг/номер.

<a name="namecon" id="namecon"></a>
## Naming Conventions

[Up](#contnent)

[Style Guide](https://developers.google.com/protocol-buffers/docs/style)\
[Uber style guiding](https://github.com/uber/prototool/blob/dev/etc/style/uber1/uber1.proto)

* CamelCase для имён структур
* underscore_separated_names для имён полей
* CamelCase для имени Enum и CAPITAL_WITH_UNDERSCORE для его полей
```
syntax="proto3";

message PersonInfo {
    int32 id = 1;
    string first_name = 2;
    string last_name = 3;
    Department dep = 4;
}
enum DepartmentInfo {
  DEV_DEP = 0;
  TECH_DEP = 1;
  SALES_DEP = 2;
  BUH_DEP = 3;
  ADVERT_DEP = 4;
}
```

<a name="scalarvaluetypes" id="scalarvaluetypes"></a>
## Scalar Value Types

[Up](#contnent)

<code>protobuf</code> поддерживает следующие [скалярные типы](https://developers.google.com/protocol-buffers/docs/proto3#scalar):
* Numbers: double (64 bits),float (32 bits),int32,int64,uint32,uint64,sint32,sint64,fixed32,fixed64,sfixed32,sfixed64
* Boolean: true/false
* String - строка всегда должна содержать UTF-8 закодированй текст или 7-bit ASCII
* Bytes - всегда представляет последовательность массива байт

<a name="tag" id="tag"></a>
## Tag/Field Numbers

[Up](#contnent)

Имя поля не так важно как тэг - уникальное  число содержащееся в каждой строке в конце. Данный номер используется для идентификации поля в двоичном представлении структуры. Номер уникальный и не может быть изменен после того как начали использовать стуктуру в программе.\
Номера 1-15 занимают 1 байт и ипользуются для наиболее частых элементов структуры. Номер 16-2047 занимаю уже 2 байта.\
Наименьшее число 1, наибольшее 2^29 - 1 или 536,870,911. Также нельзя использовать 19000-19999, они зарезервированы.

<a name="repeated" id="repeated"></a>
## Repeated

[Up](#contnent)

Если необходимо определить поле,которое будет в дальнейшем содержать список или массив, то неоходимо использовать флаг <code>repeated</code> в начале поля. Такое поле может содержать 0 элеменов.\
Давайте добавим поле <code>phone_numbers</code> поле в струсткуру User
```
syntax="proto3";

message User {
    int32 id = 1;
    string name = 2;
    string email = 3;
    bytes photo = 4;
    repeated string phone_numbers = 5;
}
```
<a name="comments" id="comments"></a>
## Comments

[Up](#contnent)

В <code>protobuf</code> два вида комментарив:
* // - однострочный
* /* ... */ - многострочный

Добавим немного комментариев в пример с User
```
// syntax for proto3
syntax="proto3";
/*
User used for identify users
*/
message User {
    int32 id = 1;
    string name = 2;
    string email = 3;
    bytes photo = 4;
    //list of phone numbers
    repeated string phone_numbers = 5;
}
```
<a name="defaultvalues" id="defaultvalues"></a>
## Default Values

[Up](#contnent)

<code>protobuf</code> как и <code>Golang</code> имеет такую особенность - значение по умолчанию для полей. Т.е. если поле не задано, то ему присваивается значение ([default values](https://developers.google.com/protocol-buffers/docs/proto3#default)) по умолчанию согласно типу поля:
* bool - false
* number (int,float...) - 0
* string - пустая строка
* repeated - пустой список
* bytes - пустой байт
* enum - первое определенное значение

<a name="enum" id="enum"></a>

## Enum

[Up](#contnent)

Если необходимо что бы поле имело только одно из определённых значений, то используется <code>Enum</code>. Нужно помнить - первое значение всегда будет являеться по умолчанию для данного поля и тэг/номер должен начинаться с 0.\
Добавим такое поле department в наш пример User
```
// syntax for proto3
syntax="proto3";
/*
User used for identify users
*/
message User {
    int32 id = 1;
    string name = 2;
    string email = 3;
    bytes photo = 4;

    //list of phone numbers
    repeated string phone_numbers = 5;

    //Department defines the list of departments in our company
    enum Department {
      DEV = 0;
      TECH = 1;
      SALES = 2;
      BUH = 3;
      ADVERT = 4;
    }
    
    Department dep = 6;
}
```
<a name="oneof" id="oneof"></a>

## oneof

[Up](#contnent)

Когда необходимо использовать только одно поле из имеющихся вариантов
```
// person.proto
syntax="proto3";

package person;

message Person{
    uint32 id = 1;
    oneof login{
        string email = 2;
        string full_name = 3;
    }
}
``` 
* oneof поле не может использоваться вмеcте с repeated

<a name="map" id="map"></a>

## map

[Up](#contnent)

Данный тип данных, как и в Go, используется для создания неупорядоченных коллекций пар видп key-value.

```
// person.proto
syntax="proto3";

package person;

message Person{
  uint32 id = 1;
  string first_name = 2;
  map<string,int32> costs = 3;
}
```

* map поле не может использоваться вмеcте с repeated
* в качестве ключа может быть любой скалярный тип данных кроме float  и bytes 

<a name="embeddedmsg" id="embeddedmsg"></a>

## Embedded messages

[Up](#contnent)

Можно использовать другую структуру, как тип для поля.\
Определим новую BirthDay стукруру и используем ее как тип для нового поля birthday структоры User.
```
syntax="proto3";
/*
User used for identify users
*/
message User {
    int32 id = 1;
    string name = 2;
    string email = 3;
    bytes photo = 4;

    //list of phone numbers
    repeated string phone_numbers = 5;

    //Department defines the list of departments in our company
    enum Department {
      DEV = 0;
      TECH = 1;
      SALES = 2;
      BUH = 3;
      ADVERT = 4;
    }
    
    Department department = 6;

    BirthDay birthday = 7;
}

message BirthDay {
  uint32 day = 1;
  uint32 month = 2;
  uint32 year = 3;
}
```

<a name="nestedmsg" id="nestedmsg"></a>
## Nested messages

[Up](#contnent)

Можно определять стурктуру внутри структуры и использовать ее как тип поля.\
Добавим внутри структуры User новую структуру Address и используем ее, как тип для нового поля addresses.
```
syntax="proto3";
/*
User used for identify users
*/
message User {
    int32 id = 1;
    string name = 2;
    string email = 3;
    bytes photo = 4;

    //list of phone numbers
    repeated string phone_numbers = 5;

    //Department defines the list of departments in our company
    enum Department {
      DEV = 0;
      TECH = 1;
      SALES = 2;
      BUH = 3;
      ADVERT = 4;
    }
    
    Department department = 6;

    BirthDay birthday = 7;

    message Address {
      string country = 1;
      string city = 2;
      string street = 3;
      string apt = 4;
      string zip_code= 5;
    }
    repeated Address addresses = 8;

}

message BirthDay {
  uint32 day = 1;
  uint32 month = 2;
  uint32 year = 3;
}
```
<a name="imports" id="imports"></a>
## Imports messages

[Up](#contnent)

Отдельные структуры можно выносить в отделный файл, а затем импортировать.\
Вынесим стурктуру BirthDay в файл <code>birthday.proto</code> и затем ипрортируем в файл <code>user.proto</code> содержащий структуру User.

```
//birthday.proto
syntax="proto3";

message BirthDay {
    uint32 day = 1;
    uint32 month = 2;
    uint32 year = 3;
}
```
```
// user.proto
syntax="proto3";
/*
i.e. if you work in this folder ~/code/go/src/golangBook/dfe/protobuf
and the root of a project is ~/code/go/src
then you point golangBook/dfe/protobuf/birthday.proto for import
*/
import "golangBook/dfe/protobuf/birthday.proto";

/*
User used for identify users
*/
message User {
    int32 id = 1;
    string name = 2;
    string email = 3;
    bytes photo = 4;

    //list of phone numbers
    repeated string phone_number = 5;

    //Department defines the list of departments in our company
    enum Department {
      DEV = 0;
      TECH = 1;
      SALES = 2;
      BUH = 3;
      ADVERT = 4;
    }
    
    Department department = 6;

    BirthDay birthday = 7;

    message Address {
      string country = 1;
      string city = 2;
      string street = 3;
      string apt = 4;
      string zip_code = 5;
    }
    repeated Address addresses = 8;

}
```

<a name="packages" id="packages"></a>
## Packages

[Up](#contnent)

Спецификатор <code>package</code> в <code>.proto</code> файле позволяет:
1. избежать конфликта имен структур 
2. когда код скомпилирован он помещается в указаный пакет.
   
В файле <code>birthday.proto</code> укажем, что это теперь <code>bd</code> пакет, а в файле <code>user.proto</code> что <code>user</code>. Соответственно, чтоб в файле  <code>user.proto</code> использовать структуру <code>BirthDay</code> необходимо указывать пакет.
```
//birthday.proto
syntax="proto3";

package bd; //теперь это пакет bd

message BirthDay {
    uint32 day = 1;
    uint32 month = 2;
    uint32 year = 3;
}
```
```
// user.proto
syntax="proto3";

import "golangBook/dfe/protobuf/birthday.proto";
package user; //теперь это пакет user
/*
User used for identify users
*/
message User {
    int32 id = 1;
    string name = 2;
    string email = 3;
    bytes photo = 4;

    //list of phone numbers
    repeated string phone_number = 5;

    //Department defines the list of departments in our company
    enum Department {
      DEV = 0;
      TECH = 1;
      SALES = 2;
      BUH = 3;
      ADVERT = 4;
    }
    
    Department department = 6;

    bd.BirthDay birthday = 7; //<-- указываем bd пакет

    message Address {
      string country = 1;
      string city = 2;
      string street = 3;
      string apt = 4;
      string zip_code = 5;
    }
    repeated Address addresses = 8;

}
```

<a name="updprotrules" id="updprotrules"></a>
## Updating Protocol Rules

[Up](#contnent)

* Нельзя менять номера поля
* Можно добавлять новые поля
* Если встречается неизвестное поле, то старый код игнорирует его
* поля могут удаляться, но освободившийся номер нельзя переиспользовать. В таких случаях либо неисплоьзуемое поле переименовывают, добавляя префикс "OBSOLETE_" к имени, либо номера резервируют
* некоторые типы данных полей можно менять один на другой (например int32, uint32, int64, uint64, bool)

<a name="addfield" id="addfield"></a>
### Adding Fields

[Up](#contnent)

В структуру можно без проблем добавлять новые поля
```
// person.proto
syntax="proto3";

message Person {
  uint32 id = 1;
  string fname = 2;
}
```
Добавим поле `lname`
```
// person.proto
syntax="proto3";

message Person {
  uint32 id = 1;
  string fname = 2;
  string lname = 3;
}
```
* код при встрече нового поля игнорирует его
* код при встрече старой структуры (без поля) будет использовать значение по умолчанию для данного поля 

<a name="renamefield" id="renamefield"></a>
### Renaming Fields

[Up](#contnent)

Можно без проблем переименовывать поля 
```
// person.proto
syntax="proto3";

message Person {
  uint32 id = 1;
  string fname = 2;
}
```
Переименуем поле `fname`
```
// person.proto
syntax="proto3";

message Person {
  uint32 id = 1;
  string first_name = 2;
}
```

<a name="removefield" id="renamefield"></a>
### Removing Fields

[Up](#contnent)

Можно без проблем удалять поля, но нельзя переиспользовать номер удаленного поля
```
// person.proto
syntax="proto3";

message Person {
  uint32 id = 1;
  string fname = 2;
}
```
Удалим поле `fname`
```
// person.proto
syntax="proto3";

message Person {
  uint32 id = 1;
}
```
* код при стрече стурктуры без поля использует значение по умолчанию для данного поля
* при удалении поля всегда нужно резервировать удаленное имя поля и его номер 

```
// person.proto
syntax="proto3";

message Person {
  reserved 2;
  reserved "fname";
  uint32 id = 1;
}
```

<a name="goproto" id="goproto"></a>
# Go и Protocol Buffers

[Up](#contnent)

Теперь перейдём к примерам использования <code>protobuf</code> с Go.\
Создадим проект в папке:\
<code>~/code/go/src/protobuf</code>\
Опишем структуру <code>Car</code> в фале <code>~/code/go/src/protobuf/src/car.proto</code>
```
// car.proto
syntax="proto3";

package car;

message Car {
    string company=1;
    string model=2;
    uint32 price=3;
    uint32 year=4;
    uint32 mileage=5;
    string color=6;
    float engine_capacity=7;
    uint32 horse_power=8;

}
```
Компилируем\
<code>
mkdir car\
protoc -I=src/ --go_out=car src/car.proto
</code>

Получаем файл
```
car/car.pb.go
```
Вкратце данный файл будет содержать нашу структуру <code>Car</code> уже адаптированную для Go и ряд методов для работы с данной структурой.\
В файле <code>main.go</code> создадим экземпляр данной структуры и выведим результат.
```go
package main

import (
	"fmt"
	"protobuf/car"
)

func main() {
	hondaInsight := &car.Car{
		Company:        "Honda",
		Model:          "Insight",
		Price:          520000,
		Year:           2010,
		Mileage:        23000,
		Color:          "black",
		EngineCapacity: 1.3,
		HorsePower:     88,
	}
	fmt.Printf("%v\n", hondaInsight)
	hondaInsight.Color = "white"
	fmt.Printf("%v\n", hondaInsight.GetColor())
}
```

<a name="optgopkg" id="optgopkg"></a>
## option go_package

[Up](#contnent)

Данная опция позволяет перезаписать настройки <code>package</code> опции.

```
// car.proto
syntax="proto3";

package car;
option go_package="carpb"; //<--добавили

message Car {
    string company=1;
    string model=2;
    uint32 price=3;
    uint32 year=4;
    uint32 mileage=5;
    string color=6;
    float engine_capacity=7;
    uint32 horse_power=8;

}
```
Компилируем\
<code>
mkdir carpb\
protoc -I=src/ --go_out=carpb src/car.proto</code>\
Теперь необходимо внести изменеия в <code>main.go</code> файл, потому что теперь пакет <code>carpb</code>, а не <code>car</code>.
```go
package main

import (
	"fmt"
	"protobuf/carpb"
)

func main() {
	hondaInsight := &carpb.Car{ //<--меняем car на carpb
		Company:        "Honda",
		Model:          "Insight",
		Price:          520000,
		Year:           2010,
		Mileage:        23000,
		Color:          "black",
		EngineCapacity: 1.3,
		HorsePower:     88,
	}
	fmt.Printf("%v\n", hondaInsight)
	hondaInsight.Color = "white"
	fmt.Printf("%v\n", hondaInsight.GetColor())
}
```

<a name="rwfile" id="rwfile"></a>
## read/write file

Воспользуемся той же самой структурой <code>Car</code> и реализуем запись в файл и чтение из файла.

[Up](#contnent)

```go
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"protobuf/car"

	"github.com/golang/protobuf/proto"
)

func main() {
	hondaInsight := &carpb.Car{
		Company:        "Honda",
		Model:          "Insight",
		Price:          520000,
		Year:           2010,
		Mileage:        23000,
		Color:          "black",
		EngineCapacity: 1.3,
		HorsePower:     88,
	}
	fname := "HICar.bin"
	//struct-->protobuf-->toFile
	err := wFile(fname, hondaInsight)
	if err != nil {
		log.Fatalln(err)
	}
	//fromFile-->protobuf-->struct
	hi, err := rFile(fname)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(hi)
}

func wFile(fname string, car *carpb.Car) error {
	data, err := proto.Marshal(car)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(fname, data, 0664)
	if err != nil {
		return err
	}
	log.Println("Data has been written.")
	return nil

}
func rFile(fname string) (*carpb.Car, error) {
	data, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}
	car := &carpb.Car{}
	err = proto.Unmarshal(data, car)
	if err != nil {
		return nil, err
	}
	return car, nil
}
```

<a name="enumrepgoex" id="enumrepgoex"></a>
## Enum,repeated

[Up](#contnent)

Попробуем усложнить пример стурктуры <code>Car</code> вынеся поле <code>color</code> в <code>enum</code> список <code>Colors</code> и добавив поле <code>change_of_oil</code> с опцией <code>repeated</code>.
```
// car.proto
syntax="proto3";

package car;
option go_package="carpb";

message Car {
    string company=1;
    string model=2;
    uint32 price=3;
    uint32 year=4;
    uint32 mileage=5;
    Colors color=6;
    float engine_capacity=7;
    uint32 horse_power=8;
    repeated uint32 change_of_oil =9;

}

enum Colors {
    White = 0;
    Black = 1;
    Red = 2;
    Green = 3;
    Blue = 4;
    Yellow = 5;
}
```
Компилируем\
<code>protoc -I=src/ --go_out=carpb src/car.proto</code>

```go
package main

import (
	"fmt"
	"protobuf/carpb"
)

func main() {
	hondaInsight := &carpb.Car{
		Company:        "Honda",
		Model:          "Insight",
		Price:          520000,
		Year:           2010,
		Mileage:        23456,
		Color:          carpb.Colors_Black,//<--используем константу из пакета carpb
		EngineCapacity: 1.3,
		HorsePower:     88,
		ChangeOfOil:    []uint32{5005, 10465, 17999, 23130},
	}
	fmt.Println(hondaInsight)
}
```
<a name="oneofgo" id="oneofgo"></a>
## oneof

[Up](#contnent)

Создаём новый файл `src/person.proto`, поле `login` будет использовать св-во `oneof`
```
// person.proto
syntax="proto3";

package person;

message Person{
    uint32 id = 1;
    oneof login{
        string email = 2;
        string full_name = 3;
    }
}
```
Компилируем\
`mkdir person`\
<code>protoc -I=src/ --go_out=person src/person.proto</code>

```go
package main

import (
	"fmt"
	"protobuf/person"
)

func main() {
	tonyHawk := &person.Person{
		Id: 1,
	}

	tonyHawk.Login = &person.Person_Email{
		Email: "tonyhawk@email.email",
	}
	fmt.Println(tonyHawk) //id:1 email:"tonyhawk@email.email"

	tonyHawk.Login = &person.Person_FullName{
		FullName: "Tony Hawk",
	}
	fmt.Println(tonyHawk) //id:1 full_name:"Tony Hawk"
}
```

<a name="mapgo" id="mapgo"></a>
## map

[Up](#contnent)

Добавим в структуру `Person` поле `Costs` типа `map`

```
// person.proto
syntax="proto3";

package person;

message Person{
  uint32 id = 1;
  string first_name = 2;
  map<string,int32> costs = 3;
}
```
Компилируем\
`protoc -I=src/ --go_out=person src/person.proto`

```go
package main

import (
	"fmt"
	"protobuf/person"
)

func main() {
	tonyHawk := &person.Person{
		Id: 1,
		Costs: map[string]int32{
			"skate":  100,
			"tshirt": 9,
		},
	}
	tonyHawk.Costs["snickers"] = 87
	fmt.Println(tonyHawk.Costs) //map[skate:100 snickers:87 tshirt:9]
}
```