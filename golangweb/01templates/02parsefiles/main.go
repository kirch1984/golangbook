package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	t, err := template.ParseFiles("index.gohtml")
	if err != nil {
		log.Fatalln("Can't read the template:", err)
	}

	err = t.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}

}
