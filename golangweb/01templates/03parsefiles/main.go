package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	//Парсим one.txt
	t, err := template.ParseFiles("one.txt")
	if err != nil {
		log.Fatalln("Can't read one.txt file.", err)
	}

	//Выводим сеодержимое one.txt
	err = t.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatalln("Can't Execute one.txt file.", err)
	}

	//Парсим сразу два файла "two.txt","three.txt"
	//т.е. сейчас переменная t содержит данные
	//трёх файлов-шаблонов
	t, err = t.ParseFiles("two.txt", "three.txt")
	if err != nil {
		log.Fatalln("Can't read files:", err)
	}
	//Теперь чтоб вывести один из нужных нам темплэйтов
	//необходимо воспользоваться методом ExecuteTemplate
	//Выводим сеодержимое two.txt
	err = t.ExecuteTemplate(os.Stdout, "two.txt", nil)
	if err != nil {
		log.Fatalln("Can't Execute two.txt file.", err)
	}
	//Выводим сеодержимое three.txt
	err = t.ExecuteTemplate(os.Stdout, "three.txt", nil)
	if err != nil {
		log.Fatalln("Can't Execute three.txt file.", err)
	}
	//Выводим сеодержимое one.txt
	err = t.ExecuteTemplate(os.Stdout, "one.txt", nil)
	if err != nil {
		log.Fatalln("Can't Execute one.txt file.", err)
	}
	//Если теперь воспользоваться методом Execute
	//тов результате нам будет выведен первый
	//распарсенный файл - one.txt
	err = t.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatalln("Can't Execute one.txt file.", err)
	}
}
