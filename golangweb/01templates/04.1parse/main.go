package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	//переменная которую передадим в шаблон
	name := "Mark"
	//будем парсить данную переменную(строку)
	text := `
	<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
	<h1>Hello,{{.}}!</h1>
</body>
</html>
`
	//парсим строку и назначаем данному шаблону имя index
	//чтоб позже вызвать его с помощью ExecuteTemplate
	t1, err := template.New("index").Parse(text)
	if err != nil {
		log.Fatalln("Can't read the template:", err)
	}

	err = t1.ExecuteTemplate(os.Stdout, "index", name)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}

	//можно не задавать имя шаблона и вызывть его
	//с помощью Execute, результат будет аналогичным
	t2, err := template.New("").Parse(text)
	if err != nil {
		log.Fatalln("Can't read the template:", err)
	}
	err = t2.Execute(os.Stdout, name)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}

}
