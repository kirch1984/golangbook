package main

import (
	"html/template"
	"log"
	"os"
)

func main() {
	//Парсим все файлы в папке templates/
	t, err := template.ParseGlob("templates/*")
	if err != nil {
		log.Fatalln("Can't read the templates files.\n", err)
	}
	//Выводим one.txt
	err = t.ExecuteTemplate(os.Stdout, "one.txt", nil)
	if err != nil {
		log.Fatalln("Can't execute the template one.txt.\n", err)
	}
	//Выводим two.txt
	err = t.ExecuteTemplate(os.Stdout, "two.txt", nil)
	if err != nil {
		log.Fatalln("Can't execute the template two.txt.\n", err)
	}
	//Выводим three.txt
	err = t.ExecuteTemplate(os.Stdout, "three.txt", nil)
	if err != nil {
		log.Fatalln("Can't execute the template three.txt.\n", err)
	}
}
