package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	//Данную переменную name мы передадим в шаблон
	name := "Mark"
	//Парсим темплэйт index.gohtml
	t := template.Must(template.ParseFiles("index.gohtml"))
	//Выводим шаблон index.gohtml и передаём в него значение переменной name
	err := t.Execute(os.Stdout, name)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}

}
