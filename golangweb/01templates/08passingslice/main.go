package main

import (
	"log"
	"os"
	"text/template"
)

func main() {

	weekdays := []string{"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"}

	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, weekdays)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
