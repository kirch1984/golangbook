package main

import (
	"log"
	"os"
	"text/template"
)

func main() {

	capital := map[string]string{
		"Japan": "Tokyo",
		"Korea": "Seoul",
		"China": "Beijing",
	}

	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, capital)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
