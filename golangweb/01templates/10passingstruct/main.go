package main

import (
	"log"
	"os"
	"text/template"
)

type Address struct {
	Line       string
	State      string
	City       string
	PostalCode string
}

type Person struct {
	FirstName string
	LastName  string
	BirthDate string
	Gender    string
	Address
	DevStack []string
}

func main() {

	p1 := Person{
		FirstName: "Tony",
		LastName:  "Hawk",
		BirthDate: "12.12.2001",
		Gender:    "male",
		Address: Address{
			Line:       "534 Erewhon St",
			State:      "Vic",
			City:       "PleasantVille",
			PostalCode: "000000",
		},
		DevStack: []string{"Docker", "MySql", "Redis", "MongoDB", "HTML", "CSS", "BootStrap", "JS", "Vue", "Golang"},
	}

	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, p1)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
