package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	name := "Mark"
	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, name)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
