package main

import (
	"log"
	"os"
	"text/template"
)

type User struct {
	Login string
	Admin bool
}

func main() {
	u1 := User{"hawk@mail.com", true}
	u2 := User{"blade@mail.com", false}
	u3 := User{"stark@mail.com", false}
	us := []User{u1, u2, u3}

	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, us)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
