package main

import (
	"log"
	"os"
	"sort"
	"strings"
	"text/template"
)

//данная функция возвращает отсортированный слайс интов
func si(i []int) []int {
	sort.Ints(i)
	return i
}

func main() {
	//инициализируем переменную типа FuncMap,
	//которую в дальнейшем передадим в функцию Funcs
	fs := template.FuncMap{
		"si": si,              //возвращает отсортированный слайс интов
		"su": strings.ToUpper, //возвращает строку в верхнем регистре
	}
	nums := []int{1, 9, 4, 3, 7, 8, 0, 2}
	text := "some text bla bla bla bla"
	//данную переменную передадим в шаблон
	data := struct {
		Nums []int
		Text string
	}{
		nums,
		text,
	}
	//при использовании Funcs необходимо придерживаться
	//такой последовательности - Funcs до парсинга файла.
	//Если Funcs вызвать уже после парсинга, то получится,
	//что шаблон будет иметь функции про которые ничего не знает
	t := template.Must(template.New("").Funcs(fs).ParseFiles("index.gohtml"))

	err := t.ExecuteTemplate(os.Stdout, "index.gohtml", data)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
