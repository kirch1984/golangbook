package main

import (
	"log"
	"os"
	"text/template"
)

type rectangle struct {
	A int
	B int
}

func (r rectangle) Perimeter() int {
	return (r.A + r.B) * 2
}
func (r rectangle) Area() int {
	return r.A * r.B
}
func main() {
	r := rectangle{5, 10}

	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, r)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
