package main

import (
	"log"
	"os"
	"text/template"
)

func main() {

	text := `
	{{define "one"}}
	ONE
	{{end}}
	{{define "two"}}
	TWO
	{{end}}
	{{define "three"}}
	THREE
	{{end}}
	call all templates
	{{template "one"}}
	{{template "two"}}
	{{template "three"}}
`

	t := template.Must(template.New("index").Parse(text))
	//вызываем только шаблон one
	err := t.ExecuteTemplate(os.Stdout, "one", nil)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
	//вызываем только шаблон two
	err = t.ExecuteTemplate(os.Stdout, "two", nil)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
	//вызываем только шаблон three
	err = t.ExecuteTemplate(os.Stdout, "three", nil)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
	//вызываем шаблон index
	err = t.ExecuteTemplate(os.Stdout, "index", nil)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
