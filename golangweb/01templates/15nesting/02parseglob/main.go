package main

import (
	"html/template"
	"log"
	"os"
)

func main() {
	name := "Mark"
	//Парсим все файлы в папке templates/
	t, err := template.ParseGlob("templates/*")
	if err != nil {
		log.Fatalln("Can't read the templates files.\n", err)
	}
	//выводим только шаблон body и передаёи
	//в него занчение переменной name
	err = t.ExecuteTemplate(os.Stdout, "body", name)
	if err != nil {
		log.Fatalln("Can't execute the template body.\n", err)
	}
	//выводим только шаблон footer
	err = t.ExecuteTemplate(os.Stdout, "footer", nil)
	if err != nil {
		log.Fatalln("Can't execute the template footer.\n", err)
	}

	//выводим шаблон index.gohtml,который вкючает в себя
	//шаблоны body и footer, передаёи
	//в него занчение переменной name
	err = t.ExecuteTemplate(os.Stdout, "index.gohtml", name)
	if err != nil {
		log.Fatalln("Can't execute the template index.gohtml.\n", err)
	}

}
