> * [Intruduction](#intruduction)
> * [Go templates](#gotemplates)
>   * [ParseFiles and Execute](#parsefilesexecute)
>   * [ParseFiles and ExecuteTemplate](#parsefilesexecutetemplate)
>   * [ParseGlobe and ExecuteTemplate](#parseglobeexecutetemplate)
>   * [Parse and Execute/ExecuteTemplate](#parseexecute)
>   * [Must](#must)
>   * [Passing data into template](#passingdata)
>     * [Variables in template](#variables)
>     * [Passing slice into template](#passingslice)
>     * [Passing map into template](#passingmap)
>     * [Passing struct into template](#passingstruct)
>   * [Functions in templates](#funcsintemp)
>     * [Global functions](#globalfuncs)
>     * [Passing functions into template](#passingfuncs)
>   * [Methods in templates](#methodsintpl)
>   * [Nesting templates](#nestingtpl)


<a name="intruduction" id="intruduction"></a>
# Intruduction
Шаблоны позволяют создавать документы и затем объеденять их с данными. Таким образом позволяя персонализировать результат для каждого пользователя или группы пользователей.
Простейший пример использования шаблона - в корпоративной рассылки, когда делается рассылка по всей компании о каком нибудь мероприятии, где содержмое письма одинаково для всех за исключением имени получателя указанное в самом начале письма.

В Go cамый примитивный пример создания шаблона - это с помощью конкатенации.
```golang
package main

import "fmt"

func main() {
	text := "Hello, World!"
	template := `
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
	<h1>` + text + `</h1>
</body>
</html>
	`
	fmt.Println(template)
}
``` 
если сейчас выполнить команду
```
go run main.go  > index.html
```
то в итоге мы получем полноценную html страницу, где в качестве шаблона выступали данные переменной <code>template</code> и в шаблон подставляются данные из переменной <code>text</code>.\
Данный пример подводит нас к тому, что былоб не плохо содержимое <code>template</code> вынести в отдельный файл. Что позволит шаблону быть многоиспользуемым, а функция main станет выгядить более аккуратно.

<a name="gotemplates" id="gotemplates"></a>
# GO templates
В Go имеется два пакета в стандартной библиотеке для работы с шаблонами\
[text/template](https://godoc.org/text/template)\
[html/template](https://godoc.org/html/template)

<code>html/template</code> повторяет почти весь функционал <code>text/template</code>, плюс при генерации html страниц экранирует передаваемые в него данные,  что позволяет избегать атаки основанные на внедрении кода (XSS). Надо отметить, что пакет <code>text/template</code> содержит больше документации, поэтому если не находятся ответы в документации <code>html/template</code>, то можно поискать их в <code>text/template</code>.\
Знакомство с шаблонами в Go начнём с <code>text/template</code>.\
В общем суть работы с шаблонами сводится к двум шагам:
1. распарсить файл-шаблон и сохранить его содержимое в программе для дальнейшего использования
2. "выполнить" распарсенный шаблон (при необходимости передав в него данные)

Для выполнения первого шага в пакетах содержатся фун-ии [ParseFiles](https://godoc.org/text/template#ParseFiles) и [ParseGlob](https://godoc.org/text/template#ParseGlob).\
Для второго шага используются методы [Execute](https://godoc.org/text/template#Template.Execute) и [ExecuteTemplate](https://godoc.org/text/template#Template.ExecuteTemplate)
<a name="parsefilesexecute" id="parsefilesexecute"></a>
## ParseFiles and Execute
Рассмотрим пример использования <code>ParseFiles</code> и <code>Execute</code>.\
 <code>ParseFiles</code> - это вариадотивная фун-ия, которая позволяет принимать сразу несколько аргументов в качестве которых выступают имена файлов и возвращает указатель на объект типа  <code>Template</code>\
 <code>Execute</code> - это метод принимающий два аргумента - приёмник шаблона и передаваемые в него данные. Где приёмником может быть стандартный интерфейс вывода, файл, интерфейс ResponseWriter ипользуемы для http ответа и т.д.\
В примере ниже распарсим файл <code>index.gohtml</code> и выведим его на экран не передав никаких данных. Расширение <code>.gohtml</code> не является обязательным требованием к файлам-шаблонам в Go.

```golang
package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
    //распарсиваем один файл index.gohtml
    //вообще о переменной t можно думать, как о некотором
    //контейнере(абстрактно конечно), куда мы помещаем файлы и в последтвии
    //вызываем по мере необходимости методами Execute или
    //ExecuteTemplate
    t, err := template.ParseFiles("index.gohtml")
    if err != nil {
        log.Fatalln("Can't read the template:", err)
    }
    //выполняем первый распарсенный файл-шаблон
    //никаких данных в него не передаём
    err = t.Execute(os.Stdout, nil)
    if err != nil {
        log.Fatalln("Can't execute the template:", err)
    }

}
```
<code>index.gohtml</code> имеет следующее содержимое
```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
	<h1>Hello,World!</h1>
</body>
</html>
```
<a name="parsefilesexecutetemplate" id="parsefilesexecutetemplate"></a>
## ParseFiles and ExecuteTemplate
В качестве следующего примера распарсим несколько файлов с помощью <code>ParseFiles</code> и результат выведим с помощью методов <code>Execute и ExecuteTemplate</code>.
Парсить будем файлы <code>one.txt,two.txt,three.txt</code> содержимое которых соответствует имени файла.
```golang
package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	//Парсим one.txt
	t, err := template.ParseFiles("one.txt")
	if err != nil {
		log.Fatalln("Can't read one.txt file.", err)
	}

	//Выводим сеодержимое one.txt
	err = t.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatalln("Can't Execute one.txt file.", err)
	}

	//Парсим сразу два файла "two.txt","three.txt"
	//т.е. сейчас переменная t содержит данные
	//трёх файлов-шаблонов
	t, err = t.ParseFiles("two.txt", "three.txt")
	if err != nil {
		log.Fatalln("Can't read files:", err)
	}
	//Теперь чтоб вывести один из нужных нам темплэйтов
	//необходимо воспользоваться методом ExecuteTemplate
	//Выводим сеодержимое two.txt
	err = t.ExecuteTemplate(os.Stdout, "two.txt", nil)
	if err != nil {
		log.Fatalln("Can't Execute two.txt file.", err)
	}
	//Выводим содержимое three.txt
	err = t.ExecuteTemplate(os.Stdout, "three.txt", nil)
	if err != nil {
		log.Fatalln("Can't Execute three.txt file.", err)
	}
	//Выводим сеодержимое one.txt
	err = t.ExecuteTemplate(os.Stdout, "one.txt", nil)
	if err != nil {
		log.Fatalln("Can't Execute one.txt file.", err)
	}
	//Если теперь воспользоваться методом Execute
	//то в результате нам будет выведен первый
	//распарсенный файл - one.txt
	err = t.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatalln("Can't Execute one.txt file.", err)
	}
}
```
Запускаем
```
go run main.go 
one
two
three
one
one
```
<a name="parseglobeexecutetemplate" id="parseglobeexecutetemplate"></a>
## ParseGlob
Функция <code>ParseGlob</code> позволяет парсить файлы по паттерну имени файлов. Т.е. можно указать например только расширение <code>*.txt</code> файл-шаблонов или указать, что нужно парсить все файлы в папке, как в примере ниже. 
```golang
package main

import (
	"html/template"
	"log"
	"os"
)

func main() {
	//Парсим все файлы в папке templates/
	t, err := template.ParseGlob("templates/*")
	if err != nil {
		log.Fatalln("Can't read the templates files.\n", err)
	}
	//Выводим one.txt
	err = t.ExecuteTemplate(os.Stdout, "one.txt", nil)
	if err != nil {
		log.Fatalln("Can't execute the template one.txt.\n", err)
	}
	//Выводим two.txt
	err = t.ExecuteTemplate(os.Stdout, "two.txt", nil)
	if err != nil {
		log.Fatalln("Can't execute the template two.txt.\n", err)
	}
	//Выводим three.txt
	err = t.ExecuteTemplate(os.Stdout, "three.txt", nil)
	if err != nil {
		log.Fatalln("Can't execute the template three.txt.\n", err)
	}
}

```
Запускаем
```
go run main.go 
one
two
three
```

<a name="parseexecute" id="parseexecute"></a>
## Parse and Execute/ExecuteTemplate
<code>ParseFiles</code> и <code>ParseGlob</code> парсят файлы. Но помимо этих функций есть еще метод [Parse](https://godoc.org/text/template#Template.Parse), который позволяет парсить строку. Использовать данный метод следует совместно с функцией [New](https://godoc.org/text/template#New), которая возвращает объект типа <code>Template</code>.
```golang
package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	//будем парсить данную переменную(строку)
	text := `
	<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
	<h1>Hello, World!</h1>
</body>
</html>
`
	//парсим строку и назначаем данному шаблону имя index
	//чтоб позже вызвать его с помощью ExecuteTemplate
	t1, err := template.New("index").Parse(text)
	if err != nil {
		log.Fatalln("Can't read the template:", err)
	}

	err = t1.ExecuteTemplate(os.Stdout, "index", nil)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}

	//можно не задавать имя шаблона и вызывть его
	//с помощью Execute, результат будет аналогичным
	t2, err := template.New("").Parse(text)
	if err != nil {
		log.Fatalln("Can't read the template:", err)
	}
	err = t2.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}

}

```
Результат
```html
<h1>Hello,World!</h1>

<h1>Hello,World!</h1>
```

<a name="must" id="must"></a>
## Must
Пакет <code>text/template</code> содержит функцию [Must](https://godoc.org/text/template#Must), которая немного облегчает нам жизнь. Она используется во время парсинга файлов и сама паникует в случае возникновения проблем.\
Перепишем предыдущий пример с использованием Must функции.
```golang
package main

import (
	"html/template"
	"log"
	"os"
)

func main() {
	//Парсим все файлы в папке templates/
	t := template.Must(template.ParseGlob("templates/*"))

	//Выводим one.txt
	err := t.ExecuteTemplate(os.Stdout, "one.txt", nil)
	if err != nil {
		log.Fatalln("Can't execute the template one.txt.\n", err)
	}
	//Выводим two.txt
	err = t.ExecuteTemplate(os.Stdout, "two.txt", nil)
	if err != nil {
		log.Fatalln("Can't execute the template two.txt.\n", err)
	}
	//Выводим three.txt
	err = t.ExecuteTemplate(os.Stdout, "three.txt", nil)
	if err != nil {
		log.Fatalln("Can't execute the template three.txt.\n", err)
	}
}
```

<a name="passingdata" id="passingdata"></a>
## Passing data into template
В Go в файл-шаблон можно передать только одну переменную любого поддерживаемого языком типа - <code>int,bool,float,string,slice,map,struct, interface{}...</code> И конечно, как правило, для обеспечения гибкости веб приложения используется <code>struct</code> или <code>interface{}</code>.\
Для того чтобы обратиться в шаблоне к переданному в него аргументу используется конструкция <code>{{.}}</code>.\
Воспользуемся одним из первых примеров, только теперь в шаблон передадим аргумент.

```golang
package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	//Данную переменную name мы передадим в шаблон
	name := "Mark"
	//Парсим темплэйт index.gohtml
	t := template.Must(template.ParseFiles("index.gohtml"))
	//Выводим шаблон index.gohtml и передаём в него значение переменной name
	err := t.Execute(os.Stdout, name)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}

}
```
<code>index.gohtml</code> имеет следующее содержимое
```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
	<h1>Hello,{{.}}!</h1>
</body>
</html>
```
Результат
```html
<h1>Hello,Mark!</h1>
```

<a name="variables" id="variables"></a>
### Variables in template
В шаблоне можно инициализировать переменные с помощью конструкции <code>{{\$name := value}}</code> и в дальнейшеим использовать <code>{{\$name}}</code>\
Перепишем предыдущий пример с использованием переменных в шаблоне.
```golang
package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	//Данную переменную name мы передадим в шаблон
	name := "Mark"
	//Парсим темплэйт index.gohtml
	t := template.Must(template.ParseFiles("index.gohtml"))
	//Выводим шаблон index.gohtml и передаём в него значение переменной name
	err := t.Execute(os.Stdout, name)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}

}
```
Инициализируем в шаблоне переменные <code>{{\$name := .}}</code> и <code>{{\$age := 21}}</code>
```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
	{{$name := .}}
	{{$age := 21}}
	<p>Hello,{{$name}}!</p>
	<p>You are {{$age}} years old.</p>
</body>
</html>
```
Теперь в качестве результата мы получим
```html
<p>Hello,Mark!</p>
<p>You are 21 years old.</p>
```
<a name="passingslice" id="passingslice"></a>
### Passing slice
Если в шаблон передать составной тип данных например как slice, то проитерироваться по нему можно с помощью конструкций\
<code>
{{range .}}
{{.}}
{{end}}
</code>
и
<code>
{{range \$i,\$v := .}}
{{\$i}},{{\$v}}
{{end}}
</code>, а обратиться к отдельному элементу слайса при помощи конструкции <code>{{index . 0}}</code>\
В примере ниже передадим слайс строк содержащий дни недели, а в шаблоне попробуем взаимодесйтовать с слайсом и его элементами разными способами.
```golang
package main

import (
	"log"
	"os"
	"text/template"
)

func main() {

	weekdays := []string{"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"}

	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, weekdays)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
```

```html
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>

<body>
	<!-- весь слайс, как есть-->
	<p>{{.}}</p>
	<!-- отдельный элемент слайса-->
	<p>{{index . 0}}</p>
	<!-- итерируемся по значениям слайса-->
	<ul>
		{{range .}}
		<li>{{.}}</li>
		{{end}}
	</ul>
	<!-- итерируемся по индексам и значениям слайса-->
	<ul>
		{{range $i,$v := .}}
		<li>{{$i}}-{{$v}}</li>
		{{end}}
	</ul>
</body>

</html>
```
В итоге мы получим результат
```html
<!-- весь слайс, как есть -->
<p>[SUN MON TUE WED THU FRI SAT]</p>
<!-- отдельный элемент слайса -->
<p>SUN</p>
<!-- итерируемся по значениям слайса -->
<ul>	
	<li>SUN</li>	
	<li>MON</li>	
	<li>TUE</li>	
	<li>WED</li>	
	<li>THU</li>	
	<li>FRI</li>	
	<li>SAT</li>	
</ul>
<!-- итерируемся по индексам и значениям слайса -->
<ul>	
	<li>0-SUN</li>	
	<li>1-MON</li>	
	<li>2-TUE</li>	
	<li>3-WED</li>	
	<li>4-THU</li>	
	<li>5-FRI</li>	
	<li>6-SAT</li>	
</ul>
```
<a name="passingmap" id="passingmap"></a>
### Passing map
Рассмотри пример передачи map в шаблон. Передаваемый map будет содержать столицы стран.\
В отличае от слайса, map позволяет обратиться к отдельному элементу по ключу <code>{{.Key}}</code>, а в остальном всё тоже самое что и для слайса.
Надо отметить такой момент, что порядок значений в map не сохраняется при итерировании, а в слайсе сохраняется.
```golang
package main

import (
	"log"
	"os"
	"text/template"
)

func main() {

	capital := map[string]string{
		"Japan": "Tokyo",
		"Korea": "Seoul",
		"China": "Beijing",
	}

	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, capital)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
```
```html
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>

<body>
	<!-- весь map, как есть-->
	<p>{{.}}</p>
	<!-- отдельный элемент map-->
	<p>{{index . "Japan"}}</p>
	<!-- отдельный элемент map-->
	<p>{{.China}}</p>
	<!-- итерируемся по значениям map-->
	<ul>
		{{range .}}
		<li>{{.}}</li>
		{{end}}
	</ul>
	<!-- итерируемся по индексам и значениям map-->
	<ul>
		{{range $k,$v := .}}
		<li>{{$k}}-{{$v}}</li>
		{{end}}
	</ul>
</body>

</html>
```
Результат соответствующий
```html
<!-- весь map, как есть-->
<p>map[Japan:Tokyo Korea:Seoul China:Beijing]</p>
<!-- отдельный элемент map-->
<p>Tokyo</p>
<!-- отдельный элемент map-->
<p>Beijing</p>
<!-- итерируемся по значениям map-->
<ul>	
	<li>Beijing</li>
	<li>Tokyo</li>	
	<li>Seoul</li>	
</ul>
<!-- итерируемся по индексам и значениям map-->
<ul>	
	<li>China-Beijing</li>	
	<li>Japan-Tokyo</li>	
	<li>Korea-Seoul</li>	
</ul>
```

<a name="passingstruct" id="passingstruct"></a>
### Passing struct
Самый популярный тип данных передаваемый в шаблон - это <code>struct</code>. Он позволят объеденить различные типы данных и передать их в шаблон, как единое целое. И уже в шаблоне либо итерироваться по отдельным его полям содержащим составные данные slice,map, либо обращаться к отдельному полю по его имени.
```golang
package main

import (
	"log"
	"os"
	"text/template"
)

type Address struct {
	Line       string
	State      string
	City       string
	PostalCode string
}

type Person struct {
	FirstName string
	LastName  string
	BirthDate string
	Gender    string
	Address
	DevStack []string
}

func main() {

	p1 := Person{
		FirstName: "Tony",
		LastName:  "Hawk",
		BirthDate: "12.12.2001",
		Gender:    "male",
		Address: Address{
			Line:       "534 Erewhon St",
			State:      "Vic",
			City:       "PleasantVille",
			PostalCode: "000000",
		},
		DevStack: []string{"Docker","MySql", "Redis", "MongoDB", "HTML", "CSS", "BootStrap", "JS", "Vue", "Golang"},
	}

	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, p1)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}

```
```html
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>

<body>
	<!-- весь struct, как есть-->
	<p>{{.}}</p>
	
	<!-- значение отдельного поля струкуры-->
	<p>{{.FirstName}}</p>
	<p>{{.LastName}}</p>

	<p>{{.Address}}</p>
	<p>{{.Address.City}}</p>
	<p>{{.State}}</p>

	<!--итерируемся по слайсу-->
	{{range .DevStack}}
	<p>{{.}}</p>
	{{end}}

</body>

</html>
```
Результат
```html
<!-- весь struct, как есть-->
<p>{Tony Hawk 12.12.2001 male {534 Erewhon St Vic PleasantVille 000000} [Docker MySql Redis MongoDB HTML CSS BootStrap JS Vue Golang]}</p>

<!-- значение отдельного поля струкуры-->
<p>Tony</p>
<p>Hawk</p>

<p>{534 Erewhon St Vic PleasantVille 000000}</p>
<p>PleasantVille</p>
<p>Vic</p>

<!--итерируемся по слайсу-->
<p>Docker</p>
<p>MySql</p>
<p>Redis</p>
<p>MongoDB</p>
<p>HTML</p>
<p>CSS</p>
<p>BootStrap</p>
<p>JS</p>
<p>Vue</p>
<p>Golang</p>
```
<a name="funcsintemp" id="funcsintemp"></a>
## Functions in templates
Функции в шаблонах бывают двух типов:
1. Глобальные функции - это предопределенные функции, т.е. определены заранее. Их можно использовать в шаблонах без каких либо предварительных инициализаций. Список таких функций можно найти [здесь](https://godoc.org/text/template#hdr-Functions).
2. Второй тип функций - это функции, которые создаются в основном теле программы и затем передаются в шаблон.
<a name="globalfuncs" id="globalfuncs"></a>
### Global functions
Пример для глобальной функции <code>len</code>
```golang
package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	name := "Mark"
	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, name)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
```
```html
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>

<body>
	<p>{{.}}</p>
	<p>{{len .}}</p>

	{{$var := "12345"}}
	<p>{{$var}}</p>
	<p>{{len $var}}</p>

</body>

</html>
```
Результат
```html
<p>Mark</p>
<p>4</p>

<p>12345</p>
<p>5</p>
```
В шаблонах возможно использование условного оператора <code>{{if}} //todo {{end}}</code>. Более подробно про синтаксис  можно найти [здесь](https://godoc.org/text/template#hdr-Actions)\
Пример для глобальной функции <code>and</code>
```golang
package main

import (
	"log"
	"os"
	"text/template"
)

type User struct {
	Login string
	Admin bool
}

func main() {
	u1 := User{"hawk@mail.com", true}
	u2 := User{"blade@mail.com", false}
	u3 := User{"stark@mail.com", false}
	us := []User{u1, u2, u3}

	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, us)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
```
```html
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>

<body>

	{{range .}}
		{{if and .Admin .Login}}
		<h1>Admin is {{.Login}}</h1>
		{{else}}
		<h1>User is {{.Login}}</h1>
		{{end}}
	{{end}}

</body>

</html>
```
Результат
```html
<h1>Admin is hawk@mail.com</h1>
<h1>User is blade@mail.com</h1>
<h1>User is stark@mail.com</h1>
```

<a name="passingfuncs" id="passingfuncs"></a>
### Passing functions into template
Для того чтобы передать в шаблон функцию используется метод [Funcs](https://godoc.org/text/template#Template.Funcs). При этом метод принимает в качестве аргумента переменную типа [FuncMap](https://godoc.org/text/template#FuncMap), которая является <code>map[string]interface{}</code> типом.\
Использование фцнкций в шаблоне сводится к следующим шагам
1. Создаём, если требуется, функцию которую планируем использовать в шаблоне
2. Определеяем переменную типа <code>template.FuncMap</code>, которая является по сути мэпом, где в качестве ключа указывается алиас для функции по которому будет вызываться функция в шаблоне, а в качестве аргумента ключа указывается либо имя уже созданной функции, либо передаётся анонимная функция.
3. Далее передаём переменную типа template.FuncMap в метод Funcs на шаге инициализации шаблонов
4. В самом шаблоне функция вызывается конструкцией <code>{{FUNCALIAS .}}</code> или <code>{{. | FUNCALIAS}}</code>
```golang
package main

import (
	"log"
	"os"
	"sort"
	"strings"
	"text/template"
)

//данная функция возвращает отсортированный слайс интов
func si(i []int) []int {
	sort.Ints(i)
	return i
}

func main() {
	//инициализируем переменную типа FuncMap,
	//которую в дальнейшем передадим в функцию Funcs
	fs := template.FuncMap{
		"si": si,              //возвращает отсортированный слайс интов
		"su": strings.ToUpper, //возвращает строку в верхнем регистре
	}
	nums := []int{1, 9, 4, 3, 7, 8, 0, 2}
	text := "some text bla bla bla bla"
	//данную переменную передадим в шаблон
	data := struct {
		Nums []int
		Text string
	}{
		nums,
		text,
	}
	//при использовании Funcs необходимо придерживаться
	//такой последовательности - Funcs до парсинга файла.
	//Если Funcs вызвать уже после парсинга, то получится,
	//что шаблон будет иметь функции про которые ничего не знает
	t := template.Must(template.New("").Funcs(fs).ParseFiles("index.gohtml"))

	err := t.ExecuteTemplate(os.Stdout, "index.gohtml", data)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
```
```html
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>

<body>
	<p>{{.}}</p>
	<p>{{si .Nums}}</p>
	<p>{{.Text|su}}</p>

</body>

</html>
```
Результат
```html
<p>{[1 9 4 3 7 8 0 2] some text bla bla bla bla}</p>
<p>[0 1 2 3 4 7 8 9]</p>
<p>SOME TEXT BLA BLA BLA BLA</p>
```

<a name="methodsintpl" id="methodsintpl"></a>
## Methods in templates
Помимо функций в шаблоны можно передавать методы. Никаких особых телодвижений для этого не требуется. Просто инициализируем метод для типа структуры и в шаблоне к нему обращаемся, как к полю <code>{{.MethodName}}</code>. Единствнное на что следует обратить внимание, что в качестве получателя методу указвается не указетель на тип, а просто тип.\
В примере определим структуру - прямоугольник. И для данной структуры создадим два метода подсчета периметра и площади. В шаблоне обратимся к этим методам.
```golang
package main

import (
	"log"
	"os"
	"text/template"
)

type rectangle struct {
	A int
	B int
}

func (r rectangle) Perimeter() int {
	return (r.A + r.B) * 2
}
func (r rectangle) Area() int {
	return r.A * r.B
}
func main() {
	r := rectangle{5, 10}

	t := template.Must(template.ParseFiles("index.gohtml"))

	err := t.Execute(os.Stdout, r)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
```
```html
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>

<body>
	<p>A:{{.A}}</p>
	<p>B:{{.B}}</p>
	<p>Perimeter:{{.Perimeter}}</p>
	<p>Area:{{.Area}}</p>

</body>

</html>
```
Результат
```html
<p>A:5</p>
<p>B:10</p>
<p>Perimeter:30</p>
<p>Area:50</p>
```
<a name="nestingtpl" id="nestingtpl"></a>
## Nesting templates
Шаблоны могут быть встраемыми один в другой, что позволяет разбить их на более мелкие составляющие для более гибкого управления приложением.\
Делается это с помощью конструкции:
```
//определяем шаблон
{{define "TEMPLATENAME"}}
//SOME TEMPLATE TEXT/HTML CODE
{{end}}

//используем шаблон
{{template "TEMPLATENAME"}}
```
Передать данные в такой шаблон можно так
```
{{template "TEMPLATENAME" .}}
```
Сперва рассмотрим пример с использоавнием <code>Parse</code>.
```golang
package main

import (
	"log"
	"os"
	"text/template"
)

func main() {

	text := `
	{{define "one"}}
	ONE
	{{end}}
	{{define "two"}}
	TWO
	{{end}}
	{{define "three"}}
	THREE
	{{end}}
	call all templates
	{{template "one"}}
	{{template "two"}}
	{{template "three"}}
`

	t := template.Must(template.New("index").Parse(text))
	//вызываем только шаблон one
	err := t.ExecuteTemplate(os.Stdout, "one", nil)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
	//вызываем только шаблон two
	err = t.ExecuteTemplate(os.Stdout, "two", nil)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
	//вызываем только шаблон three
	err = t.ExecuteTemplate(os.Stdout, "three", nil)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
	//вызываем шаблон index
	err = t.ExecuteTemplate(os.Stdout, "index", nil)
	if err != nil {
		log.Fatalln("Can't execute the template:", err)
	}
}
```
Результат
```
ONE
TWO
THREE

call all templates
ONE
TWO
THREE
```
Использование встраиваемых шаблонов в файлах аналогично.\
Определяем шаблон footer
```html
{{define "footer"}}
<h4>Copyright 2018</h4>
{{end}}
```
Определяем шаблон body
```html
{{define "body"}}
<h1>Hello,{{.}}</h1>
{{end}}
```
Определяем шаблон index.gohtml
```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
    {{template "body" .}}
    {{template "footer"}}
</body>
</html>
```
```golang
package main

import (
	"html/template"
	"log"
	"os"
)

func main() {
	name := "Mark"
	//Парсим все файлы в папке templates/
	t, err := template.ParseGlob("templates/*")
	if err != nil {
		log.Fatalln("Can't read the templates files.\n", err)
	}
	//выводим только шаблон body и передаёи
	//в него занчение переменной name
	err = t.ExecuteTemplate(os.Stdout, "body", name)
	if err != nil {
		log.Fatalln("Can't execute the template body.\n", err)
	}
	//выводим только шаблон footer
	err = t.ExecuteTemplate(os.Stdout, "footer", nil)
	if err != nil {
		log.Fatalln("Can't execute the template footer.\n", err)
	}

	//выводим шаблон index.gohtml,который вкючает в себя
	//шаблоны body и footer, передаёи
	//в него занчение переменной name
	err = t.ExecuteTemplate(os.Stdout, "index.gohtml", name)
	if err != nil {
		log.Fatalln("Can't execute the template index.gohtml.\n", err)
	}

}
```
Результат
```html
<h1>Hello,Mark</h1>

<h4>Copyright 2018</h4>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
    
<h1>Hello,Mark</h1>  
<h4>Copyright 2018</h4>

</body>
```