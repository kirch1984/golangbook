package main

import (
	"fmt"
	"log"
	"net/http"
)

//создадим новый тип myHTTP
type myHTTP struct{}

//создадим метод ServeHTTP
//с сигнатурой http.ResponseWriter, *http.Request
//для типа myHTTP
//т.е. теперь любой объект типа myHTTP может обрабатывать
//http запросы и отвечать на них
func (m myHTTP) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello,World!")
}

func main() {
	var m myHTTP
	//запускаем сервер на 127.0.0.1:8080
	err := http.ListenAndServe("127.0.0.1:8080", m)
	if err != nil {
		log.Fatalln(err)
	}
}
