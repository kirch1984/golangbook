<a name="introduction" id="introduction"></a>
# Introduction
[net/http package](#https://godoc.org/net/http)

Самым ключевым компонентом <code>net/http</code> пакета является <code>Handler</code>(Обработчик), он отвечает за создание тела ответа и его заголовок.\
Handler представляет собой интерфейс
```golang
type Handler interface{
	ServeHTTP(http.ResponseWriter, *http.Request)
}
```
т.е. любой объект удовлетворяющий реализации данного интерфейса (имеющий метод ServeHTTP отвечающий сигнатуре (http.ResponseWriter, *http.Request)) может являться обработчиком HTTP запросов.

HTTP сервер можно создать вызвав функцию <code>ListenAndServe(addr string, handler Handler)</code>. Где в качестве первого параметра принимается ip адрес и порт, а в качестве второго параметра принимается объект типа <code>Handler</code>.\
Если не указывать первый параметр, то по умолчанию сервер поднимется на всех интерфесах на 80 порту.
```golang
package main

import (
	"fmt"
	"log"
	"net/http"
)

//создадим новый тип myHTTP
type myHTTP struct{}

//создадим метод ServeHTTP
//с сигнатурой http.ResponseWriter, *http.Request
//для типа myHTTP
//т.е. теперь любой объект типа myHTTP может обрабатывать
//http запросы и отвечать на них
func (m myHTTP) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello,World!")
}

func main() {
	var m myHTTP
	//запускаем сервер на 127.0.0.1:8080
	err := http.ListenAndServe("127.0.0.1:8080", m)
	if err != nil {
		log.Fatalln(err)
	}
}
```
Проверяем
```
curl  http://127.0.0.1:8080
Hello,World!
```



Второй важный компонет - <code>ServeMux</code> - это мультиплексор(маршрутизатор HTTP запросов), он сравнивает входящие HTTP запросы с его списком URI ресурсов и вызывает соответствующий <code>Handler</code>(Обработчик) для ресурса запрошенного клиентом.