<a id="contnent" name="contnent"></a>

- [Introduction](#introduction)
	- [Types of API in gRPC](#types-of-api-in-grpc)
- [Настройка WorkFlow](#Настройка-workflow)
	- [Расширения для VSC](#Расширения-для-vsc)
	- [Setup Protoc Compiler](#setup-protoc-compiler)
	- [Golang Packages](#golang-packages)
- [Go и gRPC](#go-и-grpc)
	- [Unary RPC](#unary-rpc)
	- [Server streaming RPC](#server-streaming-rpc)
	- [Client streaming RPC](#client-streaming-rpc)
	- [Bidirectional streaming RPC](#bidirectional-streaming-rpc)
- [Error](#error)
	- [Go Example](#go-example)
- [Deadlines](#deadlines)
	- [Go Example](#go-example-1)
- [gRPC Reflection & Evans CLI](#grpc-reflection--evans-cli)
- [SSL Security](#ssl-security)



<a name="introduction" id="introduction"></a>

# Introduction

[Up](#contnent)

[HTTP/2](https://http2.github.io/)\
[HTTP2 vs HTTP1.1](https://imagekit.io/demo/http2-vs-http1)\
[gRPC](https://grpc.io/)\
[grpc-go](https://github.com/grpc/grpc-go)\
[Comparing gRPC services with HTTP APIs](https://docs.microsoft.com/en-us/aspnet/core/grpc/comparison?view=aspnetcore-3.0)\
[REST v. gRPC](https://husobee.github.io/golang/rest/grpc/2016/05/28/golang-rest-v-grpc.html)

gRPC клиент может напрямую вызвыать методы на сервере, как будто это локальные объекты. Как и большенство RPC систем, gRPC основан на идеи определения сервиса, методы вызываются удаленно с необходимыми параметрами и возвращают данные. На стороне сервера, сервер реализует интерфейс и запускает gRPC сервер для обработки клиентских запросов. На стороне клиента находится "заглушка"(клиент), которая предосталяет теже методы, что и реализованы на сервере.


<img src="./landing-2.svg">


По умолчанию gRPC использует proto buff.\
Proto Buff в gRPC определеяет: стурктуры данных proto buff(message) и сервисы (service).
* Messages(proto buf структур) (data, Request и Response)
  ```
  syntax="proto3"
  message Greeting{
      string fname = 1;
  }
  message GreetRequest{
      Greeting greet = 1;
  }
  message GreetResponse{
      string result = 1;
  }
  ```
* Services (Service name и RPC endpoints)
  ```
  service GreetService{
      rpc Greet(GreetRequest) returns (GreetResponse) {};
  }
  ```

## Types of API in gRPC

[Up](#contnent)

gRPC позволяет определить четыре типа сервисов:

* Unary RPC - классическсий request/response тип. т.е. клиент отправляет один запрос и получает один ответ
* Server Streaming RPC - клиент отправляет один запрос и в ответ получает поток данных, который необходимо обработать. gRPC гарантирует порядок сообщений в пределах одного RPC вызова
* Client Streaming RPC - клиент отправляет не один запрос, а их последовательность. Сервер возвращает один ответ
* Bi Directional Streaming RPC- обе стороны отправляют и получают мн-во сообщений используя потоки чтения и записи. Потоки работают независимо,порядок сообщений гарантирован.

```
service GreetService{
    //Unary
    rpc Greet(GreetRequest) returns (GreetResponse) {};
    //Server Streaming
    rpc GreetManyTimes(GreetManyTimesRequest) returns (stream GreetManyTimesResponse) {};
    //Client Streaming
    rpc LongGreet(stream LongGreetRequest) returns (LongGreetResponse) {};
    //Bi Directional Streaming
    rpc GreetEveryOne(stream GreetEveryOneRequest) returns (stream GreetEveryOneResponse) {};
}
```

Сначала в файле proto определяются стуктуры и сервисы. Затем с помощью <code>protoc</code> и нужного плагина для языка программирования компилирутеся клиентский и серверный код.

* сервер реализует методы(объявленые сервисы) и запускает сервер для обработки клиентских запросов. gRPC декодирует запросы, выполянет методы и кодирует ответы
* на стороне клиента находится локальный объект, именуемый как "заглушка", который реализует теже самые методы, что и сервисы. Клиент может просто вызывать методы для данного объекта, оборачивая параметры вызова в тип proto структуры (message)


<strong>Метаданные</strong> - это информация о конкретном RPC вызове (например данные аутентификации) представленная в форме списка пар ключ-значение, где ключ - строка, а значение может быть строкой или бинарными данными. Метаданные для gRPC непрозрачны, что позволяет клиенту предоставлять информацию связанную с вызовом на сервер и наоборот.

<strong>Каналы</strong> - обеспечивают соединенис с gRPC сервером на указанынй хост и порт. Клиент може определить аргументы канала для изменения gRPC поведения, например включиьт или выключить сжатие. Каналы имеют состояния - connected и idle.

<a name="workflow" id="workflow"></a>
# Настройка WorkFlow

[Up](#contnent)

<a name="vsc" id="vsc"></a>
## Расширения для VSC
Для работы с protocol buffer в [Visual Studio Code](https://code.visualstudio.com/) необходимо установить два расширения:
> [vscode-proto3](https://marketplace.visualstudio.com/items?itemName=zxh404.vscode-proto3)\
> [Clang-Format](https://marketplace.visualstudio.com/items?itemName=xaver.clang-format)\
Затем установить пакет <code>clang-format</code> для вашей ОС.\
Установка для Ubuntu:
```
aptitude install clang-format-8.0
ln -s /usr/bin/clang-format-8 /usr/bin/clang-format
```
Либо вместо создания ссылка зайти в настройки vsc <code>File->Preferences->Settings</code>, найти ключ <code>clang-format.executable</code> и указать в качестве значения <code>/usr/bin/clang-format-8</code>.

<a name="protoccompiler" id="protoccompiler"></a>
## Setup Protoc Compiler
<code>protoc</code> из созданного файла <code>.proto</code>, в котором содержится нужная нам структура <code>message</code>, генерирует файл под определенный язык программирования.
В этом файле содержится темплэйт,созданный из <code>message</code> в <code>.proto</code>, который затем будет использоваться для создания объектов, а также различные методы для работы.\
Установка для Linux:
```
curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.9.0/protoc-3.9.0-linux-x86_64.zip
unzip protoc-3.9.0-linux-x86_64.zip -d protoc-3.9.0
sudo mv protoc-3.9.0/bin/* /usr/local/bin/
sudo mv protoc-3.9.0/include/* /usr/local/include/
```

<a name="gopackages" id="gopackages"></a>
## Golang Packages
Для работы в Go с protocol buffer нам понадобятся два пакета:
> [protoc-gen-go package](#https://godoc.org/github.com/golang/protobuf/protoc-gen-go/descriptor)\
> [proto package](#https://godoc.org/github.com/golang/protobuf/proto)
```
go get -u github.com/golang/protobuf/protoc-gen-go
go get -u github.com/golang/protobuf/proto
```
Для работы в Go с gRPC нам понадобится пакет:
```
go get -u google.golang.org/grpc
```

# Go и gRPC

[Up](#contnent)

Пример на Go будет отправлять приветственное сообщение ползователю, в качестве запроса сервер будет получать имя и фамилию пользователя.

Реализация примера на Go будет включать в себя следующте шаги:
* Определение proto buff стурктур и серивисов в файл .proto
* Генерация клиентского и серверного кода с помощью protoc и плагина для Go
* Используя Go gRPC API напишем простой клиент и сервер для сервисов

Для начала создадим рабочую папку для нашего проекта (ваш путь будет другим)
```
mkdir -p $GOPATH/src/gitlab.com/kirch1984/greet/{greetpb,server,client}
cd $GOPATH/src/gitlab.com/kirch1984/greet/
```
в папке <code>greetpb</code> будет находиться proto файл и скомпилированный protoc код\
в папках <code>server</code> и <code>client</code> соответственно клиентский и серверный код

## Unary RPC

[Up](#contnent)

В данном разделе будем реализовывать Unary RPC тип для нашего примера.

Первый шаг включает в себя определение сервиса и proto стурктур используемых в качестве запросов и ответов.\
Создадим файл <code>greet.proto</code> в папке <code>greetpb</code> и добавим в него следующий код

```
syntax = "proto3";

package greet;
option go_package="greetpb";

message GreetRequest{
    string first_name =1;
    string last_name=2;
}

message GreetResponse{
    string result =1;
}

service GreeetService{
    //Unary RPC
    rpc Greet(GreetRequest) returns(GreetResponse){};
}
```

На втором шаге компилируем proto файл с помощью <code>protoc</code> и плагина для Go

```
protoc greetpb/greet.proto --go_out=plugins=grpc:.
```
В результате получам файл <code>greetpb/greet.pb.go</code> содержащий необходимые Go структуры и методы для работы с ними

На заключительном шаге пишем Go код для клиента и сервера.\
Начнем с сервера. Нам будет необходимо:
* реализовать интерфейс сервиса
* запустить gRPC сервер для получения запросов от клиента

создаём файл <code>server/server.go</code> и рализуем интерфейс сервиса
```go
package main

import (
	"context"
	"fmt"

	"gitlab.com/kirch1984/greet/greetpb"
)

// Создаём новый тип, который будет реализовывать интерфейс сервиса
type server struct{}

/*
Создаём метод Greet для реализвации интерфейса
Чтоб узнать, какие именно методы необходимы для
реализации интерфеса, можно посмотреть в файт greet.pb.go
и в нём найти GreetServiceServer
*/
func (s *server) Greet(ctx context.Context, r *greetpb.GreetRequest) (*greetpb.GreetResponse, error) {
	firstName := r.GetFirstName()
	lastName := r.GetLastName()
	hellMsg := fmt.Sprintf("Hello, %s %s!\n", firstName, lastName)
	response := greetpb.GreetResponse{
		Result: hellMsg,
	}
	return &response, nil
}

func main() {

}
```
Далее пишем код для запуска gRPC сервера
```go
func main() {
	port := "127.0.0.1:50051"
	log.Printf("Listen at: %s\n", port)
	// определяем какой порт мы будт слушать
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Error listen: %v\n", err)
	}
	// создаём экземпляр grpc сервера
	gs := grpc.NewServer()
	// регистрируем сервис на сервере
	greetpb.RegisterGreeetServiceServer(gs, &server{})
	// вызываем Serve() для принятия входящих соединений на указанном при создании lis порту 
	err = gs.Serve(lis)
	if err != nil {
		log.Fatalf("Error run gRPC Server: %v\n", err)
	}
}
```

Пишем код для клиента.\
Создаём файл <code>client/client.go</code>
```go
package main

import (
	"context"
	"log"

	"gitlab.com/kirch1984/greet/greetpb"

	"google.golang.org/grpc"
)

func main() {
    // создаём grpc канал для общения с сервером используя grpc.Dial()
    // grpc.WithInsecure() - указываем, что не будем использвать какие-либо настройки безопасности
	port := "127.0.0.1:50051"
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	defer conn.Close()
	if err != nil {
		log.Fatalf("Error connect to grpc server: %v\n", err)
	}
	// далее нам необходимо создать заглушку для выполнения RPC зарпосов
	client := greetpb.NewGreeetServiceClient(conn)
	//создаем запрос
	request := &greetpb.GreetRequest{
		FirstName: "Tony",
		LastName:  "Hawk",
	}
	//вызываем метод Greet на стороне клиента
	response, err := client.Greet(context.Background(), request)
	if err != nil {
		log.Fatalf("Error got response: %v\n", err)
	}
	log.Printf("Got gRPC response: %v\n", response.Result)
}
```
Запускаем сервер и клиент
```
go run server/server.go
go run client/client.go 
```
Получаем результат
```
2019/08/15 01:22:33 Got gRPC response: Hello, Tony Hawk!
```
## Server streaming RPC

[Up](#contnent)

В данном разделе будем реализовывать Server streaming RPC тип для нашего примера.\
Реализация будет аналогично состоять из шагов:
* создаем proto файл
* компилируем proto файл с помощью protoc
* пишем код для сервера и клинта


Создаём папку для проекта
```
mkdir -p $GOPATH/src/gitlab.com/kirch1984/greetManyTimes/{greetpb,server,client}
cd $GOPATH/src/gitlab.com/kirch1984/greetManyTimes/
```
Создаём файл <code>greetpb/greet.proto</code>
```
syntax = "proto3";

package greet;
option go_package="greetpb";

message GreetRequest{
    string first_name =1;
    string last_name=2;
}

message GreetResponse{
    string result =1;
}

service GreeetService{
    //Unary RPC
    //rpc Greet(GreetRequest) returns(GreetResponse){};
    //Server streaming RPC
    rpc GreetManyTimes(GreetRequest) returns (stream GreetResponse) {};
}
```
Компилируем
```
protoc greetpb/greet.proto --go_out=plugins=grpc:.
```
В результате получаем файл <code>greetpb/greet.pb.go</code>

Теперь переходим к написаню кода для сервера
* реализуем интерфейс
* пишем код запуска gRPC сервера для обработки клиентских запросов

Создаём файл <code>server/server.go</code> и приступаем к реализации интерфейса
```go
package main

import (
	"fmt"
	"strconv"
	"time"

	"gitlab.com/kirch1984/greetManyTimes/greetpb"
)

type server struct{}

// смотрим в файле greet.pb.go, какие методы нужно создать для реализации GreetService интерфейса
func (*server) GreetManyTimes(r *greetpb.GreetRequest, stream greetpb.GreeetService_GreetManyTimesServer) error {
	firstName := r.GetFirstName()
	lastName := r.GetLastName()
	res := &greetpb.GreetResponse{}
	helloMsg := fmt.Sprintf(" Hello, %s %s!\n", firstName, lastName)
	//т.к. отправлять будем несоклько ответов то реализуем это через цикл
	for i := 0; i < 10; i++ {
		res.Result = strconv.Itoa(i) + helloMsg
		stream.Send(res)
		//в качестве примера добавим небольшую задержку
		time.Sleep(500 * time.Millisecond)
	}
	return nil
}

func main() {}
```
пишем код для запуска gRPC сервера
```go
func main() {
	port := "127.0.0.1:50051"
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Error listen at %v : %v\n", port, err)
	}
	log.Println("Run gRPC server...")
	// создаем gRPC сервер
	s := grpc.NewServer()
	// регистрируем сервис
	greetpb.RegisterGreeetServiceServer(s, &server{})
	// вызываем Serve()
	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("Error run gRPC server: %v\n", err)
	}
}
```

Далее переходим на писанию клиента.\
Создайм файл <code>client/client.go</code>
```go
package main

import (
	"context"
	"io"
	"log"

	"gitlab.com/kirch1984/greetManyTimes/greetpb"
	"google.golang.org/grpc"
)

func main() {
	// создаём канал
	port := "127.0.0.1:50051"
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	defer conn.Close()
	if err != nil {
		log.Fatalf("Error create gRPC channel: %v\n", err)
	}
	// создаём stub
	client := greetpb.NewGreeetServiceClient(conn)
	req := &greetpb.GreetRequest{
		FirstName: "Tony",
		LastName:  "Hawk",
	}
	res, err := client.GreetManyTimes(context.Background(), req)
	if err != nil {
		log.Fatalf("Error send gRPC Request: %v\n", err)
	}
	// реализуем получение потока ответов от сервера
	for {
		msg, err := res.Recv()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatalf("Error get gRPC Response: %v\n", err)
		}
		log.Printf("Got gRPC response: %v", msg.Result)
	}
}
```
Запускаем
```
 go run server/server.go
 go run client/cleint.go
```
Результат
```
2019/08/15 17:59:48 Got gRPC response: 0 Hello, Tony Hawk!
2019/08/15 17:59:48 Got gRPC response: 1 Hello, Tony Hawk!
2019/08/15 17:59:49 Got gRPC response: 2 Hello, Tony Hawk!
2019/08/15 17:59:49 Got gRPC response: 3 Hello, Tony Hawk!
2019/08/15 17:59:50 Got gRPC response: 4 Hello, Tony Hawk!
2019/08/15 17:59:50 Got gRPC response: 5 Hello, Tony Hawk!
2019/08/15 17:59:51 Got gRPC response: 6 Hello, Tony Hawk!
2019/08/15 17:59:51 Got gRPC response: 7 Hello, Tony Hawk!
2019/08/15 17:59:52 Got gRPC response: 8 Hello, Tony Hawk!
2019/08/15 17:59:52 Got gRPC response: 9 Hello, Tony Hawk!
```

## Client streaming RPC

[Up](#contnent)

В данном разделе будем реализовывать Client streaming RPC тип для нашего примера.\
Реализация будет аналогично состоять из шагов:
* создаем proto файл
* компилируем proto файл с помощью protoc
* пишем код для сервера и клинта


Создаём папку для проекта
```
mkdir -p $GOPATH/src/gitlab.com/kirch1984/greetLong/{greetpb,server,client}
cd $GOPATH/src/gitlab.com/kirch1984/greetLong/
```
Создаём файл <code>greetpb/greet.proto</code>
```
syntax="proto3";

package greet;
option go_package="greetpb";

message GreetRequest{
    string first_name = 1;
    string last_name = 2;
}

message GreetResponse{
    string result = 1;
}

service GreetService{
    //Unary RPC
    //rpc Greet(GreetRequest) returns(GreetResponse){};
    //Server streaming RPC
    //rpc GreetManyTimes(GreetRequest) returns (stream GreetResponse) {};
    //Client streaming RPC
    rpc LongGreet(stream GreetRequest) returns (GreetResponse){};
}
```
Компилируем
```
protoc greetpb/greet.proto --go_out=plugins=grpc:.
```
В результате получаем файл <code>greetpb/greet.pb.go</code>

Теперь переходим к написаню кода для сервера
* реализуем интерфейс
* пишем код запуска gRPC сервера для обработки клиентских запросов

Создаём файл <code>server/server.go</code> и приступаем к реализации интерфейса
```go
package main

import (
	"bytes"
	"io"
	"log"

	"gitlab.com/kirch1984/greetLong/greetpb"
)

type server struct{}

// смотрим в файле greet.pb.go, какие методы нужно создать для реализации GreetService интерфейса
func (*server) LongGreet(stream greetpb.GreetService_LongGreetServer) error {
	var fname string
	var lname string
	helloMsg := "Hello, "
	buf := &bytes.Buffer{} //используетcя для более эффективной конкатенации строк
	//т.к. получать будем несколько запросов то реализуем это через бесконечный цикл
	for {
		req, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				//по завершению получения всех запросов, отправляем ответ
				return stream.SendAndClose(&greetpb.GreetResponse{
					Result: buf.String(),
				})
			}
			log.Fatalf("Error got gRPC Request: %v\n", err)
		}

		fname = req.GetFirstName()
		lname = req.GetLastName()
		buf.WriteString(helloMsg + fname + " " + lname + "!")
		log.Printf("Got gRPC Request: %v\n", req)
	}
}
func main() {}
```
пишем код для запуска gRPC сервера
```go
func main() {
	port := "127.0.0.1:50051"
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Error listen %v: %v\n", port, err)
	}
	log.Println("Run gRPC server...")
	// создаем gRPC сервер
	s := grpc.NewServer()
	// регистрируем сервис
	greetpb.RegisterGreetServiceServer(s, &server{})
	// вызываем Serve()
	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("Error run gRPC server: %v\n", err)
	}
}
```
Далее переходим на писанию клиента.\
Создайм файл <code>client/client.go</code>
```go
package main

import (
	"context"
	"log"

	"gitlab.com/kirch1984/greetLong/greetpb"
	"google.golang.org/grpc"
)

func main() {
	port := "127.0.0.1:50051"
	// cсоздаём канал
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	defer conn.Close()
	if err != nil {
		log.Fatalf("Error create gRPC channel: %v\n", err)
	}
	// создаем stub
	client := greetpb.NewGreetServiceClient(conn)
	// создаем слайс запросов
	reqs := []*greetpb.GreetRequest{
		&greetpb.GreetRequest{
			FirstName: "Tony",
			LastName:  "Hawk",
		},
		&greetpb.GreetRequest{
			FirstName: "Kevin",
			LastName:  "Mitnick",
		},
		&greetpb.GreetRequest{
			FirstName: "Lamo",
			LastName:  "Adrian",
		},
	}
	// реализуем поточную отправку запросов на сервер
	stream, err := client.LongGreet(context.Background())
	if err != nil {
		log.Fatalf("Error got stream: %v\n", err)
	}

	for _, req := range reqs {
		err = stream.Send(req)
		if err != nil {
			log.Printf("Error send gRPC Request: %v\n", err)
		}
	}
	// получаем ответ
	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error got gRPC Response: %v\n", err)
	}
	log.Printf("Gor gRPC Resposne: %v\n", res.Result)
}
```
Запускаем
```
 go run server/server.go
 go run client/cleint.go
```
Результат\
server
```
2019/08/16 17:18:56 Run gRPC server...
2019/08/16 17:18:58 Got gRPC Request: first_name:"Tony" last_name:"Hawk" 
2019/08/16 17:18:58 Got gRPC Request: first_name:"Kevin" last_name:"Mitnick" 
2019/08/16 17:18:58 Got gRPC Request: first_name:"Lamo" last_name:"Adrian"
```
client
```
2019/08/16 17:18:58 Gor gRPC Resposne: Hello, Tony Hawk!Hello, Kevin Mitnick!Hello, Lamo Adrian!
```
## Bidirectional streaming RPC

[Up](#contnent)

В данном разделе будем реализовывать Bi Di streaming RPC тип для нашего примера.\
Реализация будет аналогично состоять из шагов:
* создаем proto файл
* компилируем proto файл с помощью protoc
* пишем код для сервера и клинта


Создаём папку для проекта
```
mkdir -p $GOPATH/src/gitlab.com/kirch1984/greetEveryone/{greetpb,server,client}
cd $GOPATH/src/gitlab.com/kirch1984/greetEveryone/
```

Создаём файл <code>greetpb/greet.proto</code>
```
syntax="proto3";

package greet;
option go_package="greetpb";

message GreetRequest{
    string first_name = 1;
    string last_name = 2;
}

message GreetResponse{
    string result = 1;
}

service GreetService{
    //Unary RPC
    //rpc Greet(GreetRequest) returns(GreetResponse){};
    //Server streaming RPC
    //rpc GreetManyTimes(GreetRequest) returns (stream GreetResponse) {};
    //Client streaming RPC
    //rpc LongGreet(stream GreetRequest) returns (GreetResponse){};
	//Bi Di Streaming RPC
	rpc GreetEveryone(stream GreetRequest) returns (stream GreetResponse){};
}
```
Компилируем
```
protoc greetpb/greet.proto --go_out=plugins=grpc:.
```
В результате получаем файл <code>greetpb/greet.pb.go</code>

Теперь переходим к написаню кода для сервера
* реализуем интерфейс
* пишем код запуска gRPC сервера для обработки клиентских запросов

Создаём файл <code>server/server.go</code> и приступаем к реализации интерфейса
```go
package main

import (
	"io"
	"log"
	"net"

	"gitlab.com/kirch1984/greetEveryone/greetpb"

	"google.golang.org/grpc"
)

type server struct{}

// смотрим в файле greet.pb.go, какие методы нужно создать для реализации GreetService интерфейса
func (*server) GreetEveryone(stream greetpb.GreetService_GreetEveryoneServer) error {
	var fname string
	var lname string
	res := greetpb.GreetResponse{}
	// в данном пример реализуем немедленный ответ, хотя можно сперва аккумулировать все запросы от клиента, а уже потом ответь 
	for {
		req, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatalf("Error to get gRPC Bi Di Stream Request: %v\n", err)
		}
		log.Printf("Got gRPC Bi Di Stream Request: %v\n", req)
		fname = req.GetFirstName()
		lname = req.GetLastName()
		res.Result = "Hello, " + fname + " " + lname + "!"
		err = stream.Send(&res)
		if err != nil {
			log.Fatalf("Error to send gRPC Bi Di Stream Response: %v\n", err)
		}
	}
	return nil
}
func main() {}
```
пишем код для запуска gRPC сервера
```go
func main() {
	port := "127.0.0.1:50051"
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Error listen %v: %v\n", port, err)
	}
	log.Println("Run gRPC server...")
	// создаем gRPC сервер
	s := grpc.NewServer()
	// регистрируем сервис
	greetpb.RegisterGreetServiceServer(s, &server{})
	// вызываем Serve()
	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("Error run gRPC server: %v\n", err)
	}
}
```
Далее переходим на писанию клиента.\
Создайм файл <code>client/client.go</code>
```go
package main

import (
	"context"
	"io"
	"log"
	"sync"

	"gitlab.com/kirch1984/greetEveryone/greetpb"

	"google.golang.org/grpc"
)

func main() {
	port := "127.0.0.1:50051"
	// cсоздаём канал
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Error to create gRPC client connection: %v\n", err)
	}
	// создаем stub
	client := greetpb.NewGreetServiceClient(conn)
	// вынесим реализацию BiDiStream в отдельную фунию
	doBiDiStream(client)
}

func doBiDiStream(client greetpb.GreetServiceClient) {
	// создаем слайс запросов
	reqs := []*greetpb.GreetRequest{
		&greetpb.GreetRequest{
			FirstName: "Tony",
			LastName:  "Hawk",
		},
		&greetpb.GreetRequest{
			FirstName: "Kevin",
			LastName:  "Mitnick",
		},
		&greetpb.GreetRequest{
			FirstName: "Lamo",
			LastName:  "Adrian",
		},
	}
	// реализуем поточную отправку запросов на сервер
	stream, err := client.GreetEveryone(context.Background())
	if err != nil {
		log.Fatalf("Error to get gRPC stream object: %v\n", err)
	}
	wg := &sync.WaitGroup{}
	// отправляем запросы
	wg.Add(1)
	go func() {
		for _, req := range reqs {
			err = stream.Send(req)
			if err != nil {
				log.Fatalf("Error to send gRPC Bi Di Stream Request: %v\n", err)
			}
		}
		err = stream.CloseSend()
		if err != nil {
			log.Fatalf("Error to close stream: %v\n", err)
		}
		wg.Done()
	}()
	// получаем ответы
	wg.Add(1)
	go func() {
		for {
			res, err := stream.Recv()
			if err != nil {
				if err == io.EOF {
					break
				}
				log.Fatalf("Error to ger gRPC Bi Di Stream Response: %v\n", err)
			}
			log.Printf("Got gRPC Bi Di Stream Response: %v\n", res.Result)
		}
		wg.Done()
	}()
	//block
	wg.Wait()

}
```

Запускаем
```
 go run server/server.go
 go run client/cleint.go
```
Результат\
server
```
2019/08/27 00:28:04 Run gRPC server...
2019/08/27 00:28:06 Got gRPC Bi Di Stream Request: first_name:"Tony" last_name:"Hawk" 
2019/08/27 00:28:06 Got gRPC Bi Di Stream Request: first_name:"Kevin" last_name:"Mitnick" 
2019/08/27 00:28:06 Got gRPC Bi Di Stream Request: first_name:"Lamo" last_name:"Adrian" 
```
client
```
2019/08/27 00:28:06 Got gRPC Bi Di Stream Response: Hello, Tony Hawk!
2019/08/27 00:28:06 Got gRPC Bi Di Stream Response: Hello, Kevin Mitnick!
2019/08/27 00:28:06 Got gRPC Bi Di Stream Response: Hello, Lamo Adrian!
```

# Error

[Up](#contnent)

[Error Handling](https://grpc.io/docs/guides/error/)\
[A handy guide to gRPC errors](http://avi.im/grpc-errors/)\
[Advanced gRPC Error Usage](https://jbrandhorst.com/post/grpc-errors/)

При возникновении ошибки, gRPC возвращает один из кодов ошибки и опционально текст ошибки, который представляет дополнительную информацио о том что произошло.

## Go Example

[Up](#contnent)

Для обработки gRPC ошибок необходимо импортировать два пакета

```go
import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)
```

Рассмотрим простой пример сервиса, который делит число `a` на `b` и возвращает результат деления. В случае, если `b` равен `0`, то нам вернется ошибка о неверном аргументе.

Создаём папку для проекта
```
mkdir -p $GOPATH/src/gitlab.com/kirch1984/go-divider/{dividerpb,server,client}
cd $GOPATH/src/gitlab.com/kirch1984/go-divider/
```

Создаём файл <code>dividerpb/divider.proto</code>

```
syntax="proto3";

package divider;
option go_package="dividerpb";

message DivierRequest {
    float a = 1;
    float b = 2;
}

message DividerResponse{
    float result  = 1;
}

service DividerService {
    rpc Divide(DivierRequest) returns(DividerResponse) {};
}
```
компилируем
```
protoc dividerpb/divider.proto --go_out=plugins=grpc:.
```

Переходим к написанию сервера, создаем файл `server/server.go`\
Начнем с реализации интерфейса нашего сервиса `DividerService`
```go
package main

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/kirch1984/go-divider/dividerpb"
)

type server struct{}

func (*server) Divide(ctx context.Context, r *dividerpb.DivierRequest) (*dividerpb.DividerResponse, error) {
	dividend := r.GetA()
	divider := r.GetB()
	if divider == 0 {
		//возвращаем grpc ошибку с необходимым статусом и поясняющим текстом
		//используя status.Errorf(<grpc error code>, <error message>)
		return nil, status.Errorf(codes.InvalidArgument, "Divider incorrect")
	}
	res := dividerpb.DividerResponse{
		Result: dividend / divider,
	}
	return &res, nil
}

func main() {}
```
пишем gRPC сервер
```go
func main() {
	port := "127.0.0.1:50051"
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Error listen %v: %v\n", port, err)
	}
	log.Println("Run gRPC server...")
	s := grpc.NewServer()
	dividerpb.RegisterDividerServiceServer(s, &server{})
	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("Error run gRPC Server: %v\n", err)
	}
}
```
Переходим к написанию клиента, создаем файл `client/client.go`
```go
package main

import (
	"context"
	"log"

	"google.golang.org/grpc/status"

	"gitlab.com/kirch1984/go-divider/dividerpb"
	"google.golang.org/grpc"
)

func main() {
	port := "127.0.0.1:50051"
	//создаем grpc соединение
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Error to connect to gRPC server: %v\n", err)
	}
	//создаем stub
	client := dividerpb.NewDividerServiceClient(conn)
	//пример
	doDivide(client, 7, 2)
	doDivide(client, 7, 0)
}

func doDivide(client dividerpb.DividerServiceClient, a, b float32) {
	req := dividerpb.DivierRequest{
		A: a,
		B: b,
	}
	res, err := client.Divide(context.Background(), &req)
	if err != nil {
		//здесь мы проверяме - это grpc ошибка или нет
		statusErr, ok := status.FromError(err)
		//если grpc ошибка
		if ok {
			//выводим статус и текст ошибки
			log.Printf("Satus Code: %v ; Message Error: %v\n", statusErr.Code(), statusErr.Message())
			return
		}
		log.Printf("Error to get gRPC Response: %v\n", err)
		return
	}
	log.Printf("Got gRPC Respnse: %v\n", res.Result)
}
```
Запускаем
```
go run server/server.go
go run client/client.go
```
Рузультат
```
2019/08/29 13:56:41 Got gRPC Respnse: 3.5
2019/08/29 13:56:41 Satus Code: InvalidArgument ; Message Error: Divider incorect
```

# Deadlines

[Up](#contnent)

[gRPC and Deadlines](https://grpc.io/blog/deadlines/)

Всегда выставляйте deadline для запросов.\
Deadline позволяет gRPC клиенту выставить значение сколько времени он готов ждать завершения RPC, прежде чем RPC завершится с ошибкой `DEADLINE_EXCEEDED`.\
Если не выставлять deadline, то ресурсы будут удерживаться для всех работающих,зависших запросов, что может привести к исчерпанию ресурсов (например памяти).\
Чтоб избежать этого, необходимо указывать deadline.

На стороне клиента deadline создается с помощью контекста
```go
deadLine := time.Duration(time.Second)
ctx, cancel := context.WithTimeout(context.Background(), deadLine)
defer cancel()
```
На стороне сервера перед отправкой ответа, обязательно проверить - ожидает ли клиент ответ.
```go
//можно через switch/case
if ctx.Err() == context.DeadlineExceeded || ctx.Err() == context.Canceled {
	return status.Errorf(codes.Canceled, "Client cancelled, abandoning.")
}
```

## Go Example

В качестве практики испольуем предыдущий пример про делитель, добавив необходимый код и задержку на сервере для эмуляции "тормозов".

сперва добавим deadline на стороне клиента
```go
package main

import (
	"context"
	"log"
	"time"

	"google.golang.org/grpc/codes"

	"google.golang.org/grpc/status"

	"gitlab.com/kirch1984/go-divider/dividerpb"
	"google.golang.org/grpc"
)

func main() {
	port := "127.0.0.1:50051"
	//создаем grpc соединение
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Error to connect to gRPC server: %v\n", err)
	}
	//создаем stub
	client := dividerpb.NewDividerServiceClient(conn)

	doDivide(client, 7, 2)
	doDivide(client, 7, 0)
}

func doDivide(client dividerpb.DividerServiceClient, a, b float32) {
	req := dividerpb.DivierRequest{
		A: a,
		B: b,
	}
	//deadline
	//создаем контекст с таймаутом (время которое клиент готов ждать выполнение запроса), который будет передан серверу вместе с запросом
	//в данном пример таймаут 1 сек
	deadLine := time.Duration(time.Second)
	ctx, cancel := context.WithTimeout(context.Background(), deadLine)
	defer cancel()
	res, err := client.Divide(ctx, &req)
	if err != nil {
		//здесь мы проверяме - это grpc ошибка или нет
		statusErr, ok := status.FromError(err)
		//если grpc ошибка
		if ok {
			//проверяем, что отвалились по таймауту
			if statusErr.Code() == codes.DeadlineExceeded {
				log.Println("Deadline was exceeded")
				//проверяем не является ошибка неверного аргумента
			} else if statusErr.Code() == codes.InvalidArgument {
				log.Printf("Satus Code: %v ; Message Error: %v\n", statusErr.Code(), statusErr.Message())
			} else {
				log.Printf("unexpected error: %v\n", err)
			}
			return
		}
		log.Printf("Error to get gRPC Response: %v\n", err)
		return
	}
	log.Printf("Got gRPC Respnse: %v\n", res.Result)
}
```

на серере добаялем проверку перед отправкой данных
```go
package main

import (
	"context"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/kirch1984/go-divider/dividerpb"
)

type server struct{}

func (*server) Divide(ctx context.Context, r *dividerpb.DivierRequest) (*dividerpb.DividerResponse, error) {
	dividend := r.GetA()
	divider := r.GetB()

	if divider == 0 {
		//возвращаем grpc ошибку с необходимым статусом и поясняющим текстом
		//используя status.Errorf(<grpc error code>, <error message>)
		return nil, status.Errorf(codes.InvalidArgument, "Divider incorect")
	}
	res := dividerpb.DividerResponse{
		Result: dividend / divider,
	}
	//эмулируем "тормоза"
	time.Sleep(2 * time.Second)
	//перед отправкой ответа проверяем, что клиент всё ещё ожидает ответа
	switch ctx.Err() {
	case context.DeadlineExceeded, context.Canceled:
		log.Printf("error: client canceled request: %v\n", ctx.Err())
		return nil, status.Errorf(codes.Canceled, "Client cancelled, abandoning.")
	}

	return &res, nil
}

func main() {
	port := "127.0.0.1:50051"
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Error listen %v: %v\n", port, err)
	}
	log.Println("Run gRPC server...")
	s := grpc.NewServer()
	dividerpb.RegisterDividerServiceServer(s, &server{})
	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("Error run gRPC Server: %v\n", err)
	}
}
```

запускам
```
go run server/server.go
go run client/client.go
```
Результат\
server
```
2019/08/29 17:18:28 Run gRPC server...
2019/08/29 17:18:31 error: client canceled request: context canceled
```
client
```
2019/08/29 17:18:30 Deadline was exceeded
2019/08/29 17:18:30 Satus Code: InvalidArgument ; Message Error: Divider incorect
```

# gRPC Reflection & Evans CLI

[Up](#contnent)

[package reflection](https://godoc.org/google.golang.org/grpc/reflection)\
[https://github.com/ktr0731/evans](https://github.com/ktr0731/evans)

в gRPC рефлексия полезна в случаях:
* позволяет продемонстриовать, какие точки входа доступны на сервере
* позволяет cli утилитам общатсья с сервером без наличия proto файла


Чтоб включить рефлексию достаточно добавить в код строчки на сервер
```
import "google.golang.org/grpc/reflection"
...
reflection.Register(s)
...
```

Добавим рефлексию в пример с делением\
открываем файл `$GOPATH/src/gitlab.com/kirch1984/go-divider/server/server.go`
```go
func main() {
	port := "127.0.0.1:50051"
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Error listen %v: %v\n", port, err)
	}
	log.Println("Run gRPC server...")
	s := grpc.NewServer()
	dividerpb.RegisterDividerServiceServer(s, &server{})
	reflection.Register(s) // <- добавляем рефлексию
	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("Error run gRPC Server: %v\n", err)
	}
}
```

Запускаем сервер
```
cd $GOPATH/src/gitlab.com/kirch1984/go-divider/
go run server/server.go
```

Устанавливаем evans
```
go get github.com/ktr0731/evans
```
Подключаемся с помощью evans к нашему gRPC серверу
```
evans -r -p 50051 --host 127.0.0.1
```
Вызовем метод `Divide`
```
divider.DividerService@127.0.0.1:50051> call Divide
a (TYPE_FLOAT) => 77
b (TYPE_FLOAT) => 3
{
  "result": 25.666666
}

divider.DividerService@127.0.0.1:50051> call Divide
a (TYPE_FLOAT) => 3
b (TYPE_FLOAT) => 0
command call: failed to send a request: rpc error: code = InvalidArgument desc = Divider incorect

divider.DividerService@127.0.0.1:50051> 

```

# SSL Security

[Up](#contnent)

[Authentication](https://grpc.io/docs/guides/auth/)\
[grpc-auth-support](https://github.com/grpc/grpc-go/blob/master/Documentation/grpc-auth-support.md)\
[Secure gRPC with TLS/SSL](https://bbengfort.github.io/programmer/2017/03/03/secure-grpc.html)\
[Building microservices in Go and Python using gRPC and TLS/SSL authentication](https://medium.com/@gustavoh/building-microservices-in-go-and-python-using-grpc-and-tls-ssl-authentication-cfcee7c2b052)

* В продакшене RPC должны быть шфрованными, реализуется это с помощью SSl/TLS
* SSL/TLS позволяет избежать атаки man in the middle (MID)