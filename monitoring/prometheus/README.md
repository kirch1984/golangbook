
- [Introduction to Prometheus](#introduction-to-prometheus)
- [Installation and Getting Started](#installation-and-getting-started)
- [Monitoring Nodes and Containers](#monitoring-nodes-and-containers)
  - [Monitoring nodes](#monitoring-nodes)
    - [Configuring the Textfile collector](#configuring-the-textfile-collector)
    - [Enabling the systemd collector](#enabling-the-systemd-collector)
    - [Scraping the Node Exporter](#scraping-the-node-exporter)
    - [Filtering collectors on the server](#filtering-collectors-on-the-server)
  - [Monitoring Docker](#monitoring-docker)
    - [Scraping cAdvisor](#scraping-cadvisor)

# Introduction to Prometheus

[Prometheus DOCS](https://prometheus.io/docs/)\
[Alertmanager](https://prometheus.io/docs/alerting/alertmanager/)\
[Exporters and integrations](https://prometheus.io/docs/instrumenting/exporters/)\
[Client libraries](https://prometheus.io/docs/instrumenting/clientlibs/)\
[Grafana](https://grafana.com/)\
[Prometheus with Docker](https://prometheus.io/docs/prometheus/latest/installation/#using-docker)\
[Configuration](https://prometheus.io/docs/prometheus/latest/configuration/configuration/)\
[PromQL](https://prometheus.io/docs/prometheus/latest/querying/basics/)\
[Translating between monitoring languages](https://www.robustperception.io/translating-between-monitoring-languages)\
[Common query patterns in PromQL](https://www.robustperception.io/common-query-patterns-in-promql)

Prometheus предназначен для мониторинга в реальном времени облаков, микросервисов в контейнерах, сервисов и приложений.


Prometheus сосредоточен на том что происходит сейчас, а не на отслеживании данных неделями или месяцами.\
Prometheus работает вытягивая веременные ряды данных из приложений. Приложения могут использовать библиотеки для предоставления данны временного ряда или использовать прокси называемомго экспортером, как HTTP конечная точка. Также сужествуют [push gateway](https://github.com/prometheus/pushgateway) на тот случай если хост находится за натом.


Источник( метрик, которые он может получить, называются конечными точками(endpoint). Обычно они соответсвуют одному процессу, хосту, сервису или приложению. Чтоб вытащить данные с конечной точки, Промитей вызывает конфигурацию называемую целью(таргет/target). Это информация необходима для получения данных и может сожержать - способ подключения, метаданные, аутентификационные данные итд. Группа таргетов называется джобом(jobs). Джоб -  это объединение таргетов по какой либо роли - например Apache сервера за балансировщиком. Полученне данные промитей может хранить, как локально, так и отправить на другой сторэдж. По умолчанию промитей хранит данные локально 15 дней.

Обнаружение ресурсов может происходить одним из следующих способов:
* пользователь предостаялет список ресурсво сам
* обнаружние на основе файлов - к примеру используя истурмент упралвения сгенерировать список ресурсов которые автоматически обновляются в промитее
* автоматическое обанружение - Consul, DNS SRV ...

У промитея нет своего встроенного интсрумента оповещения, поэтому он использует отделыйн сервер для этого [Alertmanager.](https://prometheus.io/docs/alerting/alertmanager/)

Временной ряд данных состоит из имени, обычно описывающий назначение собираемых даннйх и набора меток ключ-значение. Совокупность имени и меток делает каждый временой ряд уникальным.\
Метки делятся на два типа:
* Target - связаны больше с архитектурой, например могут идентифицировать дата центр откуда временой ряд был взят. Такие метки добавляются промитеем в течении или после скрапинга.
* Instrumentation - как правило поступаю от отслеживаемого ресурса. Такие метки добавляются экспортером или самим клиентом до того как временный данные буду скрапится.
```
//template
<time series name>{<label name>=<label value>, ...}
//exapmle
total_website_visits{site="MegaApp", location="NJ", instance="webserver",job="web"}
```
Каждый временной ряд содержит обязательно метки `instance` - опредляет исходный хост или приложение, и `job` - содержит имя джоба вытягивающий временной ряд данных.

# Installation and Getting Started

```
//Running Prometheus with Docker
docker run -p 9090:9090 prom/prometheus
//Mounting a configuration file into the Docker container
docker run -p 9090:9090 -v /tmp/prometheus.yml:/etc/prometheus/
prometheus.yml prom/prometheus
```
```
version: '3'

services:
  grafana:
    image: grafana/grafana
    hostname: 'calendar_grafana'
    container_name: 'calendar_grafana'
    restart: always
    ports:
      - "3000:3000"
    links:
      - prometheus
    networks: 
      - calendar
    

  prometheus:
    image: prom/prometheus
    hostname: 'calendar_prometheus'
    container_name: 'calendar_prometheus'
    restart: always
    ports:
      - "9090:9090"
    networks: 
      - calendar


networks: 
  calendar:
```
Конфигурационный файл прометея `/etc/prometheus/prometheus.yml` разбит на блоки:
* `global` - содержит глобальные настройки для контроля поведения сервера и включает в себя такие натсройки, как:\
`scrape_interval` - параметр определящий интервал между получением данных от приложения или сервиса. Данный параметр можно перезатереть параметром более локально масштаба, но это не рекомендуется.\
`evaluation_interval` - как часто прометею оценивать правила. Правила бывают двух видов:
  * `Recording` - позволяет определить - частые и "дорогие" выражения и сохранить их результат, как временный ряд данных
  * `Alerting` - позволяют определить условия оповещения
* `alerting` - как упоминалось ранее, у прометея нет своей опвещалки, поэтому используется отдельный сервис для этого. В данной области определяются настройки для `Alertmanager`
* `rule_files` - список файлов определяющих `Recording` или `Alerting` правила
* `scrape_configs` - здесь определяются все таргеты, который прометей опрашивает
```
scrape_configs:
    - job_name: 'prometheus'
    static_configs:
    - targets: ['localhost:9090']
```
`static_configs` - можно думать об этой опции, как "human service discovery"

Валидация конфига прометея
```
promtool check config prometheus.yml
```

После уствноки и запуска сервера, можно воспользоваться следующими ссылками\
http://localhost:9090/metrics\
http://localhost:9090/graph

Прометей имеет встроенный язык [PromQL](https://prometheus.io/docs/prometheus/latest/querying/basics/) - позволяющий запрашиать и агригировать метрики.\
В качестве примера рассмотрим мерику `http_requests_total` - возвращает общее число HTTP запросов сделанных обработчиками Прометею.\
`sum(http_requests_total)` - суммируем все http запросы\
`sum(http_requests_total) by (job)` - агригируем по джобам, `without` - агригирует данные без указанной метки\
`sum(rate(http_requests_total[5m])) by (job)`\
`rate()` - данная фуния вычесляет среднюю скорость роста временного диапазона в секунду. Всегда используется с счётчиком. Данную фунию хорошо исп для медленно изменяющегося счётчика.  

# Monitoring Nodes and Containers
## Monitoring nodes
[Node Exporter](https://github.com/prometheus/node_exporter) - экспортер для метрик железа и ОС (*nix систем) написанный на Go с подключаемыми коллекторами.

```
aptitude install prometheus-node-exporter
```
или
```
wget
https://github.com/prometheus/node_exporter/releases/download/v
0.16.0/node_exporter-0.16.0.linux-amd64.tar.gz
$ tar -xzf node_exporter-*
$ sudo cp node_exporter-*/node_exporter /usr/local/bin/
```
Смотрим версию версию 
```
prometheus-node-exporter --version
node_exporter, version 0.15.2+ds (branch: debian/sid, revision: 0.15.2+ds-1)
  build user:       pkg-go-maintainers@lists.alioth.debian.org
  build date:       20171214-15:26:08
  go version:       go1.9.2
```
`prometheus-node-exporter` - бинарник конфигурируемый через флаги. Список всех флагов:
```
prometheus-node-exporter --help
```
По умолчанию запускается на порту `9100` и показывает метрики `/metrics`\
С помощью флагов `--web.listen-address` и `--web.telemetry-path` можно менять порт и путь отображения метрик
```
prometheus-node_exporter --web.listen-address=":9600" --web.telemetry-
path="/node_metrics"
```

Данные эксопртер имеет список, как включенных так и отключенных коллекторов.\
[Enabled by default](https://github.com/prometheus/node_exporter#enabled-by-default)\
[Disabled by default](https://github.com/prometheus/node_exporter#disabled-by-default)

`prometheus-node-exporter --no-collector.arp` - выключаем коллектор\
`prometheus-node-exporter --collector.arp` - включаем коллектор

### Configuring the Textfile collector
`textfile` - очень полезный коллектор, позволяющий выставлять собственные метрики. Работает он сканирую файлы в специальных папках и извлекая строки [форматированный как Прометей формат](https://prometheus.io/docs/instrumenting/exposition_formats/#text-format-details) и выствляет их для предоствления серверу.

Метрики определяются в файлах `.prom` внутри папки `/var/lib/prometheus/node-exporter/`. [Плюс содержимое такого файла должно быть специально отформатировано](https://prometheus.io/docs/instrumenting/exposition_formats/#text-format-details).
```
//template
metric_name [
  "{" label_name "=" `"` label_value `"` { "," label_name "=" `"` label_value `"` } [ "," ] "}"
] value [ timestamp ]
```
В качестве примера создадим метрику содержащую метаданный про текущий хост:
```
metadata{role="docker_server",datacenter="VL"} 1
```
`metadata` - имя метрики\
`role` - метка определяющая роль сервера\
`datacenter` - метка с местоположением \
и в конце имеется цифра 1, потому что данная метрика предоставляет контекст, а не счётчик или таймер\
Создаем файл для метрики:
```
echo 'metadata{role="docker_server",datacenter="NJ"} 1' | sudo tee /var/lib/prometheus/node-exporter/metadata.prom
```

### Enabling the systemd collector
Настроим еще один коллектор `systemd`, который отслеживет состояние всех сервисов из systemd сервисс Linux ОС. Ограничим пример тремя сервисами для мониторинга - `docker,ssh,rsyslog`.\
Добавим в файл `/etc/default/prometheus-node-exporter` опции  `--collector.systemd --collector.systemd.unit-whitelist=(docker|ssh|rsyslog).service"`.

```
ARGS="--collector.diskstats.ignored-devices=^(ram|loop|fd|(h|s|v|xv)d[a-z]|nvme\\d+n\\d+p)\\d+$ \
      --collector.filesystem.ignored-mount-points=^/(sys|proc|dev|run)($|/) \
      --collector.netdev.ignored-devices=^lo$ \
      --collector.textfile.directory=/var/lib/prometheus/node-exporter \
      --collector.systemd \
      --collector.systemd.unit-whitelist=(docker|ssh|rsyslog).service"
```
Перезапускаем сервис
```
service prometheus-node-exporter restart
```


### Scraping the Node Exporter
Таким образом был добвлен коллектор `textfile` и `systemd`. Теперь проверим, что в файле `/etc/prometheus/prometheus.yml` имеется следющий конфиг(если нет, то добавить). Вместо `localhost` нужно указать ip сервера на котором устанвливался `node_exporter`.

```
- job_name: node
    static_configs:
        - targets: ['localhost:9100']
```

### Filtering collectors on the server
node-exporter может возвращать много информации, которая невсегда нужна. Прометей сервер имеет возможность ограничивать данную информацию. Делается это путем добавления опции `params` в файл `/etc/prometheus/prometheus.yml`
```
- job_name: node
    static_configs:
      - targets: ['localhost:9100']
    params:
      collect[]:
        - cpu
        - meminfo
        - diskstats
        - netdev
        - netstat
        - filefd
        - filesystem
        - xfs
        - systemd
```

## Monitoring Docker

Контэйниры мониторятся с помощью [Google’s cAdvisor](https://github.com/google/cadvisor)\
cAdvisor тоже запускается как докер контэйнер на хостовой машине, он возвращает метрики для всех запущенных контейнеров.
```
docker run \
  --volume=/:/rootfs:ro \
  --volume=/var/run:/var/run:ro \
  --volume=/sys:/sys:ro \
  --volume=/var/lib/docker/:/var/lib/docker:ro \
  --volume=/dev/disk/:/dev/disk:ro \
  --publish=8080:8080 \
  --detach=true \
  --name=cadvisor \
  google/cadvisor:latest
```
Заходим
```
http://mimas.poex.me:8080/containers/
http://mimas.poex.me:8080/metrics
```
### Scraping cAdvisor
Добавляем в конфиг прометея:

```
  - job_name: node
    static_configs:
      - targets: ['localhost:9100']
    params:
      collect[]:
        - cpu
        - meminfo
        - diskstats
        - netdev
        - netstat
        - filefd
        - filesystem
        - xfs
        - systemd
   - job_name: docker
     static_configs:
       - targets: ['mimas.poex.me:8080']
```